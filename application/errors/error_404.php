<?php

require_once 'variables.php';

$title = '404 Error';
$errorHeading = 'Page Not Found';
$errorMessage = <<<ERROR_MSG
    <p>Sorry, the page you requested was not found.</p>
    <p><a href="$appPath">Return to homepage</a></p>
ERROR_MSG;

require_once 'template.php';
