<?php

require_once 'variables.php';

$title = 'IE Not Supported';
$errorHeading = 'Internet Explorer Not Supported';
$errorMessage = <<<ERROR_MSG
    <p>
        We're sorry, but Internet Explorer browser is not supported by the application.<br/>
        Please consider using <a href="https://www.google.com/intl/en/chrome/browser/">Google Chrome</a>
        (recommended) or <a href="http://www.mozilla.org/en-US/firefox/new/">Mozilla Firefox</a>
        instead.<br/>
        Sorry for the inconvenience, and thank you for your kind consideration.
    </p>
ERROR_MSG;

require_once 'template.php';
