<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title><?= "$appName - $title" ?></title>
    <link rel="icon" type="image/png" href="<?= $appPath ?>/public/images/favicon.png"/>
    <link rel="stylesheet" type="text/css" href="<?= $appPath ?>/public/css/style.css"/>
</head>

<body>

<div id="header">
    <div>
        <img src="<?= $appPath ?>/public/images/app_icon.png" alt="medicare_logo"/>
        <span class="sitename"><?= $appName ?></span>
    </div>
    <hr/>
</div>

<div class="text-align-center" style="margin: 50px 0">
    <div class="section-box section-box-shadow rounded-corners">
        <div class="section-box-header rounded-corners-top" style="text-align: left">
            <?= $errorHeading ?>
        </div>
        <div class="section-box-body text-align-center">
            <?= $errorMessage ?>
        </div>
    </div>
</div>

<div class="footer">
    Copyright &copy;
    <a href="http://www.crypticit.com" target="_blank">Cryptic IT</a>, 2011 - <?= date_format(date_create(), "Y") ?>.
    All Rights Reserved.<br/>
    Version: <?= $appVersion ?>
</div>

</body>

</html>
