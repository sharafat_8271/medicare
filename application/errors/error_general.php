<?php

require_once 'variables.php';

$title = 'Application Error';
$errorHeading = $title;
$errorMessage = <<<ERROR_MSG
    <p style="text-align: left">
        We're sorry, the application has encountered an error. The $appName development team has already been notified.<br/>
        However, if you have any queries or complaints, please contact
        <a href="mail-to:medicare-support@crypticit.com">medicare-support@crypticit.com</a>.
    </p>
    <p><a href="$appPath">Return to homepage</a></p>
ERROR_MSG;

require_once 'template.php';
