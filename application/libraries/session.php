<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * When using CodeIgniter 2.0 session library, I faced a problem that refreshing a page caused new sessions to be created.
 * Somewhat as described at http://stackoverflow.com/questions/7492406/codeigniter-session-problem
 * I couldn't solve it, so, instead, I made this session library of my own.
 *
 * @author Sharafat
 * @created: 12/29/12 12:04 PM
 */
class Session {
    const FLASHDATA_KEY_PREFIX = "flash_";

    public function __construct() {
        // Delete 'old' flashdata (from last request)
        $this->sweepFlashData();

        // Mark all new flashdata as old (data will be deleted before next request)
        $this->markFlashData();
    }

    private function markFlashData() {
        foreach ($this->all_userdata() as $key => $value) {
            $parts = explode(':new:', $key);
            if (is_array($parts) && count($parts) === 2) {
                $newKey = self::FLASHDATA_KEY_PREFIX . ':old:' . $parts[1];
                $this->set_userdata($newKey, $value);
                $this->unset_userdata($key);
            }
        }
    }

    private function sweepFlashData() {
        foreach ($this->all_userdata() as $key => $value) {
            if (strpos($key, ':old:')) {
                $this->unset_userdata($key);
            }
        }
    }

    public function set_userdata($key, $value) {
        $_SESSION[$key] = $value;
    }

    public function userdata($key) {
        return array_key_exists($key, $_SESSION) ? $_SESSION[$key] : null;
    }

    public function unset_userdata($key) {
        unset($_SESSION[$key]);
    }

    public function sess_destroy() {
        @session_unset();
        @session_destroy();
    }

    public function all_userdata() {
        return $_SESSION;
    }

    public function set_flashdata($key, $value) {
        $this->set_userdata(self::FLASHDATA_KEY_PREFIX . ':new:' . $key, $value);
    }

    public function flashdata($key) {
        return $this->userdata(self::FLASHDATA_KEY_PREFIX . ':old:' . $key);
    }

    public function keep_flashdata($key) {
        // 'old' flashdata gets removed. Here we mark the flashdata as 'new' to preserve it from _flashdata_sweep()
        // Note the function will return FALSE if the $key provided cannot be found
        $oldFlashDataKey = self::FLASHDATA_KEY_PREFIX . ':old:' . $key;
        $value = $this->userdata($oldFlashDataKey);

        $newFlashDataKey = self::FLASHDATA_KEY_PREFIX . ':new:' . $key;
        $this->set_userdata($newFlashDataKey, $value);
    }
}
