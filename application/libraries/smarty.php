<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

require_once(APPPATH . 'libraries/smarty/libs/SmartyBC.class.php');

class CI_Smarty extends SmartyBC {

    public function __construct() {
        parent::__construct();

        $this->caching = Smarty::CACHING_OFF;
        $this->setCompileDir(FCPATH . 'cache' . DS);
        $this->setCacheDir(FCPATH . 'cache' . DS);
        $this->setTemplateDir(FCPATH . APPPATH . 'views' . DS);
    }

    public function view($template_name, CI_Controller $controller = null) {
        if (strpos($template_name, '.') === false && strpos($template_name, ':') === false) {
            $template_name .= '.tpl';
        }

        if ($controller != null) {
            $this->assign("elapsed_time", $this->getElapsedTime($controller));
            $this->assign("memory_usage", $this->getMemoryUsage());
        }

        parent::display($template_name);
    }

    private function getElapsedTime(CI_Controller $controller) {
        return $controller->benchmark->elapsed_time('total_execution_time_start', 'total_execution_time_end');
    }

    private function getMemoryUsage() {
        return (!function_exists('memory_get_usage')) ? '0' : round(memory_get_usage() / 1024 / 1024, 2);
    }
}

