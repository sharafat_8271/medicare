<?php
/**
 * Author:  sharafat
 * Created: 10/28/12 8:48 PM
 */
use Doctrine\DBAL\Logging\SQLLogger;

class Log4PhpSqlLogger implements SQLLogger {
    private $log;

    function __construct() {
        $this->log = Logger::getLogger(__CLASS__);
    }

    /**
     * Logs a SQL statement somewhere.
     *
     * @param string $sql    The SQL to be executed.
     * @param array  $params The SQL parameters.
     * @param array  $types  The SQL parameter types.
     *
     * @return void
     */
    public function startQuery($sql, array $params = null, array $types = null) {
        $query = "SQL: $sql"
            . ($params ? PHP_EOL . "Params: " . print_r($params, true) : "")
            . ($types ? PHP_EOL . "Types: " . print_r($types, true) : "");
        $this->log->debug($query);
    }

    /**
     * Mark the last started query as stopped. This can be used for timing of queries.
     *
     * @return void
     */
    public function stopQuery() {
        //ignore
    }
}
