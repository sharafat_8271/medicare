<?php
require_once(APPPATH . '/config/language.php');

/**
 * Author:  sharafat
 * Created: 8/29/12 11:30 PM
 */
class I18N {
    private static $instance;
    private $messageFileName;
    private $messages = array();

    public static function getInstance() {
        if (self::$instance == null) {
            self::$instance = new I18N();
        }

        return self::$instance;
    }

    public function __construct() {
        if (self::$instance == null) {
            $this->messages = $this->loadMessagesFile();
            self::$instance = $this;
        }
    }

    private function loadMessagesFile() {
        $CI_config =& load_class('Config');
        $lang = $CI_config->item('language');
        global $config;
        $messages = ($lang == $config['default_language'])
            ? $this->loadDefaultMessageFile() : $this->loadMessageFile($lang);
        return $messages;
    }

    private function loadDefaultMessageFile() {
        $messageFileName = "messages.ini";
        $messages = $this->parseMessageFile($messageFileName);
        if (!$messages) {
            trigger_error("Messages file not found: $messageFileName", E_USER_ERROR);
            exit;
        }

        return $messages;
    }

    private function loadMessageFile($lang) {
        $messageFileName = "messages_$lang.ini";
        $messages = $this->parseMessageFile($messageFileName);
        if (!$messages) {
            trigger_error("Messages file not found: $messageFileName", E_USER_WARNING);
            $messages = $this->loadDefaultMessageFile();
        }

        return $messages;
    }

    private function parseMessageFile($messageFileName) {
        global $config;
        $messagesFilePath = $config['messages_file_path'] . DS . $messageFileName;
        $messages = parse_ini_file($messagesFilePath);
        return $messages;
    }

    public function _($key) {
        $value = array_key_exists($key, $this->messages) ? $this->messages[$key] : null;

        if (!$value) {
            trigger_error("No such key exists in {$this->messageFileName}: $key", E_USER_NOTICE);
            return $key;
        }

        if (func_num_args() == 1) {
            return $value;
        }

        /* format-string */
        $args = func_get_args();
        //remove the first argument of this function (i.e. $key)
        array_splice($args, 0, 1);
        //if an array has been passed as argument (from Smarty template), then extract it
        if (is_array($args[0])) {
            $args = $args[0];
        }
        return vsprintf($value, $args);
    }
}
