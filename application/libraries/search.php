<?php
/**
 * Author:  sharafat
 * Created: 1/16/13 9:02 PM
 */

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Search {
    const FLASH_DATA_KEY_SEARCH = "search";

    const DQL_PERSON_OBJECT_NAME = "person";

    const DB_FIELD_NAME = "name";
    const DB_FIELD_ID = "id";
    const DB_FIELD_EMAIL = "email";
    const DB_FIELD_CATEGORY = "category";
    const DB_FIELD_DOB = "dob";
    const DB_FIELD_PROVIDER = "provider";

    const SEARCH_FIELD_PERSON_TYPE = "personType";
    const SEARCH_FIELD_FILTER_BY = "filterBy";
    const SEARCH_FIELD_FILTER_TEXT = "filterText";
    const SEARCH_FIELD_NAME_STARTS_WITH = "nameStartsWith";
    const SEARCH_FIELD_CATEGORY = "category";
    const SEARCH_FIELD_SORT_BY = "sortBy";
    const SEARCH_FIELD_SORT_ORDER = "sortOrder";
    const SEARCH_FIELD_OFFSET = "offset";
    const SEARCH_FIELD_PROVIDER = "provider";

    const PERSON_TYPE_ALL = 1;
    const PERSON_TYPE_ADMIN = 2;
    const PERSON_TYPE_DOCTOR = 3;
    const PERSON_TYPE_PATIENT = 4;

    const FILTER_BY_NAME = 1;
    const FILTER_BY_REG_NO = 2;
    const FILTER_BY_EMAIL = 3;

    const SORT_ORDER_ASC = "ASC";
    const SORT_ORDER_DESC = "DESC";

    /** @var $input CI_Input */
    private $input;

    public function getQueryObject(models\Provider $provider, $personType, CI_Input $input,
                                   Doctrine\ORM\EntityManager $entityManager, $offset, $limit) {
        $this->input = $input;

        list($query, $parameters) = $this->getSearchQueryAndParameters($provider, $personType);

        $log = Logger::getLogger(__CLASS__);
        $log->debug("searchQuery: $query");
        $log->debug("searchParameters: " . print_r($parameters, true));

        return $entityManager->createQuery($query)
            ->setParameters($parameters)
            ->setFirstResult($offset)
            ->setMaxResults($limit);
    }

    private function getSearchQueryAndParameters(models\Provider $provider, $personType) {
        list($fields, $values) = $this->prepareQueryParameters($provider, $personType);

        $selectClause = $this->getSelectClause();
        $fromClause = $this->getFromClause($personType);
        $whereClause = $this->getWhereClause($fields, $values);
        $orderByClause = $this->getOrderByClause();

        return array("$selectClause $fromClause $whereClause $orderByClause", $values);
    }

    private function prepareQueryParameters(models\Provider $provider, $personType) {
        $fields = array();
        $values = array();

        $this->prepareQueryParameterProvider($provider, $fields, $values);
        $this->prepareQueryParameterNameStartsWith($fields, $values);
        $this->prepareQueryParameterFilterByNameOrRegNo($fields, $values);
        if ($personType == self::PERSON_TYPE_PATIENT) {
            $this->prepareQueryParameterCategory($fields, $values);
        }

        return array($fields, $values);
    }

    private function prepareQueryParameterProvider(models\Provider $provider, &$fields, &$values) {
        $fields[self::SEARCH_FIELD_PROVIDER] = self::DQL_PERSON_OBJECT_NAME . "." . self::DB_FIELD_PROVIDER
            . " = :" . self::SEARCH_FIELD_PROVIDER;
        $values[self::SEARCH_FIELD_PROVIDER] = $provider;
    }

    private function prepareQueryParameterNameStartsWith(&$fields, &$values) {
        $nameStartsWith = trim($this->input->get(self::SEARCH_FIELD_NAME_STARTS_WITH));
        if ($nameStartsWith) {
            $fields[self::SEARCH_FIELD_NAME_STARTS_WITH] = self::DQL_PERSON_OBJECT_NAME . "." . self::DB_FIELD_NAME
                . " LIKE :" . self::SEARCH_FIELD_NAME_STARTS_WITH;
            $values[self::SEARCH_FIELD_NAME_STARTS_WITH] = $nameStartsWith . "%";
        }
    }

    private function prepareQueryParameterFilterByNameOrRegNo(&$fields, &$values) {
        $filterBy = $this->input->get(self::SEARCH_FIELD_FILTER_BY);
        $filterText = trim($this->input->get(self::SEARCH_FIELD_FILTER_TEXT));
        if ($filterBy && $filterText) {
            switch ($filterBy) {
                case self::FILTER_BY_NAME:
                    $fields[self::SEARCH_FIELD_FILTER_BY] = self::DQL_PERSON_OBJECT_NAME . "." . self::DB_FIELD_NAME
                        . " LIKE :" . self::SEARCH_FIELD_FILTER_BY;
                    $values[self::SEARCH_FIELD_FILTER_BY] = "%$filterText%";
                    break;
                case self::FILTER_BY_REG_NO:
                    if (strpos($filterText, models\Person::ID_PREFIX) === 0) {
                        $filterText = substr($filterText, strlen(models\Person::ID_PREFIX));
                    }
                    $filterText = (int) $filterText;

                    $fields[self::SEARCH_FIELD_FILTER_BY] = self::DQL_PERSON_OBJECT_NAME . "." . self::DB_FIELD_ID
                        . " = :" . self::SEARCH_FIELD_FILTER_BY;
                    $values[self::SEARCH_FIELD_FILTER_BY] = $filterText;
                    break;
                case self::FILTER_BY_EMAIL:
                    $fields[self::SEARCH_FIELD_FILTER_BY] = self::DQL_PERSON_OBJECT_NAME . "." . self::DB_FIELD_EMAIL
                        . " = :" . self::SEARCH_FIELD_FILTER_BY;
                    $values[self::SEARCH_FIELD_FILTER_BY] = $filterText;
                    break;
            }
        }
    }

    private function prepareQueryParameterCategory(&$fields, &$values) {
        $category = $this->input->get(self::SEARCH_FIELD_CATEGORY);
        if ($category) {
            $fields[self::SEARCH_FIELD_CATEGORY] = self::DQL_PERSON_OBJECT_NAME . "." . self::DB_FIELD_CATEGORY
                . " = :" . self::SEARCH_FIELD_CATEGORY;
            $values[self::SEARCH_FIELD_CATEGORY] = $category;
        }
    }

    private function getSelectClause() {
        return "SELECT " . self::DQL_PERSON_OBJECT_NAME;
    }

    private function getFromClause($personType) {
        switch ($personType) {
            case self::PERSON_TYPE_ADMIN:
                $personClass = "Admin";
                break;
            case self::PERSON_TYPE_DOCTOR:
                $personClass = "Doctor";
                break;
            case self::PERSON_TYPE_PATIENT:
                $personClass = "Patient";
                break;
            case self::PERSON_TYPE_ALL:
                $personClass = "Person";
                break;
            default:
                $personClass = null;
        }

        return "FROM models\\$personClass " . self::DQL_PERSON_OBJECT_NAME;
    }

    private function getWhereClause(array $fields) {
        return count($fields) > 0 ? "WHERE " . implode(" AND ", $fields) : "";
    }

    private function getOrderByClause() {
        $sortBy = self::DQL_PERSON_OBJECT_NAME . "."
            . ($this->input->get(self::SEARCH_FIELD_SORT_BY) ? : self::DB_FIELD_NAME);
        $sortOrder = $this->input->get(self::SEARCH_FIELD_SORT_ORDER) ? : self::SORT_ORDER_ASC;

        return "ORDER BY $sortBy $sortOrder";
    }
}
