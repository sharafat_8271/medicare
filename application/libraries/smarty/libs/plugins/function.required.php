<?php
/**
 * Smarty {required} function plugin
 *
 * File:     function.required.php
 * Type:     function
 * Name:     required
 * Purpose:  Generate a required asterisk for a form field using a provided .required css style
 *
 * @author: Sharafat Ibn Mollah Mosharraf (sharafat underscore 8271 at yahoo dot co dot uk)
 *
 * @return string
 */
function smarty_function_required() {
    return "<span class='required'>*</span>";
}
