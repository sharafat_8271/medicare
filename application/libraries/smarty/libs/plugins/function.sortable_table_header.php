<?php
/**
 * Smarty {sortable_table_header} function plugin
 *
 * File:     function.sortable_table_header.php
 * Type:     function
 * Name:     sortable_table_header
 * Purpose:  Put sortable icon in column header
 *
 * @author: Sharafat Ibn Mollah Mosharraf (sharafat underscore 8271 at yahoo dot co dot uk)
 *
 * @param array                    $params   parameters (field)
 * @param Smarty_Internal_Template $template template object
 *
 * @return bool
 */
/** @noinspection PhpInconsistentReturnPointsInspection */
function smarty_function_sortable_table_header(array $params, Smarty_Internal_Template $template) {
    if (!array_key_exists("field", $params)) {
        trigger_error("sortable_table_header: missing 'field' parameter", E_USER_WARNING);
        return false;
    }
    if (!array_key_exists("text", $params)) {
        trigger_error("sortable_table_header: missing 'text' parameter", E_USER_WARNING);
        return false;
    }

    $sortClass = "unsorted";
    $nextSortOrder = "asc";
    $sortTitle = "Sort Ascending";

    $sortBy = $template->tpl_vars['sortBy'];
    $sortOrder = $template->tpl_vars['sortOrder'];
    $field = $params['field'];

    if ($field == "dob") { //special case as dob is presented as 'age' whose sort order is the exact opposite of dob.
        if ($sortBy == $field) {
            if ($sortOrder == "desc") {
                $sortClass = "sorted-asc";
                $sortTitle = "Sort Descending";
            } else {
                $nextSortOrder = "desc";
                $sortClass = "sorted-desc";
            }
        } else {
            $nextSortOrder = "desc";
        }
    } elseif ($sortBy == $field) {
        if ($sortOrder == "desc") {
            $sortClass = "sorted-desc";
        } else {
            $sortClass = "sorted-asc";
            $nextSortOrder = "desc";
            $sortTitle = "Sort Descending";
        }
    }

    $requestUrl = $_SERVER['REQUEST_URI'];
    $urlWithoutSortByAndSortOrderParams = preg_replace(array('/' . Search::SEARCH_FIELD_SORT_BY . '=.*&?/',
                                                             '/' . Search::SEARCH_FIELD_SORT_ORDER . '=.*&?/',
                                                             '/&$/',    //remove trailing &
                                                             '/\?$/'),  //remove trailing ?
                                                       array('', '', '', ''), $requestUrl);
    $sortUrl = $urlWithoutSortByAndSortOrderParams
        . (strstr($urlWithoutSortByAndSortOrderParams, '?') !== false ? '&' : '?')
        . Search::SEARCH_FIELD_SORT_BY . '=' . $field . '&' . Search::SEARCH_FIELD_SORT_ORDER . '=' . $nextSortOrder;

    return <<<TH
    <th class="$sortClass">
        <a href="$sortUrl" title="$sortTitle">{$params['text']}</a>
    </th>
TH;
}
