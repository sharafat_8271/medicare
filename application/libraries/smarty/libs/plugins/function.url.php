<?php
/**
 * Smarty {url} function plugin
 *
 * File:     function.url.php
 * Type:     function
 * Name:     url
 * Purpose:  Generate the base URL or the URL for a path or controller-action-id in CodeIgniter
 *
 * @author: Sharafat Ibn Mollah Mosharraf (sharafat underscore 8271 at yahoo dot co dot uk)
 *
 * @param array                    $params   parameters (empty or 'path' or 'controller'[, 'action', ['id']])
 * @param Smarty_Internal_Template $template template object
 *
 * @return string
 */
require BASEPATH . "helpers/url_helper.php";

function smarty_function_url($params, $template = null) {
    $url = rtrim(base_url(), '/');

    if (segmentExists('path', $params)) {
        $url = appendSegment('path', $params, $url);
    } else {
        if (segmentExists('controller', $params)) {
            $url = appendSegment('controller', $params, $url);
            $url = appendSegment('action', $params, $url);
            $url = appendSegment('id', $params, $url);
        }
    }

    return $url;
}

function segmentExists($segmentName, $params) {
    return in_array($segmentName, array_keys($params)) && !empty($params[$segmentName]);
}

function appendSegment($segmentName, $params, $url) {
    if (segmentExists($segmentName, $params)) {
        $url .= ("/") . $params[$segmentName];
    }

    return $url;
}
