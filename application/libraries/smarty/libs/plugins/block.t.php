<?php
/**
 * Author:  sharafat
 * Created: 8/31/12 12:50 AM
 */
//include APPPATH . "libraries/i18n.php";

function smarty_block_t($params, $content) {
    if (is_null($content)) {
        return null;
    }

    return count($params) > 0 ? I18N::getInstance()->_($content, $params) : I18N::getInstance()->_($content);
}
