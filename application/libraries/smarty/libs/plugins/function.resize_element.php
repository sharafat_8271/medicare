<?php
/**
 * Smarty {resize_element} function plugin
 *
 * File:     function.resize_element.php
 * Type:     function
 * Name:     resize_element
 * Purpose:  Resize an HTML element to fit a size maintaining original aspect ratio
 *
 * @author: Sharafat Ibn Mollah Mosharraf (sharafat underscore 8271 at yahoo dot co dot uk)
 *
 * @param array                    $params   parameters (actualWidth, actualHeight, targetWidth, targetHeight, var,
 *                                                       [forceScale])
 *                                           Pass -1 as argument to targetWidth or targetHeight to ignore it.
 * @param Smarty_Internal_Template $template template object
 *
 * @return bool
 */
/** @noinspection PhpInconsistentReturnPointsInspection */
function smarty_function_resize_element(array $params, Smarty_Internal_Template $template) {
    $requiredParams = array('actualWidth', 'actualHeight', 'targetWidth', 'targetHeight', 'var');
    foreach ($requiredParams as $requiredParam) {
        if (!array_key_exists($requiredParam, $params)) {
            resizeElementTriggerError($requiredParam);
            return false;
        }
    }

    $forceScale = true;
    if (array_key_exists('forceScale', $params)) {
        $forceScale = $params['forceScale'];
    }

    if ($params['targetWidth'] > -1 && $params['targetHeight'] > -1) {  //ignore both width & height
        $ratioWidth = $params['targetWidth'] / $params['actualWidth'];
        $ratioHeight = $params['targetHeight'] / $params['actualHeight'];
        $ratio = $ratioWidth < $ratioHeight ? $ratioWidth : $ratioHeight;
    } else if ($params['targetWidth'] == -1) {  //ignore width
        $ratioHeight = $params['targetHeight'] / $params['actualHeight'];
        $ratio = $ratioHeight;
    } else if ($params['targetHeight'] == -1) { //ignore height
        $ratioWidth = $params['targetWidth'] / $params['actualWidth'];
        $ratio = $ratioWidth;
    } else {
        $ratio = 1;
    }

    if ($ratio < 1 || $forceScale) {
        $newWidth = $params['actualWidth'] * $ratio;
        $newHeight = $params['actualHeight'] * $ratio;
    } else {
        $newWidth = $params['actualWidth'];
        $newHeight = $params['actualHeight'];
    }

    $template->assign($params['var'], array($newWidth, $newHeight));
}

function resizeElementTriggerError($parameterName) {
    trigger_error("resize_element: missing '$parameterName' parameter", E_USER_WARNING);
}
