<?php
/**
 * Smarty {boolean} function plugin
 *
 * File:     function.boolean.php
 * Type:     function
 * Name:     boolean
 * Purpose:  Generate an HTML tick or cross mark as a boolean expression output.
 *
 * @author: Sharafat Ibn Mollah Mosharraf (sharafat underscore 8271 at yahoo dot co dot uk)
 *
 * @return string
 */
function smarty_function_boolean($params) {
    if (!array_key_exists('value', $params)) {
        trigger_error("boolean: missing 'value' parameter", E_USER_WARNING);
        return;
    }

    $emptyOnFalse = false;
    if (array_key_exists('emptyOnFalse', $params)) {
        $emptyOnFalse = $params['emptyOnFalse'];
    }

    if ($params['value'] === true) {
        return "&#10004;";
    } elseif ($params['value'] === false) {
        return $emptyOnFalse ? "" : "&#10007;";
    } else {
        return "";
    }
}

