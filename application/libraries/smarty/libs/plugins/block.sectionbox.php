<?php
/**
 * Author:  sharafat
 * Created: 8/31/12 12:50 AM
 */

function smarty_block_sectionbox($params, $content) {
    if (is_null($content)) {
        return null;
    }

    $title = array_key_exists('title', $params) ? $params['title'] : "";
    $titleActions = array_key_exists('titleActions', $params) ? $params['titleActions'] : "";
    $attributes = array_key_exists('attrs', $params) ? $params['attrs'] : "";
    $roundedCorners = array_key_exists('rounded', $params) ? $params['rounded'] : true;
    $fullWidth = array_key_exists('fullwidth', $params) ? $params['fullwidth'] : false;
    $shadow = array_key_exists('shadow', $params) ? $params['shadow'] : false;
    $clazz = "section-box" . ($fullWidth ? "-full-width" : "");
    $additionalClasses = array_key_exists('class', $params) ? $params['class'] : "";
    $additionalClasses .= $roundedCorners ? " rounded-corners" : "";
    $additionalClasses .= $shadow ? " section-box-shadow" : "";
    $boxHeaderRoundedCornerClass = $roundedCorners ? "rounded-corners-top" : "";

    return <<<A
<div class="$clazz $additionalClasses" $attributes>
    <div class="section-box-header $boxHeaderRoundedCornerClass">
        <span style="float:left">$title</span>
        <span style="float:right; font-weight: normal">$titleActions</span>
    </div>
    <div class="section-box-body">
        $content
    </div>
</div>
A;
}
