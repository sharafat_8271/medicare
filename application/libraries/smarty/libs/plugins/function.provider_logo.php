<?php

require_once 'function.resize_element.php';

/**
 * Smarty {provider_logo} function plugin
 *
 * File:     function.provider_logo.php
 * Type:     function
 * Name:     provider_logo
 * Purpose:  Returns image file path of provider logo with optional resizing
 *
 * @author: Sharafat Ibn Mollah Mosharraf (sharafat underscore 8271 at yahoo dot co dot uk)
 *
 * @param array                    $params   parameters (var, [targetWidth], [targetHeight])
 * @param Smarty_Internal_Template $template template object
 *
 * @return bool
 */
function smarty_function_provider_logo(array $params, Smarty_Internal_Template $template) {
    if (!array_key_exists('var', $params)) {
        trigger_error("provider_logo: missing 'var' parameter", E_USER_WARNING);
        return false;
    }

    $targetWidth = -1;
    if (array_key_exists('targetWidth', $params)) {
        $targetWidth = $params['targetWidth'];
    }

    $targetHeight = -1;
    if (array_key_exists('targetHeight', $params)) {
        $targetHeight = $params['targetHeight'];
    }

    /** @var $provider models\Provider */
    $provider = $template->getTemplateVars('session')->userdata('login')->getProvider();

    if ($provider->getLogoFileName() == "") {
        $template->assign($params['var'], array('url'    => "", 0 => "",
                                                'width'  => 0, 1 => 0,
                                                'height' => 0, 2 => 0));
        return true;
    }

    $logoUrl = smarty_function_url(array('path' => 'public/provider_logos/' . $provider->getLogoFileName()));
    $logoDimension = getimagesize($logoUrl);

    smarty_function_resize_element(array('actualWidth'  => $logoDimension[0],
                                         'actualHeight' => $logoDimension[1],
                                         'targetWidth'  => $targetWidth,
                                         'targetHeight' => $targetHeight,
                                         'var'          => 'resizedLogoDimension',
                                         'forceResize'  => true),
                                   $template);
    $resizedLogoDimension = $template->getTemplateVars('resizedLogoDimension');
    $newWidth = $resizedLogoDimension[0];
    $newHeight = $resizedLogoDimension[1];

    $template->assign($params['var'], array('url'    => $logoUrl,
                                            0        => $logoUrl,
                                            'width'  => $newWidth,
                                            1        => $newWidth,
                                            'height' => $newHeight,
                                            2        => $newHeight));
}
