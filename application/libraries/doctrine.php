<?php
use Doctrine\Common\ClassLoader,
Doctrine\ORM\Configuration,
Doctrine\ORM\EntityManager,
Doctrine\ORM\Mapping\Driver\DatabaseDriver,
Doctrine\ORM\Tools\EntityGenerator,
Doctrine\ORM\Tools\DisconnectedClassMetadataFactory,
Doctrine\Common\Cache\ArrayCache;

class Doctrine {

    private $entityManager = null;

    public function __construct() {
        // load database configuration from CodeIgniter
        require_once APPPATH . 'config/database.php';

        // Set up class loading. You could use different autoloaders, provided by your favorite framework,
        // if you want to.
        require_once APPPATH . 'libraries/Doctrine/Common/ClassLoader.php';

        $doctrineClassLoader = new ClassLoader('Doctrine', APPPATH . 'libraries');
        $doctrineClassLoader->register();
        $entitiesClassLoader = new ClassLoader('models', rtrim(APPPATH, "/"));
        $entitiesClassLoader->register();
        $proxiesClassLoader = new ClassLoader('Proxies', FCPATH . 'cache/proxies');
        $proxiesClassLoader->register();

        // Set up caches
        $config = new Configuration;
        $cache = new ArrayCache;
        $config->setMetadataCacheImpl($cache);
        $driverImpl = $config->newDefaultAnnotationDriver(array(APPPATH . 'models'));
        $config->setMetadataDriverImpl($driverImpl);
        $config->setQueryCacheImpl($cache);

        $config->setQueryCacheImpl($cache);

        // Proxy configuration
        $config->setProxyDir(FCPATH . 'cache/proxies');
        $config->setProxyNamespace('Proxies');
        $config->setAutoGenerateProxyClasses(TRUE);

        // Set up logger
        require_once APPPATH . 'libraries/Log4PhpSqlLogger.php';
        $logger = new Log4PhpSqlLogger();
        $config->setSQLLogger($logger);

        // Database connection information
        /** @var $db array */
        /** @var $active_group string */
        $connectionOptions = array(
            'driver' => 'pdo_mysql',
            'user' => $db[$active_group]['username'],
            'password' => $db[$active_group]['password'],
            'host' => $db[$active_group]['hostname'],
            'dbname' => $db[$active_group]['database']
        );

        // Create EntityManager
        $this->entityManager = EntityManager::create($connectionOptions, $config);

//        $this->generateSchemaFromEntityClasses();
//        $this->generateEntityClassesFromSchema();
    }

    /**
     * generate DB schema automatically from entity classes
     */
    private function generateSchemaFromEntityClasses() {
        $classes = array();
        foreach (glob(APPPATH . "models/*.php") as $file) {
            $classes[] = $this->entityManager->getClassMetadata("models\\" . basename($file, ".php"));
        }

        $tool = new \Doctrine\ORM\Tools\SchemaTool($this->entityManager);
        $tool->updateSchema($classes);
    }

    /**
     * generate entity classes automatically from db tables
     */
    private function generateEntityClassesFromSchema() {
        $this->entityManager->getConfiguration()->setMetadataDriverImpl(
            new DatabaseDriver($this->entityManager->getConnection()->getSchemaManager())
        );

        $cmf = new DisconnectedClassMetadataFactory();
        $cmf->setEntityManager($this->entityManager);
        $metadata = $cmf->getAllMetadata();
        $generator = new EntityGenerator();

        $generator->setUpdateEntityIfExists(true);
        $generator->setGenerateStubMethods(true);
        $generator->setGenerateAnnotations(true);
        $generator->generate($metadata, APPPATH . "models/Entities");
    }

    public function getEntityManager() {
        return $this->entityManager;
    }
}
