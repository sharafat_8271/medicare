<?php
/**
 * Author:  sharafat
 * Created: 1/24/13 7:45 AM
 */
class ProtocolService {

    public function getProtocols(models\Patient $patient, DateTime $from = null, DateTime $to = null) {
        $protocols = $patient->getProtocols();
        $filteredProtocols = array();

        /** @var $protocol models\Protocol */
        foreach ($protocols as $protocol) {
            if (!isDateTimeWithinRange($protocol->getProtocolDate(), $from, $to)) {
                continue;
            }

            $filteredProtocols[] = $protocol;
        }

        usort($filteredProtocols, function(models\Protocol $lhs, models\Protocol $rhs) {
            if ($lhs->getProtocolDate() > $rhs->getProtocolDate()) {
                return 1;
            } elseif ($lhs->getProtocolDate() < $rhs->getProtocolDate()) {
                return -1;
            } else {
                if ($lhs->getCycle() > $rhs->getCycle()) {
                    return 1;
                } elseif ($lhs->getCycle() < $rhs->getCycle()) {
                    return -1;
                } else {
                    if ($lhs->getDay() > $rhs->getDay()) {
                        return 1;
                    } elseif ($lhs->getDay() > $rhs->getDay()) {
                        return -1;
                    } else {
                        return 0;
                    }
                }
            }
        });

        return $filteredProtocols;
    }

}
