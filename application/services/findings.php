<?php
/**
 * Author:  sharafat
 * Created: 1/22/13 10:05 PM
 */
class FindingsService {

    public function getGroupedFindings(models\Patient $patient, DateTime $from = null, DateTime $to = null) {
        $findings = array();

        /** @var $finding models\Findings */
        foreach ($patient->getFindings() as $finding) {
            if (!isDateTimeWithinRange($finding->getFindingsDate(), $from, $to)) {
                continue;
            }

            if (models\Findings::getGroupByType($finding->getType()) == models\Findings::GENERAL_EXAM) {
                $findings[models\Findings::GENERAL_EXAM][$finding->getType()][] = $finding;
            } else {
                $findings[$finding->getType()][] = $finding;
            }
        }

        if (array_key_exists(models\Findings::GENERAL_EXAM, $findings)) {
            ksort($findings[models\Findings::GENERAL_EXAM]);
        }

        ksort($findings);

        return $findings;
    }

}
