<?php
/**
 * Author:  sharafat
 * Created: 1/22/13 10:59 PM
 */
class InvestigationService {

    public function getInvestigations(models\Patient $patient, DateTime $from = null, DateTime $to = null) {
        $investigations = $this->getGroupedInvestigations($patient, $from, $to);
        $this->sortInvestigationsByDate($investigations);

        return $investigations;
    }

    private function getGroupedInvestigations(models\Patient $patient, DateTime $from = null, DateTime $to = null) {
        $investigations = array();

        /** @var $investigation models\Investigation */
        foreach ($patient->getInvestigations() as $investigation) {
            if (!isDateTimeWithinRange($investigation->getInvestigationDate(), $from, $to)) {
                continue;
            }

            $investigations[models\Investigation::getGroupByType($investigation->getType())][$investigation->getType()][]
                = $investigation;
        }

        return $investigations;
    }

    private function sortInvestigationsByDate(&$investigations) {
        foreach ($investigations as $investigationGroup => $investigationTypes) {
            switch ($investigationGroup) {
                case models\Investigation::CBC:
                    $investigations[models\Investigation::CBC] = $this->sortCbcOrSElectrolyte($investigationTypes);
                    break;
                case models\Investigation::S_ELECTROLYTE:
                    $investigations[models\Investigation::S_ELECTROLYTE] = $this->sortCbcOrSElectrolyte($investigationTypes);
                    break;
                default:
                    $this->sortByInvestigationDate($investigationTypes);
            }
        }
    }

    private function sortCbcOrSElectrolyte($investigationGroup) {
        foreach ($investigationGroup as $investigationTypes) {
            foreach ($investigationTypes as $cbcOrSElectrolyte) {
                $cbcOrSElectrolyteRecords[$cbcOrSElectrolyte->getInvestigationDate(true)][$cbcOrSElectrolyte->getType()]
                    = $cbcOrSElectrolyte;
            }
        }

        uksort($cbcOrSElectrolyteRecords, function($a, $b) {
            $dateTimeA = new DateTime($a);
            $dateTimeB = new DateTime($b);

            if ($dateTimeA > $dateTimeB) {
                return 1;
            } elseif ($dateTimeA < $dateTimeB) {
                return -1;
            } else {
                return 0;
            }
        });

        return $cbcOrSElectrolyteRecords;
    }

    private function sortByInvestigationDate($investigationGroup) {
        foreach ($investigationGroup as $investigationTypes) {
            usort($investigationTypes, function(models\Investigation $a, models\Investigation $b) {
                if ($a->getInvestigationDate() > $b->getInvestigationDate()) {
                    return 1;
                } elseif ($a->getInvestigationDate() < $b->getInvestigationDate()) {
                    return -1;
                } else {
                    if ($a->getPostedOn() > $b->getPostedOn()) {
                        return 1;
                    } elseif ($a->getPostedOn() < $b->getPostedOn()) {
                        return -1;
                    } else {
                        return 0;
                    }
                }
            });
        }
    }
}
