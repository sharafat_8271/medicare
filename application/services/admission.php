<?php
/**
 * Author:  sharafat
 * Created: 1/23/13 12:07 AM
 */
class AdmissionService {

    public function getSeparateAdmissionDischargeHistory($patient, DateTime $from = null, DateTime $to = null) {
        $admissionDischargeHistory = $patient->getAdmissionDischargeHistory();

        $admissionHistory = array();
        $dischargeHistory = array();
        /** @var $admissionDischarge models\AdmissionDischarge */
        foreach ($admissionDischargeHistory as $admissionDischarge) {
            if (!isDateTimeWithinRange($admissionDischarge->getEventDate(), $from, $to)) {
                continue;
            }

            if ($admissionDischarge->getEventType() == \models\AdmissionDischarge::ADMISSION) {
                $admissionHistory[] = $admissionDischarge;
            } else {
                $dischargeHistory[] = $admissionDischarge;
            }
        }

        return array($admissionHistory, $dischargeHistory);
    }

}
