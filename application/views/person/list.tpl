{extends file="templates/loggedInUser.tpl"}

{block name="title"}{t}{$personType}.list{/t}{/block}

{block name="head_elements"}
<script type="text/javascript">
    $(function () {
        $(".styleTable").styleTable();
    });
</script>
{/block}

{block name="loggedInBody"}

    {if $session->flashdata('person.activated') || $session->flashdata('person.deactivated')}
        <div class="text-align-center">
            <div class="success-msg shrink-wrap">
                {capture name="flashMsg"}
                    {if $session->flashdata('person.activated')}
                        {t}activated.user{/t}
                    {elseif $session->flashdata('person.deactivated')}
                        {t}deactivated.user{/t}
                    {/if}
                {/capture}
                {$smarty.capture.flashMsg|sprintf:{$session->flashdata('affected.person.id')}}
            </div>
        </div>
    {/if}

    {sectionbox title="{t}{$personType}.list{/t}" fullwidth=true}

    {if $personType == 'person'}
        {$personController = 'administer'}
    {else}
        {$personController = $personType}
    {/if}

    <div style="text-align: center">
        {php}echo form_open('{$personController}/list', array('method' => 'get'));{/php}
            {t}filter.by{/t}:
            <select name="{Search::SEARCH_FIELD_FILTER_BY}">
                <option value="{Search::FILTER_BY_NAME}">{t}name{/t}</option>
                <option value="{Search::FILTER_BY_REG_NO}"
                    {if array_key_exists(Search::SEARCH_FIELD_FILTER_BY, $smarty.get)
                        && $smarty.get.{Search::SEARCH_FIELD_FILTER_BY} == Search::FILTER_BY_REG_NO}
                        selected='selected'
                    {/if}>{t}reg.no{/t}</option>
                <option value="{Search::FILTER_BY_EMAIL}"
                        {if array_key_exists(Search::SEARCH_FIELD_FILTER_BY, $smarty.get)
                && $smarty.get.{Search::SEARCH_FIELD_FILTER_BY} == Search::FILTER_BY_EMAIL}
                    selected='selected'
                        {/if}>{t}email{/t}</option>
            </select>
            <input type="text" name="{Search::SEARCH_FIELD_FILTER_TEXT}"
                {if array_key_exists(Search::SEARCH_FIELD_FILTER_TEXT, $smarty.get)}
                   value="{$smarty.get.{Search::SEARCH_FIELD_FILTER_TEXT}}"
                {/if}/>
            <input type="submit" value="{t}filter{/t}"/>
        </form>

        {for $i = 65; $i <= 90; $i++}
            <a href="{url controller=$personController action='list'}?{Search::SEARCH_FIELD_NAME_STARTS_WITH}={$i|chr}">{$i|chr}</a>
            {if $i < 90}|{/if}
        {/for}

        <hr/>

        <div style="padding-bottom: 10px">
            {if count($personList) > 0}
                {t m={$startSerial} n={$endSerial} total={$personList|count}}showing.results.m.of.n{/t}<br/>
                {else}
                {t}no.results.found{/t}
            {/if}
        </div>

        {if count($personList) > 0}
            {$pagination}

            <table class="styleTable" style="margin: 25px auto">
                {if $personType == 'person'}
                    {include file='administer/list.tpl'}
                {else}
                    {include file=$personType|cat:'/list.tpl'}
                {/if}
            </table>

            {$pagination}
        {/if}

    </div>

    {/sectionbox}

{/block}
