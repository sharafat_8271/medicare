{extends file="templates/loggedInUser.tpl"}

{block name="title"}{t}{$personType}.profile{/t}{/block}

{block name="loggedInBody"}

    {if $session->flashdata('profile.updated') || $session->flashdata('password.updated')}
    <div class="text-align-center">
        <div class="success-msg shrink-wrap">
            {if $session->flashdata('profile.updated')}
                {t}profile.updated{/t}
                {elseif $session->flashdata('password.updated')}
                {t}password.updated{/t}
            {/if}
        </div>
    </div>
    {/if}

    {function name=editModeContent}
    <a href="{url controller=$personType action='edit' id=$person->getId()}">
        <img src="{url path='public/images/edit.png'}" alt="{t}edit{/t}" title="{t}edit{/t}" style="vertical-align: bottom;"/>
        {t}edit{/t}
    </a>
    {/function}

    {sectionbox title="{t}profile{/t}" fullwidth=true titleActions={editModeContent}}
    <table class="details th-min-width">
        {if $personType == 'doctor'}
            <tr>
                <th>{t}degree{/t}</th>
                <td>{$person->getDegree()|nl2br}</td>
            </tr>
        {/if}
        <tr>
            <th>{t}sex{/t}</th>
            <td>{t}{Person::msgKey($person->getSex())}{/t}</td>
        </tr>
        <tr>
            <th>{t}dob{/t}</th>
            <td>{$person->getDob(true)}</td>
        </tr>
        <tr>
            <th>{t}age{/t}</th>
            <td>{$person->getYearsInAge()} {t}years{/t}
                {$person->getMonthsInAge()} {t}months{/t} {$person->getDaysInAge()} {t}days{/t}</td>
        </tr>
        <tr>
            <th>{t}religion{/t}</th>
            <td>
                {if $person->getReligion() != ""}
                    {t}{Person::msgKey($person->getReligion())}{/t}
                {/if}
            </td>
        </tr>
        <tr>
            <th>{t}mobile{/t}</th>
            <td>{$person->getMobile()}</td>
        </tr>
        <tr>
            <th>{t}email{/t}</th>
            <td>{$person->getEmail()}</td>
        </tr>
        <tr>
            <th>{t}address{/t}</th>
            <td>{$person->getAddress()|nl2br}</td>
        </tr>
        <tr>
            <th>{t}admin{/t}</th>
            <td>{boolean value=$person->isAdmin()}</td>
        </tr>
    </table>
    {/sectionbox}

    {sectionbox title="{t}additional.info{/t}" class="margin-top" fullwidth=true}
    <table class="details th-min-width">
        <tr>
            <th>{t}registered.on{/t}</th>
            <td>{$person->getRegistrationDateTime(true)}</td>
        </tr>
        <tr>
            <th>{t}registered.by{/t}</th>
            <td>
                {if $person->getRegisteredBy() != null}
                    {if is_a($person->getRegisteredBy(), 'models\Doctor')}
                        {$controller = 'doctor'}
                    {else}
                        {$controller = 'admin'}
                    {/if}
                    <a href="{url controller=$controller action='details' id=$person->getRegisteredBy()->getId()}">
                        {$person->getRegisteredBy()->getName()}</a>
                {/if}
            </td>
        </tr>
    </table>
    {/sectionbox}

    {if $session->userdata('login')->getId() == $person->getId()}
        <div class="margin-top"></div>
        {include file="person/password_update.tpl" from="person/details"}
    {/if}

{/block}
