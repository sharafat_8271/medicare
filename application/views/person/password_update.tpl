{php}echo form_open('password/update');{/php}

{if $from == "password/resetPass"}
    {$sectionTitle='reset.password'}
{else}
    {$sectionTitle='change.password'}
{/if}

<div class="section-box-full-width rounded-corners">
    <div class="section-box-header rounded-corners-top">{t}{$sectionTitle}{/t}</div>
    <div class="section-box-body">
        <table class="details th-min-width">
            {if $from != "password/resetPass"}
                <tr>
                    <th>{t}current.password{/t} {required}</th>
                    <td><input type="password" name="currentPassword"/></td>
                    <td>{php}echo form_error('currentPassword'); echo form_error('verifyCurrentPasswordIsCorrect');{/php}</td>
                </tr>
            {/if}
            <tr>
                <th>{t}new.password{/t} {required}</th>
                <td><input type="password" name="newPassword"/></td>
                <td>{php}echo form_error('newPassword');{/php}</td>
            </tr>
            <tr>
                <th>{t}retype.new.password{/t} {required}</th>
                <td><input type="password" name="retypeNewPassword"/></td>
                <td>{php}echo form_error('retypeNewPassword');{/php}</td>
            </tr>
        </table>
    </div>
    <div class="section-box-footer rounded-corners-bottom">
        <input type="hidden" name="id" value="{$person->getId()}"/>
        <input type="hidden" name="from" value="{$from}"/>
        <input type="submit" name="submit" value="{t}{$sectionTitle}{/t}"/>
        {if $from == "password/resetPass"}
            <span style="float:right">
                <input type="button" value="{t}cancel{/t}"
                       onclick="window.location='{url controller='administer' action='list'}'"/>
            </span>
        {/if}
    </div>
</div>

</form>
