{extends file="templates/loggedInUser.tpl"}

{block name="title"}
    {if $person->getId() > 0}
        {$pageTitle = "edit.$personType"}
        {$action = 'update'}
        {else}
        {$pageTitle = "register.$personType"}
        {$action = 'add'}
    {/if}

    {t}{$pageTitle}{/t}
{/block}

{block name="head_elements"}
<script>
    $(function () {
        setEqualWidth(filterSameWidthInclusiveElements($("input[type='text'], input[type='password'], textarea, select")));

        $("#ageYears").change(setDob);
        $("#ageMonths").change(setDob);
        $("#ageDays").change(setDob);
    });

    function setDob() {
        var dob = new Date();
        dob.setTime(dob.getTime() -
                (($("#ageYears").val() * 365 + $("#ageMonths").val() * 30 + parseInt($("#ageDays").val())) * 24 * 3600 * 1000));
        var dobString = dob.getFullYear() + "-" + padZero(dob.getMonth() + 1) + "-" + padZero(dob.getDate());
        $("#dob").val(dobString);
    }

    function padZero(value) {
        return value < 10 ? "0" + value : value;
    }
</script>
<style type="text/css">
    textarea {
        width: auto;
        height: auto;
    }
</style>
{/block}

{block name="loggedInBody"}

    {php}echo form_open('{$personType}/{$action}');{/php}

<div class="section-box-full-width rounded-corners">
    <div class="section-box-header rounded-corners-top">{t}{$pageTitle}{/t}</div>
    <div class="section-box-body">
        <table>
            <tr>
                <th>{t}full.name{/t} {required}</th>
                <td>
                    <input type="text" name="name" value="{php}echo set_val('name', '{$person->getName()}');{/php}"/>
                </td>
                <td>{php}echo form_error('name');{/php}</td>
            </tr>
            <tr>
                <th>{t}sex{/t} {required}</th>
                <td>
                    <label>
                    <input type="radio" name="sex" value="{Person::MALE}"
                           {php}echo set_radio('sex', {Person::MALE}, '{$person->getSex()}' == {Person::MALE});{/php}/> {t}male{/t}
                    </label>
                    <label>
                    <input type="radio" name="sex" value="{Person::FEMALE}"
                           {php}echo set_radio('sex', {Person::FEMALE}, '{$person->getSex()}' == {Person::FEMALE});{/php}/> {t}female{/t}
                    </label>
                    <label>
                    <input type="radio" name="sex" value="{Person::OTHER}"
                           {php}echo set_radio('sex', {Person::OTHER}, '{$person->getSex()}' == {Person::OTHER});{/php}/> {t}other{/t}
                    </label>
                </td>
                <td>{php}echo form_error('sex');{/php}</td>
            </tr>
            <tr>
                <th>{t}dob{/t} {required}</th>
                <td>
                    <input type="text" id="dob" name="dob" class="date" size="10" readonly="true"
                           value="{php}echo set_val('dob', '{$person->getDob(true, "Y-m-d")}');{/php}"/>
                </td>
                <td>{php}echo form_error('dob');{/php}</td>
            </tr>
            <tr>
                <th>{t}age{/t}</th>
                <td>
                    <select id="ageYears" name="ageYears" class="no-same-width">
                        {for $i = 0 to 200}
                            <option value="{$i}" {php}echo set_selct('ageYears', {$i});{/php}>{$i}</option>
                        {/for}
                    </select>
                    {t}years{/t}
                    <select id="ageMonths" name="ageMonths" class="no-same-width">
                        {for $i = 0 to 11}
                            <option value="{$i}" {php}echo set_selct('ageMonths', {$i});{/php}>{$i}</option>
                        {/for}
                    </select>
                    {t}months{/t}
                    <select id="ageDays" name="ageDays" class="no-same-width">
                        {for $i = 0 to 29}
                            <option value="{$i}" {php}echo set_selct('ageDays', {$i});{/php}>{$i}</option>
                        {/for}
                    </select>
                    {t}days{/t}
                </td>
            </tr>
            <tr>
                <th>{t}religion{/t}</th>
                <td>
                    <select name="religion">
                        <option></option>
                            <option value="{Person::MUSLIM}"
                                    {php}echo set_selct('religion', {Person::MUSLIM}, '{$person->getReligion()}' == {Person::MUSLIM});{/php}>
                        {t}muslim{/t}</option>
                            <option value="{Person::HINDU}"
                                    {php}echo set_selct('religion', {Person::HINDU}, '{$person->getReligion()}' == {Person::HINDU});{/php}>
                        {t}hindu{/t}</option>
                            <option value="{Person::BUDDIST}"
                                    {php}echo set_selct('religion', {Person::BUDDIST}, '{$person->getReligion()}' == {Person::BUDDIST});{/php}>
                        {t}buddist{/t}</option>
                            <option value="{Person::CHRISTIAN}"
                                    {php}echo set_selct('religion', {Person::CHRISTIAN}, '{$person->getReligion()}' == {Person::CHRISTIAN});{/php}>
                        {t}christian{/t}</option>
                            <option value="{Person::OTHER}"
                                    {php}echo set_selct('religion', {Person::OTHER}, '{$person->getReligion()}' == {Person::OTHER});{/php}>
                        {t}other{/t}</option>
                    </select>
                </td>
                <td>{php}echo form_error('religion');{/php}</td>
            </tr>
            <tr>
                <th>{t}address{/t}</th>
                <td>
                    <textarea name="address" rows="3"
                              cols="30">{php}echo set_val('address', '{$person->getAddress()}');{/php}</textarea>
                </td>
                <td>{php}echo form_error('address');{/php}</td>
            </tr>
            <tr>
                <th>{t}mobile{/t}</th>
                <td>
                    <input type="text" name="mobile" value="{php}echo set_val('mobile', '{$person->getMobile()}');{/php}"/>
                </td>
                <td>{php}echo form_error('mobile');{/php}</td>
            </tr>
            <tr>
                <th>{t}email{/t} {if $personType != 'patient'}{required}{/if}</th>
                <td>
                    <input type="text" name="email" value="{php}echo set_val('email', '{$person->getEmail()}');{/php}"/>
                </td>
                <td>{php}echo form_error('email');{/php}</td>
            </tr>

            {include file=$templateFile}
        </table>
    </div>
    <div class="section-box-footer rounded-corners-bottom">
        <input type="hidden" name="id" value="{$person->getId()}"/>
        <input type="submit" name="submit" value="{if $person->getId() > 0}{t}update{/t}{else}{t}register{/t}{/if}"/>
        <span style="float:right">
            <input type="button" value="{t}cancel{/t}"
                   onclick="window.location='{if $person->getId() > 0}{url controller=$personType action='details' id=$person->getId()}{else}{url controller='dashboard'}{/if}'"/>
        </span>
    </div>
</div>

</form>

{/block}
