{extends file="templates/loggedInUser.tpl"}

{block name="title"}{t}prognostic.factors{/t}{/block}

{block name="loggedInBody"}

    {function name=editModeContent}
        <a href="{url controller='prognosticFactor' action='edit' id=$patient->getId()}">
            <img src="{url path='public/images/edit.png'}" alt="{t}edit{/t}" title="{t}edit{/t}"
                 style="vertical-align: bottom;"/>
            {t}edit{/t}
        </a>
    {/function}

    {capture name="sectionTitle"}{t}prognostic.factors.for.x{/t}{/capture}
    {sectionbox title="{$smarty.capture.sectionTitle|sprintf:Patient::getCategoryName($patient->getCategory())}"
                fullwidth=true titleActions="{editModeContent}"}
        <table class="details style-table" style="margin: 25px auto">
            <tr>
                <th></th>
                <th class="text-align-center">{t}status{/t}</th>
                <th class="text-align-center">{t}favorable{/t}</th>
            </tr>
            {$prognosticFactorsOfPatient = $patient->getPrognosticFactors()}
            {foreach from=$prognosticFactors item=factor name=prognosticFactors}
                <tr>
                    <td>{PrognosticFactor::getUserVisibleNameOfFactor($factor)}</td>
                    {if count($prognosticFactorsOfPatient) > 0}
                        {$patientPrognosticFactor = $prognosticFactorsOfPatient[$smarty.foreach.prognosticFactors.index]}
                        <td>{$patientPrognosticFactor->getStatus()}</td>
                        <td class="text-align-center">{boolean value=$patientPrognosticFactor->isFavorable()}</td>
                    {else}
                        <td></td>
                        <td></td>
                    {/if}
                </tr>
            {/foreach}
        </table>
    {/sectionbox}

{/block}
