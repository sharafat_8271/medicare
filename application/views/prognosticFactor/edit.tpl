{extends file="templates/loggedInUser.tpl"}

{block name="title"}{t}edit.prognostic.factors{/t}{/block}

{block name="loggedInBody"}

    {php}echo form_open('prognosticFactor/update');{/php}

    <div class="section-box-full-width rounded-corners">
        <div class="section-box-header rounded-corners-top">
            {capture name="sectionTitle"}{t}edit.prognostic.factors.for.x{/t}{/capture}
            {$smarty.capture.sectionTitle|sprintf:Patient::getCategoryName($patient->getCategory())}
        </div>

        <div class="section-box-body">
            <table>
                <tr class="tableHeader">
                    <th></th>
                    <th class="text-align-center">{t}status{/t}</th>
                    <th>{t}favorable{/t}</th>
                </tr>
                {$prognosticFactorsOfPatient = $patient->getPrognosticFactors()}
                {$factorsOfPatientExists = count($prognosticFactorsOfPatient) > 0}
                {foreach from=$prognosticFactors item=factor name=prognosticFactors}
                    {if $factorsOfPatientExists}
                        {$patientPrognosticFactor = $prognosticFactorsOfPatient[$smarty.foreach.prognosticFactors.index]}
                    {/if}
                    <tr>
                        <th>{PrognosticFactor::getUserVisibleNameOfFactor($factor)}</th>
                        <td>
                            <input type="text" name="status_{$factor}" style="width: 20em"
                                   value="{if $factorsOfPatientExists}{$patientPrognosticFactor->getStatus()}{/if}"/>
                        </td>
                        <td class="text-align-center">
                            <label>
                                <input type="radio" name="favorable_{$factor}" value="1"
                                       {if $factorsOfPatientExists && $patientPrognosticFactor->isFavorable() === true}
                                            checked="checked"
                                       {/if}/>
                                {t}yep{/t}
                            </label>
                            <label>
                                <input type="radio" name="favorable_{$factor}" value="0"
                                       {if $factorsOfPatientExists && $patientPrognosticFactor->isFavorable() === false}
                                            checked="checked"
                                       {/if}/>
                                {t}nope{/t}
                            </label>
                            <label>
                                <input type="radio" name="favorable_{$factor}" value="x"
                                       {if $factorsOfPatientExists && $patientPrognosticFactor->isFavorable() === null}
                                            checked="checked"
                                       {/if}/>
                                {t}dunno{/t}
                            </label>
                        </td>
                    </tr>
                {/foreach}
            </table>
        </div>

        <div class="section-box-footer rounded-corners-bottom">
            <input type="hidden" name="patientId" value="{$patient->getId()}"/>
            <input type="submit" name="submit" value="{t}update{/t}"/>
            <span style="float:right">
                <input type="button" value="{t}cancel{/t}"
                       onclick="window.location='{url controller='patient' action='details' id=$patient->getId()}'"/>
            </span>
        </div>
    </div>

    </form>

{/block}
