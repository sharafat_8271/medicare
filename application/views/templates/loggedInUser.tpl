{extends file="templates/default.tpl"}

{block name="html_head_elements"}
    <script type="text/javascript">
        $(function () {
            $(".date").prop("readonly", true)
                      .datepicker({
                                    changeMonth: true,
                                    changeYear: true,
                                    yearRange: "-99:+0",
                                    maxDate: 0,
                                    dateFormat: "yy-mm-dd"
                                });
        });
    </script>
    {block name=head_elements}{/block}
{/block}

{block name="body"}

<div>
    <div style="float: left">
        <span>
            <a href="{url controller='dashboard'}">
                <img src="{url path='public/images/home.png'}" style="vertical-align: middle" alt="home"/>
                {t}dashboard{/t}
            </a>
        </span>
        <span style="margin-left: 20px">
            <a href="{url controller='patient' action='list'}">
                <img src="{url path='public/images/list.png'}" style="vertical-align: middle" alt="list"/>
                {t}patient.list{/t}
            </a>
        </span>
    </div>

    <div style="float: right">
        {if is_a($session->userdata('login'), 'models\Doctor')}
            {$controller = 'doctor'}
        {elseif is_a($session->userdata('login'), 'models\Patient')}
            {$controller = 'patient'}
        {else}
            {$controller = 'admin'}
        {/if}
        {t}welcome{/t}, <a href="{url controller=$controller action='details' id={$session->userdata('login')->getId()}}">{$session->userdata('login')->getName()}</a>

        <span style="margin: 0 5px">|</span>

        <a href="{url controller='logout'}">{t}logout{/t}</a>
        <a href="{url controller='logout'}"><img src="{url path='public/images/logout.png'}" style="vertical-align: middle" alt="logout"/></a>
    </div>

    <div style="clear: both"></div>
</div>

<hr style="margin-bottom: 25px"/>

{if !empty($targetPerson)}
    <h2>
        {if is_a($targetPerson, 'models\Patient')}
            {$controllerName = "patient"}
        {elseif is_a($targetPerson, 'models\Doctor')}
            {$controllerName = "doctor"}
        {elseif is_a($targetPerson, 'models\Admin')}
            {$controllerName = "admin"}
        {/if}
        <a href="{url controller=$controllerName action='details' id={$targetPerson->getId()}}">{$targetPerson->getId(true)}</a> -
        {$targetPerson->getName()}
    </h2>
{/if}

{block name=loggedInBody}{/block}

{/block}
