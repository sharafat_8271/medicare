<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>{t}app.name{/t} - {block name=title}{/block}</title>
    <link rel="icon" type="image/png" href="{url path='public/images/favicon.png'}"/>
    <link rel="stylesheet" type="text/css" href="{url path='public/css/style.css'}"/>
    <link rel="stylesheet" type="text/css" href="{url path='public/css/redmond/jquery-ui-1.9.2.custom.css'}"/>
    <script type="text/javascript" src="{url path='public/js/jquery-1.8.3.js'}"></script>
    <script type="text/javascript" src="{url path='public/js/jquery-ui-1.9.2.js'}"></script>
    <script type="text/javascript" src="{url path='public/js/cryptic-ui-0.1.js'}"></script>
{block name=html_head_elements}{/block}
</head>

<body>

<div id="header">
    <div style="float: left">
        <img src="{url path='public/images/app_icon.png'}" alt="medicare_logo"/>
        <span class="sitename">{t}app.name{/t}</span>
    </div>
    {if $session->userdata('login')}
        <div style="float:right">
            {provider_logo targetHeight=43 var='providerLogo'}
            <img src="{$providerLogo['url']}" width={$providerLogo['width']} height={$providerLogo['height']}/>
            {$provider = $session->userdata('login')->getProvider()}
            <span class="sitename">{$provider->getName()}</span>
        </div>
    {/if}
    <div style="clear: both"></div>
    <hr/>
</div>

{block name=body}{/block}

<div class="footer">
    Copyright &copy;
    <a href="http://www.crypticit.com" target="_blank">Cryptic IT</a>, 2011 - {$smarty.now|date_format:"Y"}.
    All Rights Reserved.<br/>
    Version: {$smarty.const.APP_VERSION}
    {if ENVIRONMENT != 'production'}
        <p>
            Page rendered in <strong>{$elapsed_time}</strong> seconds with memory usage of
            <strong>{$memory_usage}</strong> MB
        </p>
    {/if}
</div>

</body>

</html>
