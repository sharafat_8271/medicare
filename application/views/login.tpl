{extends file="templates/default.tpl"}

{block name="title"}{t}login{/t}{/block}

{block name="body"}

<div style="text-align: center; margin-top: 200px">

    {sectionbox title="{t}login{/t}" shadow=true}
        {php}echo form_open('login/logIn');{/php}

        <table>
            {if isset($errorMessage)}
                <tr>
                    <td colspan="2">
                        <div class="error-msg">{$errorMessage}</div>
                    </td>
                </tr>
            {/if}
            <tr>
                <th>{t}email{/t}:</th>
                <td><input type="text" name="email" value="{php}echo set_val('email', '');{/php}"
                           size="32"/></td>
            </tr>
            <tr>
                <th>{t}password{/t}:</th>
                <td><input type="password" name="password" size="32"/></td>
            </tr>
            <tr>
                <th></th>
                <td style="text-align: center"><input type="submit" name="login" value="{t}login{/t}"/></td>
                <td></td>
            </tr>
        </table>

        </form>
    {/sectionbox}

</div>

{/block}
