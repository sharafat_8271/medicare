<tr>
    <th>{t}reg.no{/t}</th>
    <th>{t}name{/t}</th>
    <th>{t}type{/t}</th>
    <th>{t}admin{/t}</th>
    <th colspan="2">{t}action{/t}</th>
</tr>

{foreach $personList as $person}

    {if is_a($person, "models\Doctor")}
        {$personCategory = "doctor"}
    {elseif is_a($person, "models\Patient")}
        {$personCategory = "patient"}
    {elseif is_a($person, "models\Admin")}
        {$personCategory = "admin"}
    {/if}


    <tr>
        <td>
            <a href="{url controller=$personCategory action='details' id={$person->getId()}}">{$person->getId(true)}</a>
        </td>
        <td style="text-align: left">{$person->getName(true)}</td>
        <td>{t}{$personCategory}{/t}</td>
        <td>{boolean value=$person->isAdmin() emptyOnFalse=true}</td>
        <td>
            {if $personCategory != "patient"}
                <a href="{url controller='administer' action='toggleActivation' id=$person->getId()}">
                    {t}{if $person->getAccountStatus() == Person::ACTIVE}deactivate{else}activate{/if}{/t}
                </a>
            {/if}
        </td>
        <td style="padding-left: 20px">
            {if $personCategory != "patient"}
                <a href="{url controller='password' action='resetPass' id=$person->getId()}">
                    {t}reset.password{/t}
                </a>
            {/if}
        </td>
    </tr>
{/foreach}
