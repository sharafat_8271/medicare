{extends file="templates/loggedInUser.tpl"}

{block name="title"}{t}reset.password{/t}{/block}

{block name="loggedInBody"}

    {include file="person/password_update.tpl" from="password/resetPass"}

{/block}
