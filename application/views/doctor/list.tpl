<tr class="sortable-header">
    {sortable_table_header field="id" text="{t}reg.no{/t}"}
    {sortable_table_header field="name" text="{t}name{/t}"}
    {sortable_table_header field="degree" text="{t}degree{/t}"}
</tr>

{foreach $personList as $doctor}
    <tr>
        <td style="vertical-align: top">
            <a href="{url controller='doctor' action='details' id={$doctor->getId()}}">{$doctor->getId(true)}</a>
        </td>
        <td style="vertical-align: top; text-align: left">{$doctor->getName(true)}</td>
        <td style="text-align: left">{$doctor->getDegree()|nl2br}</td>
    </tr>
{/foreach}
