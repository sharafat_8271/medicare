<tr>
    <th>{t}degree{/t}</th>
    <td>
        <textarea name="degree" rows="3" cols="30">{php}echo set_val('degree', '{$person->getDegree()}');{/php}</textarea>
    </td>
    <td>{php}echo form_error('degree');{/php}</td>
</tr>

{include file="person/password_create.tpl"}

{if $session->userdata('login')->isAdmin()}
    <tr>
        <th>{t}admin{/t}</th>
        <td><input type="checkbox" name="admin" {php}echo set_chkbox('admin', '', '{$person->isAdmin()}');{/php}/></td>
        <td>{php}echo form_error('admin');{/php}</td>
    </tr>
{/if}
