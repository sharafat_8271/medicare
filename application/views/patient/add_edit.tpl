<tr>
    <th>{t}height{/t}</th>
    <td>
        <input type="text" class="no-same-width" name="height"
               value="{php}echo set_val('height', '{$person->getHeightInCentimeters()}');{/php}"/>
        <select name="heightUnit" class="no-same-width" style="width: 8em">
            <option value="cm" {php}echo set_selct('heightUnit', 'cm');{/php}>{t}cm{/t}</option>
            <option value="inches" {php}echo set_selct('heightUnit', 'inches');{/php}>{t}inches{/t}</option>
        </select>
    </td>
    <td>{php}echo form_error('height');{/php}</td>
</tr>
<tr>
    <th>{t}weight{/t}</th>
    <td>
        <input type="text" class="no-same-width" name="weight"
               value="{php}echo set_val('weight', '{$person->getWeightInKilograms()}');{/php}"/>
        <select name="weightUnit" class="no-same-width" style="width: 8em">
            <option value="kg" {php}echo set_selct('weightUnit', 'kg');{/php}>{t}kg{/t}</option>
            <option value="lb" {php}echo set_selct('weightUnit', 'lb');{/php}>{t}lb{/t}</option>
        </select>
    </td>
    <td>{php}echo form_error('weight');{/php}</td>
</tr>
<tr>
    <th>{t}category{/t}</th>
    <td>
        <select name="category">
            <option></option>
            {foreach Patient::enumerateCategories() as $categoryId => $categoryName}
                <option value="{$categoryId}"
                        {php}echo set_selct('category', {$categoryId}{if $person->getCategory()}, {$person->getCategory()} == {$categoryId}{/if});{/php}>
                    {$categoryName}</option>
            {/foreach}
        </select>
    </td>
    <td>{php}echo form_error('category');{/php}</td>
</tr>
<tr>
    <th>{t}diagnosis{/t}</th>
    <td>
        <input type="text" name="diagnosis" value="{php}echo set_val('diagnosis', '{$person->getDiagnosis()}');{/php}"/>
    </td>
    <td>{php}echo form_error('diagnosis');{/php}</td>
</tr>
<tr>
    <th>{t}current.status{/t}</th>
    <td>
        <input type="text" name="currentStatus" value="{php}echo set_val('currentStatus', '{$person->getCurrentStatus()}');{/php}"/>
    </td>
    <td>{php}echo form_error('currentStatus');{/php}</td>
</tr>
