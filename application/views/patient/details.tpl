{extends file="templates/loggedInUser.tpl"}

{block name="title"}{t}patient.details{/t}{/block}

{block name="loggedInBody"}

<div>
    {if $session->flashdata('profile.updated') || $session->flashdata('patient.admitted')
        || $session->flashdata('patient.discharged') || $session->flashdata('followup.added')
        || $session->flashdata('followup.closed') || $session->flashdata('presenting.features.updated')
        || $session->flashdata('findings.added') || $session->flashdata('prognostic.factors.updated')}
        <div class="text-align-center">
            <div class="success-msg shrink-wrap">
                {if $session->flashdata('profile.updated')}
                    {t}profile.updated{/t}
                {elseif $session->flashdata('patient.admitted')}
                    {t}patient.admitted{/t}
                {elseif $session->flashdata('patient.discharged')}
                    {t}patient.discharged{/t}
                {elseif $session->flashdata('followup.added')}
                    {t}followup.added{/t}
                {elseif $session->flashdata('followup.closed')}
                    {t}followup.closed{/t}
                {elseif $session->flashdata('presenting.features.updated')}
                    {t}presenting.features.updated{/t}
                {elseif $session->flashdata('findings.added')}
                    {t}findings.added{/t}
                {elseif $session->flashdata('prognostic.factors.updated')}
                    {t}prognostic.factors.updated{/t}
                {/if}
            </div>
        </div>
    {/if}

    <div style="float: left; width: 72%; margin-right: 3%">
        {sectionbox title="{t}status{/t}" fullwidth=true}
            <table class="details th-min-width">
                <tr>
                    <th>{t}category{/t}</th>
                    <td>{Patient::getCategoryName($person->getCategory())}</td>
                </tr>
                <tr>
                    <th>{t}diagnosis{/t}</th>
                    <td>{$person->getDiagnosis()}</td>
                </tr>
                <tr>
                    <th>{t}current.status{/t}</th>
                    <td>{$person->getCurrentStatus()}</td>
                </tr>
                <tr>
                    <th>{t}indoor{/t}</th>
                    <td>{boolean value=$person->isIndoor()}</td>
                </tr>
            </table>
        {/sectionbox}

        {sectionbox title="{t}medical.profile{/t}" class="margin-top" fullwidth=true}
            <table class="details th-min-width">
                <tr>
                    <th>{t}age{/t}</th>
                    <td>{$person->getYearsInAge()} {t}years{/t}
                        {$person->getMonthsInAge()} {t}months{/t} {$person->getDaysInAge()} {t}days{/t}</td>
                </tr>
                <tr>
                    <th>{t}sex{/t}</th>
                    <td>{t}{Person::msgKey($person->getSex())}{/t}</td>
                </tr>
                <tr>
                    <th>{t}weight{/t}</th>
                    <td>
                        {if $person->getWeightInKilograms() != ""}
                            {$person->getWeightInKilograms()} {t}kg{/t} ({$person->getWeightInPounds()} {t}lb{/t})
                        {/if}
                    </td>
                </tr>
                <tr>
                    <th>{t}height{/t}</th>
                    <td>
                        {if $person->getHeightInCentimeters() != ""}
                            {$person->getHeightInCentimeters()} {t}cm{/t}
                            {$feetInches = $person->getHeightInFeetInches()}
                            ({$feetInches['feet']}&prime; {$feetInches['inches']}&Prime;)
                        {/if}
                    </td>
                </tr>
                <tr>
                    <th style="vertical-align: bottom">{t}bsa{/t}</th>
                    <td>
                        {if $person->getBodySurfaceArea() != ""}
                            {$person->getBodySurfaceArea()} {t}meter{/t}<sup>2</sup>
                        {/if}
                    </td>
                </tr>
            </table>
        {/sectionbox}

        {sectionbox title="{t}general.profile{/t}" class="margin-top" fullwidth=true}
            <table class="details th-min-width">
                <tr>
                    <th>{t}dob{/t}</th>
                    <td>{$person->getDob(true)}</td>
                </tr>
                <tr>
                    <th>{t}religion{/t}</th>
                    <td>
                        {if $person->getReligion() != ""}
                            {t}{Person::msgKey($person->getReligion())}{/t}
                        {/if}
                    </td>
                </tr>
                <tr>
                    <th>{t}mobile{/t}</th>
                    <td>{$person->getMobile()}</td>
                </tr>
                <tr>
                    <th>{t}email{/t}</th>
                    <td>{$person->getEmail()}</td>
                </tr>
                <tr>
                    <th>{t}address{/t}</th>
                    <td>{$person->getAddress()|nl2br}</td>
                </tr>
            </table>
        {/sectionbox}

        {sectionbox title="{t}additional.info{/t}" class="margin-top" fullwidth=true}
            <table class="details th-min-width">
                <tr>
                    <th>{t}registered.on{/t}</th>
                    <td>{$person->getRegistrationDateTime(true)}</td>
                </tr>
                <tr>
                    <th>{t}registered.by{/t}</th>
                    <td>
                        {if is_a($person->getRegisteredBy(), "models\Doctor")}
                            {$controller = 'doctor'}
                        {else}
                            {$controller = 'admin'}
                        {/if}
                        <a href="{url controller=$controller action='details' id=$person->getRegisteredBy()->getId()}">
                            {$person->getRegisteredBy()->getName()}</a>
                    </td>
                </tr>
            </table>
        {/sectionbox}
    </div>

    <div style="float: right; width: 25%">
        {sectionbox title="{t}details{/t}" fullwidth=true}
            <a href="{url controller='presentingFeatures' action='details' id=$person->getId()}">
                {t}presenting.features{/t}</a><br/>
            {if $person->getCategory()}
                <a href="{url controller='prognosticFactor' action='details' id=$person->getId()}">
                    {t}prognostic.factors{/t}</a><br/>
            {/if}
            <br/>
            <a href="{url controller='findings' action='list' id=$person->getId()}">
                {t}findings{/t}</a><br/>
            <a href="{url controller='investigation' action='list' id=$person->getId()}">{t}investigations{/t}</a><br/>
            <a href="{url controller='protocol' action='details' id=$person->getId()}">{t}protocols{/t}</a><br/>
            <br/>
            <a href="{url controller='summary' action='list' id=$person->getId()}">{t}summarys{/t}</a><br/>
            <a href="{url controller='discussion' action='list' id=$person->getId()}">{t}discussions{/t}</a><br/>
            <a href="{url controller='advice' action='list' id=$person->getId()}">{t}advices{/t}</a><br/>
            <a href="{url controller='drug' action='list' id=$person->getId()}">{t}drugs{/t}</a><br/>
            <br/>
            <a href="{url controller='admission' action='list' id=$person->getId()}">
                {t}admission.discharge.history{/t}</a><br/>
            <a href="{url controller='followup' action='list' id=$person->getId()}">{t}followup.history{/t}</a>
        {/sectionbox}

        {sectionbox title="{t}actions{/t}" class="margin-top" fullwidth=true}
            <a href="{url controller='findings' action='new' id=$person->getId()}">{t}add.findings{/t}</a><br/>
            <a href="{url controller='investigation' action='new' id=$person->getId()}">{t}add.investigation{/t}</a><br/>
            <br/>
            {if $person->isLastFollowupClosed()}
                <a href="{url controller='followup' action='new' id=$person->getId()}">{t}add.next.plan{/t}</a><br/>
            {else}
                <a href="{url controller='followup' action='close' id=$person->getId()}">{t}close.followup{/t}</a><br/>
            {/if}
            {if $person->isIndoor()}
                <a href="{url controller='admission' action='discharge' id=$person->getId()}">{t}discharge{/t}</a><br/>
                {else}
                <a href="{url controller='admission' action='admit' id=$person->getId()}">{t}admit{/t}</a><br/>
            {/if}
            <br/>
            <a href="{url controller='patient' action='printForm' id=$person->getId()}">{t}print.records{/t}</a><br/>
            <br/>
            <a href="{url controller='patient' action='edit' id=$person->getId()}">{t}edit.profile{/t}</a><br/>
        {/sectionbox}
    </div>

    <div style="clear: both"></div>
</div>

{/block}
