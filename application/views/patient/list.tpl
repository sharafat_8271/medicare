<tr class="sortable-header">
    {sortable_table_header field="id" text="{t}reg.no{/t}"}
    {sortable_table_header field="name" text="{t}name{/t}"}
    {sortable_table_header field="dob" text="{t}age{/t}"}
    {sortable_table_header field="category" text="{t}category{/t}"}
</tr>

{foreach $personList as $patient}
    <tr>
        <td><a href="{url controller='patient' action='details' id={$patient->getId()}}">{$patient->getId(true)}</a></td>
        <td style="text-align: left">{$patient->getName(true)}</td>
        <td>{$patient->getYearsInAge()} {t}years{/t} {$patient->getMonthsInAge()} {t}months{/t}</td>
        <td>
            <a href="{url controller='patient' action='list'}?{Search::SEARCH_FIELD_CATEGORY}={$patient->getCategory()}">
                {Patient::getCategoryName($patient->getCategory())}</a>
        </td>
    </tr>
{/foreach}
