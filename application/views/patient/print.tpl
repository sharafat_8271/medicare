<html xmlns="http://www.w3.org/1999/html">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>{$person->getId(true)} - {$person->getName()}</title>
        <link rel="stylesheet" type="text/css" href="{url path='public/css/style.css'}"/>
        <style type="text/css" media="print">
            body {
                width: 210mm;
                margin: 0.5in;
                font-family: times, serif;
            }

            table.details th {
                font-weight: bold;
                color: black;
            }

            table td {
                padding: 0 5px;
                vertical-align: top;
            }

            body, th, td {
                font-size: 10pt;
            }

            .footer {
                position: absolute;
                bottom: 0.5in;
                left: 0;
                right: 0;
                text-align: center;
                font-size: 8pt;
            }

            .left {
                display: block;
                float: left;
                width: 50%;
            }

            .right {
                display: block;
                float: right;
                width: 49%;
            }
        </style>
    </head>

    <body>

    {function name=simpleModuleDetails}
        {if count($history) > 0}
            {sectionbox title="{t}{$module}{/t}" class="margin-top" fullwidth=true}
            <table class="details" style="margin: 5px 0">
                <tr>
                    <td style="text-align: justify">{$history[count($history) - 1]->getDetails()|nl2br}</td>
                </tr>
            </table>
            {/sectionbox}
        {/if}
    {/function}

    <div>
        <table style="width: 100%">
            <tr>
                {$provider = $session->userdata('login')->getProvider()}
                <td style="vertical-align: top">
                    {if $provider->getLogoFileName() != ""}
                        {provider_logo targetWidth=100 targetHeight=100 var='logo'}
                        <img src="{$logo['url']}" width={$logo['width']} height={$logo['height']}/>
                    {/if}
                </td>
                <td style="text-align: right">
                    <h1 style="font-variant: small-caps; margin: 0">{$provider->getName()}</h1>
                    {if $provider->getDepartmentName() != ""}
                        <h2 style="margin: 0">{$provider->getDepartmentName()}</h2>
                    {/if}
                    {if $provider->getAddress() != ""}
                        {$provider->getAddress()}<br/>
                    {/if}
                    {if $provider->getContactNo() != ""}
                        {$provider->getContactNo()}<br/>
                    {/if}
                    {if $provider->getEmail() != ""}
                        {$provider->getEmail()}
                    {/if}
                </td>
            </tr>
        </table>

        <div style="text-align: center; margin: 10px auto">
            <h2 style="margin-bottom: 0">{t}patient.evaluation.summary{/t}</h2>
            <strong>({t from=$from to=$to}for.the.period.x.to.y{/t})</strong>
        </div>
    </div>

    <h2>{$person->getId(true)} - {$person->getName()}</h2>

    <div>
        <div class="left">
            {sectionbox title="{t}general.profile{/t}" fullwidth=true}
                <table class="details">
                    <tr>
                        <th>{t}dob{/t}</th>
                        <td>{$person->getDob(true)}</td>
                    </tr>
                    <tr>
                        <th>{t}religion{/t}</th>
                        <td>
                            {if $person->getReligion() != ""}
                                {t}{Person::msgKey($person->getReligion())}{/t}
                            {/if}
                        </td>
                    </tr>
                    <tr>
                        <th>{t}mobile{/t}</th>
                        <td>{$person->getMobile()}</td>
                    </tr>
                    <tr>
                        <th>{t}email{/t}</th>
                        <td>{$person->getEmail()}</td>
                    </tr>
                    <tr>
                        <th>{t}address{/t}</th>
                        <td>{$person->getAddress()|nl2br}</td>
                    </tr>
                </table>
            {/sectionbox}
        </div>

        <div class="right">
            {sectionbox title="{t}medical.profile{/t}" fullwidth=true}
                <table class="details">
                    <tr>
                        <th>{t}age{/t}</th>
                        <td>{$person->getYearsInAge()} {t}years{/t}
                            {$person->getMonthsInAge()} {t}months{/t} {$person->getDaysInAge()} {t}days{/t}</td>
                    </tr>
                    <tr>
                        <th>{t}sex{/t}</th>
                        <td>{t}{Person::msgKey($person->getSex())}{/t}</td>
                    </tr>
                    <tr>
                        <th>{t}weight{/t}</th>
                        <td>
                            {if $person->getWeightInKilograms() != ""}
                                {$person->getWeightInKilograms()} {t}kg{/t} ({$person->getWeightInPounds()} {t}lb{/t})
                            {/if}
                        </td>
                    </tr>
                    <tr>
                        <th>{t}height{/t}</th>
                        <td>
                            {if $person->getHeightInCentimeters() != ""}
                                {$person->getHeightInCentimeters()} {t}cm{/t}
                                {$feetInches = $person->getHeightInFeetInches()}
                                ({$feetInches['feet']}&prime; {$feetInches['inches']}&Prime;)
                            {/if}
                        </td>
                    </tr>
                    <tr>
                        <th style="vertical-align: bottom">{t}bsa{/t}</th>
                        <td>
                            {if $person->getBodySurfaceArea() != ""}
                                {$person->getBodySurfaceArea()} {t}meter{/t}<sup>2</sup>
                            {/if}
                        </td>
                    </tr>
                </table>
            {/sectionbox}
        </div>
    </div>

    {sectionbox title="{t}status{/t}" class="margin-top" fullwidth=true}
        <table class="details">
            <tr>
                <th>{t}category{/t}</th>
                <td>{Patient::getCategoryName($person->getCategory())}</td>
            </tr>
            <tr>
                <th>{t}diagnosis{/t}</th>
                <td>{$person->getDiagnosis()}</td>
            </tr>
            <tr>
                <th>{t}current.status{/t}</th>
                <td>{$person->getCurrentStatus()}</td>
            </tr>
        </table>
    {/sectionbox}

    {$presentingFeatures = $person->getPresentingFeatures()}
    {if $presentingFeatures != null}
        {sectionbox title="{t}presenting.features{/t}" class="margin-top" fullwidth=true}
            <table class="details">
                {if $presentingFeatures->getFever() != ""}
                    <tr>
                        <th>Fever</th>
                        <td>{$presentingFeatures->getFever()|nl2br}</td>
                    </tr>
                {/if}
                {if $presentingFeatures->getCough() != ""}
                    <tr>
                        <th>Cough</th>
                        <td>{$presentingFeatures->getCough()|nl2br}</td>
                    </tr>
                {/if}
                {if $presentingFeatures->getWeakness() != ""}
                    <tr>
                        <th>Weakness</th>
                        <td>{$presentingFeatures->getWeakness()|nl2br}</td>
                    </tr>
                {/if}
                {if $presentingFeatures->getBodyache() != ""}
                    <tr>
                        <th>Bodyache</th>
                        <td>{$presentingFeatures->getBodyache()|nl2br}</td>
                    </tr>
                {/if}
                {if $presentingFeatures->getDyspnea() != ""}
                    <tr>
                        <th>Dyspnea</th>
                        <td>{$presentingFeatures->getDyspnea()|nl2br}</td>
                    </tr>
                {/if}
                {if $presentingFeatures->getGumbleeding() != ""}
                    <tr>
                        <th>Gum Bleeding</th>
                        <td>{$presentingFeatures->getGumbleeding()|nl2br}</td>
                    </tr>
                {/if}
                {if $presentingFeatures->getRash() != ""}
                    <tr>
                        <th>Rash</th>
                        <td>{$presentingFeatures->getRash()|nl2br}</td>
                    </tr>
                {/if}
                {if $presentingFeatures->getAbdominalDistension() != ""}
                    <tr>
                        <th>Abdominal Distension</th>
                        <td>{$presentingFeatures->getAbdominalDistension()|nl2br}</td>
                    </tr>
                {/if}
                {if $presentingFeatures->getBowelAndBladderIncontinence() != ""}
                    <tr>
                        <th>Bowel &amp; Bladder Incontinence</th>
                        <td>{$presentingFeatures->getBowelAndBladderIncontinence()|nl2br}</td>
                    </tr>
                {/if}
                {if $presentingFeatures->getBlurringOfVision() != ""}
                    <tr>
                        <th>Blurring of Vision</th>
                        <td>{$presentingFeatures->getBlurringOfVision()|nl2br}</td>
                    </tr>
                {/if}
                {if $presentingFeatures->getOthers() != ""}
                    <tr>
                        <th>Others</th>
                        <td>{$presentingFeatures->getOthers()|nl2br}</td>
                    </tr>
                {/if}
            </table>
        {/sectionbox}
    {/if}

    {if $includeSummary}
        {simpleModuleDetails module="summary" history=$person->getSummaryHistory()}
    {/if}

    {sectionbox title="{t}prognostic.factors{/t}" class="margin-top" fullwidth=true}
        <table class="details" style="margin: 5px 0">
            <tr>
                <th></th>
                <th class="text-align-center">{t}status{/t}</th>
                <th class="text-align-center">{t}favorable{/t}</th>
            </tr>
            {$prognosticFactorsOfPatient = $person->getPrognosticFactors()}
            {foreach from=PrognosticFactor::getFactorsByCategory($person->getCategory()) item=factor name=prognosticFactors}
                <tr>
                    <td>{PrognosticFactor::getUserVisibleNameOfFactor($factor)}</td>
                    {if count($prognosticFactorsOfPatient) > 0}
                        {$personPrognosticFactor = $prognosticFactorsOfPatient[$smarty.foreach.prognosticFactors.index]}
                        <td>{$personPrognosticFactor->getStatus()}</td>
                        <td class="text-align-center">{boolean value=$personPrognosticFactor->isFavorable()}</td>
                    {else}
                        <td></td>
                        <td></td>
                    {/if}
                </tr>
            {/foreach}
        </table>
    {/sectionbox}

    {if count($findings) > 0}
        {sectionbox title="{t}findings{/t}" class="margin-top" fullwidth=true}
            <table style="margin: 5px 0">
                <tr class="tableHeader">
                    <th></th>
                    <th style="padding: 0 30px">{t}date{/t}</th>
                    <th>{t}details{/t}</th>
                </tr>
                {if array_key_exists(Findings::GENERAL_EXAM, $findings)}
                    <tr>
                        <td style="white-space: nowrap">
                            <strong>{Findings::getUserVisibleNameOfType(Findings::GENERAL_EXAM)}</strong>
                        </td>
                        <td></td>
                        <td></td>
                    </tr>
                    {foreach $findings[Findings::GENERAL_EXAM] as $type => $findingList}
                        {$findingsTypeDisplayed = false}
                        {foreach $findingList as $finding}
                            <tr>
                                <td class="subheading" style="vertical-align: top; white-space: nowrap">
                                    {if !$findingsTypeDisplayed}
                                            {Findings::getUserVisibleNameOfType($type)}
                                        {$findingsTypeDisplayed = true}
                                    {/if}
                                </td>
                                <td class="text-align-center" style="vertical-align: top; white-space: nowrap">
                                    {$finding->getFindingsDate(true)}
                                </td>
                                <td>{$finding->getDetails()|nl2br}</td>
                            </tr>
                        {/foreach}
                    {/foreach}
                {/if}
                {foreach $findings as $type => $findingList}
                    {if $type != Findings::GENERAL_EXAM}
                        {$findingsTypeDisplayed = false}
                        {foreach $findingList as $finding}
                            <tr>
                                <td style="vertical-align: top; white-space: nowrap">
                                    {if !$findingsTypeDisplayed}
                                        <strong>{Findings::getUserVisibleNameOfType($type)}</strong>
                                        {$findingsTypeDisplayed = true}
                                    {/if}
                                </td>
                                <td class="text-align-center" style="vertical-align: top; white-space: nowrap;">
                                    {$finding->getPostedOn(true)}
                                </td>
                                <td>{$finding->getDetails()|nl2br}</td>
                            </tr>
                        {/foreach}
                    {/if}
                {/foreach}
            </table>
        {/sectionbox}
    {/if}

    {if count($investigations) > 0}
        {$anyInvestigationGroupPrinted = false}

        {function name=printInvestigationRecords}
            {foreach $investigations[$investigationGroup] as $investigationTypes}
                {$investigationTypeDisplayed = false}
                {foreach $investigationTypes as $investigation}
                    <tr>
                        <td>
                            {if !$investigationTypeDisplayed}
                            {Investigation::getUserVisibleNameOfType($investigation->getType())}
                            {$investigationTypeDisplayed = true}
                        {/if}
                        </td>
                        <td class="text-align-center">{$investigation->getInvestigationDate(true)}</td>
                        <td>{$investigation->getValue()}</td>
                        <td class="text-align-center">
                            {if $investigation->getAttachmentName() != ""}
                                <img src="{url path='public/images/attachment.png'}" alt="attachment"/>
                            {/if}
                        </td>
                    </tr>
                {/foreach}
            {/foreach}
        {/function}

        {function name=printInvestigationGroup}
            {if array_key_exists($investigationGroup, $investigations)}
                <hr {if $anyInvestigationGroupPrinted}class="margin-top"{/if}/>
                <strong>{$title}</strong>
                <hr/>
                <table style="margin: 5px 0">
                    <tr class="tableHeader">
                        <th></th>
                        <th>{t}date{/t}</th>
                        <th>{t}remarks{/t}</th>
                        <th>{t}attachment{/t}</th>
                    </tr>

                    {printInvestigationRecords investigationGroup=$investigationGroup}
                </table>

                {$anyInvestigationGroupPrinted = true}
            {/if}
        {/function}

        {sectionbox title="{t}investigations{/t}" class="margin-top" fullwidth=true}

        {if array_key_exists(Investigation::HAEMATOPATHOLOGY, $investigations)
        || array_key_exists(Investigation::CBC, $investigations)}
            <hr/>
            <strong>Haematopathology</strong>
            <hr/>
            <table style="margin: 5px 0">
                {if array_key_exists(Investigation::HAEMATOPATHOLOGY, $investigations)}
                    <tr class="tableHeader">
                        <th></th>
                        <th>{t}date{/t}</th>
                        <th>{t}remarks{/t}</th>
                        <th>{t}attachment{/t}</th>
                    </tr>
                {/if}

                {printInvestigationRecords investigationGroup=Investigation::HAEMATOPATHOLOGY}

                {if array_key_exists(Investigation::CBC, $investigations)}
                    <tr>
                        <td style="vertical-align: top">CBC</td>
                        <td colspan="3">
                            <table style="border-collapse: collapse; white-space: nowrap;">
                                <tr>
                                    <th>{t}date{/t}</th>
                                    <th>Hb%</th>
                                    <th>ESR</th>
                                    <th>TC</th>
                                    <th>N%</th>
                                    <th>L%</th>
                                    <th>M%</th>
                                    <th>E%</th>
                                    <th>B%</th>
                                    <th>Blast</th>
                                    <th>Myelocytes</th>
                                    <th>PBF</th>
                                </tr>

                                {foreach $investigations[Investigation::CBC] as $date => $cbcTypes}
                                    <tr>
                                        <td class="text-align-center">{$date}</td>
                                        <td class="text-align-center">
                                            {if array_key_exists(Investigation::CBC_HB, $cbcTypes)}
                                                {$cbcTypes[Investigation::CBC_HB]->getValue()}
                                            {/if}
                                        </td>
                                        <td class="text-align-center">
                                            {if array_key_exists(Investigation::CBC_ESR, $cbcTypes)}
                                                {$cbcTypes[Investigation::CBC_ESR]->getValue()}
                                            {/if}
                                        </td>
                                        <td class="text-align-center">
                                            {if array_key_exists(Investigation::CBC_TC, $cbcTypes)}
                                                {$cbcTypes[Investigation::CBC_TC]->getValue()}
                                            {/if}
                                        </td>
                                        <td class="text-align-center">
                                            {if array_key_exists(Investigation::CBC_N, $cbcTypes)}
                                                {$cbcTypes[Investigation::CBC_N]->getValue()}
                                            {/if}
                                        </td>
                                        <td class="text-align-center">
                                            {if array_key_exists(Investigation::CBC_L, $cbcTypes)}
                                                {$cbcTypes[Investigation::CBC_L]->getValue()}
                                            {/if}
                                        </td>
                                        <td class="text-align-center">
                                            {if array_key_exists(Investigation::CBC_M, $cbcTypes)}
                                                {$cbcTypes[Investigation::CBC_M]->getValue()}
                                            {/if}
                                        </td>
                                        <td class="text-align-center">
                                            {if array_key_exists(Investigation::CBC_E, $cbcTypes)}
                                                {$cbcTypes[Investigation::CBC_E]->getValue()}
                                            {/if}
                                        </td>
                                        <td class="text-align-center">
                                            {if array_key_exists(Investigation::CBC_B, $cbcTypes)}
                                                {$cbcTypes[Investigation::CBC_B]->getValue()}
                                            {/if}
                                        </td>
                                        <td class="text-align-center">
                                            {if array_key_exists(Investigation::CBC_BLAST, $cbcTypes)}
                                                {$cbcTypes[Investigation::CBC_BLAST]->getValue()}
                                            {/if}
                                        </td>
                                        <td class="text-align-center">
                                            {if array_key_exists(Investigation::CBC_MYELOCYTES, $cbcTypes)}
                                                {$cbcTypes[Investigation::CBC_MYELOCYTES]->getValue()}
                                            {/if}
                                        </td>
                                        <td class="text-align-center">
                                            {if array_key_exists(Investigation::CBC_PBF, $cbcTypes)}
                                                {$cbcTypes[Investigation::CBC_PBF]->getValue()}
                                            {/if}
                                        </td>
                                    </tr>
                                {/foreach}
                            </table>
                        </td>
                    </tr>
                {/if}
            </table>

            {$anyInvestigationGroupPrinted = true}
        {/if}

        {if array_key_exists(Investigation::BIOCHEMISTRY, $investigations)
        || array_key_exists(Investigation::S_ELECTROLYTE, $investigations)}
            <hr {if $anyInvestigationGroupPrinted}class="margin-top"{/if}/>
            <strong>Biochemistry</strong>
            <hr/>
            <table style="margin: 10px 0">
                {if array_key_exists(Investigation::BIOCHEMISTRY, $investigations)}
                    <tr class="tableHeader">
                        <th></th>
                        <th>{t}date{/t}</th>
                        <th>{t}remarks{/t}</th>
                        <th>{t}attachment{/t}</th>
                    </tr>
                {/if}

                {if array_key_exists(Investigation::S_ELECTROLYTE, $investigations)}
                    <tr>
                        <td style="vertical-align: top">S. Electrolyte</td>
                        <td colspan="3">
                            <table style="border-collapse: collapse; white-space: nowrap;">
                                <tr>
                                    <th>{t}date{/t}</th>
                                    <th>Na<sup>+</sup></th>
                                    <th>K<sup>+</sup></th>
                                    <th>Cl<sup>-</sup></th>
                                    <th>HCO<sub>3</sub><sup>-</sup></th>
                                </tr>

                                {foreach $investigations[Investigation::S_ELECTROLYTE] as $date => $sElectrolyteTypes}
                                    <tr>
                                        <td class="text-align-center">{$date}</td>
                                        <td class="text-align-center">
                                            {if array_key_exists(Investigation::S_ELECTROLITE_NA, $sElectrolyteTypes)}
                                            {$sElectrolyteTypes[Investigation::S_ELECTROLITE_NA]->getValue()}
                                        {/if}
                                        </td>
                                        <td class="text-align-center">
                                            {if array_key_exists(Investigation::S_ELECTROLITE_K, $sElectrolyteTypes)}
                                            {$sElectrolyteTypes[Investigation::S_ELECTROLITE_K]->getValue()}
                                        {/if}
                                        </td>
                                        <td class="text-align-center">
                                            {if array_key_exists(Investigation::S_ELECTROLITE_CL, $sElectrolyteTypes)}
                                            {$sElectrolyteTypes[Investigation::S_ELECTROLITE_CL]->getValue()}
                                        {/if}
                                        </td>
                                        <td class="text-align-center">
                                            {if array_key_exists(Investigation::S_ELECTROLITE_HCO3, $sElectrolyteTypes)}
                                            {$sElectrolyteTypes[Investigation::S_ELECTROLITE_HCO3]->getValue()}
                                        {/if}
                                        </td>
                                    </tr>
                                {/foreach}
                            </table>
                        </td>
                    </tr>
                {/if}

                {printInvestigationRecords investigationGroup=Investigation::BIOCHEMISTRY}

                {$anyInvestigationGroupPrinted = true}
            </table>
        {/if}

        {printInvestigationGroup title="Microbiology" investigationGroup=Investigation::MICROBIOLOGY}
        {printInvestigationGroup title="Radiology" investigationGroup=Investigation::RADIOLOGY}
        {printInvestigationGroup title="Pathology" investigationGroup=Investigation::PATHOLOGY}
        {printInvestigationGroup title="Cardiology" investigationGroup=Investigation::CARDIOLOGY}
        {printInvestigationGroup title="Respiratory" investigationGroup=Investigation::RESPIRATORY}
        {printInvestigationGroup title="Gastroenterology" investigationGroup=Investigation::GASTROENTEROLOGY}
        {printInvestigationGroup title="Others" investigationGroup=Investigation::GROUP_OTHERS}

        {/sectionbox}
    {/if}

    {if count($protocols) > 0}
        {sectionbox title="{t}protocols{/t}" class="margin-top" fullwidth=true}
            <table style="margin: 5px 0">
                <tr>
                    <th>{t}type{/t}</th>
                    <th>{t}cycle{/t}</th>
                    <th style="padding: 0 5px">{t}day{/t}</th>
                    <th>{t}date{/t}</th>
                    <th>{t}remarks{/t}</th>
                </tr>
                {foreach $protocols as $protocol}
                    <tr>
                        <td style="vertical-align: top">{$protocol->getType()}</td>
                        <td style="vertical-align: top; text-align: center">C<sub>{$protocol->getCycle()}</sub></td>
                        <td style="vertical-align: top; text-align: center">D<sub>{$protocol->getDay()}</sub></td>
                        <td style="vertical-align: top; text-align: center">{$protocol->getProtocolDate(true)}</td>
                        <td>{$protocol->getRemarks()|nl2br}</td>
                    </tr>
                {/foreach}
            </table>
        {/sectionbox}
    {/if}

    {if $includeDrug}
        {simpleModuleDetails module="drugs" history=$person->getDrugHistory()}
    {/if}

    {if count($admissionHistory) > 0}
        {sectionbox title="{t}admission.discharge.history{/t}" class="margin-top" fullwidth=true}
            <table style="margin: 5px 0">
                {include file="admission/list_table_rows.tpl" editMode=false}
            </table>
        {/sectionbox}
    {/if}

    {if $includeDiscussion}
        {simpleModuleDetails module="discussion" history=$person->getDiscussionHistory()}
    {/if}

    {if $includeAdvice}
        {simpleModuleDetails module="advice" history=$person->getAdviceHistory()}
    {/if}

    <p class="footer">
        {capture name="printedBy"}{t}report.generated.by.x.on.n{/t}{/capture}
        {$user = "<b>`$session->userdata('login')->getId(true)` - `$session->userdata('login')->getName()`</b>"}
        {nocache}
            {$now = $smarty.now|date_format:Person::REGISTERED_TIME_DISPLAY_FORMAT}
            {$smarty.capture.printedBy|sprintf:$user:$now}
        {/nocache}
    </p>

    </body>
</html>
