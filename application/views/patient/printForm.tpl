{extends file="templates/loggedInUser.tpl"}

{block name="title"}{t}print.records{/t}{/block}

{block name="head_elements"}
<script>
    $(function () {
        $(".styleTable").styleTable();
    });
</script>
{/block}

{block name="loggedInBody"}

    {php}echo form_open('patient/printRecords');{/php}

    <div class="section-box-full-width rounded-corners">
        <div class="section-box-header rounded-corners-top">{t}print.records{/t}</div>
        <div class="section-box-body" style="padding: 25px 10px">
            <table>
                <tr>
                    <th>{t}print.records.from{/t} {required}</th>
                    <td>
                        <input type="text" class="date" name="from" readonly="readonly"
                               value="{php}echo set_val('from', '{$from}');{/php}"/>
                        {t}print.records.to{/t}
                        <input type="text" class="date" name="to" value="{php}echo set_val('to', '');{/php}"/>
                    </td>
                    <td>{php}echo form_error('from'); echo form_error('to');{/php}</td>
                </tr>
                <tr>
                    <th>{t}include.latest{/t}</th>
                    <td>
                        <label><input type="checkbox" name="sections[]" checked="checked" value="drug"/> {t}drug{/t}</label><br/>
                        <label><input type="checkbox" name="sections[]" checked="checked" value="summary"/> {t}summary{/t}</label><br/>
                        <label><input type="checkbox" name="sections[]" checked="checked" value="discussion"/> {t}discussion{/t}</label><br/>
                        <label><input type="checkbox" name="sections[]" checked="checked" value="advice"/> {t}advice{/t}</label>
                    </td>
                </tr>
            </table>
        </div>

        <div class="section-box-footer rounded-corners-bottom">
            <input type="hidden" name="patientId" value="{$person->getId()}"/>
            <input type="submit" name="submit" value="{t}print.records{/t}"/>
            <span style="float:right">
                <input type="button" value="{t}cancel{/t}"
                       onclick="window.location='{url controller='patient' action='details' id=$person->getId()}'"/>
            </span>
        </div>
    </div>

    </form>

    {sectionbox title="{t}admission.discharge.history{/t}" class="margin-top" fullwidth=true}
        {if count($admissionHistory) > 0}
            <table class="styleTable" style="margin: 25px auto">
            {include file="admission/list_table_rows.tpl" editMode=false}
            </table>
        {else}
            <p class="text-align-center">{t}admission.discharge.history.not.available{/t}</p>
        {/if}
    {/sectionbox}

{/block}
