<tr class="sortable-header">
    {sortable_table_header field="id" text="{t}reg.no{/t}"}
    {sortable_table_header field="name" text="{t}name{/t}"}
</tr>

{foreach $personList as $admin}
    <tr>
        <td><a href="{url controller='admin' action='details' id={$admin->getId()}}">{$admin->getId(true)}</a></td>
        <td style="text-align: left">{$admin->getName(true)}</td>
    </tr>
{/foreach}
