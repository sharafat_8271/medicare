{php}echo form_open('{$module}/{$action}');{/php}

<div class="section-box-full-width rounded-corners {if $object->getId() == 0}margin-top{/if}">
    <div class="section-box-header rounded-corners-top">
        {if $object->getId() > 0}{t}edit.{$module}{/t}{else}{t}add.{$module}{/t}{/if}
    </div>
    <div class="section-box-body">
        <table class="details">
            <tr>
                <th>{t}{$module}{/t} {required}</th>
                <td>
                    <textarea rows="15" cols="90"
                              name="{$module}">{php}echo set_val('{$module}', '{$object->getDetails()}');{/php}</textarea>
                </td>
                <td>{php}echo form_error('{$module}');{/php}</td>
            </tr>
        </table>
    </div>
    <div class="section-box-footer rounded-corners-bottom">
        <input type="hidden" name="patientId" value="{$patient->getId()}"/>
        <input type="hidden" name="{$module}Id" value="{$object->getId()}"/>
        <input type="submit" name="submit" value="{if $object->getId() > 0}{t}update{/t}{else}{t}add{/t}{/if}"/>
        <span style="float:right">
            {if $object->getId() > 0}
                <input type="button" value="{t}cancel{/t}"
                       onclick="window.location='{url controller='patient' action='details' id=$patient->getId()}'"/>
            {/if}
        </span>
    </div>
</div>

</form>
