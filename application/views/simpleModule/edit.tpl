{extends file="templates/loggedInUser.tpl"}

{block name="title"}{t}edit.{$module}{/t}{/block}

{block name="head_elements"}
<style type="text/css">
    textarea {
        width: auto;
        height: auto;
    }
</style>
{/block}

{block name="loggedInBody"}
    {include file="simpleModule/form.tpl" action="update"}
{/block}
