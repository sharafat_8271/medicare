{extends file="templates/loggedInUser.tpl"}

{block name="title"}{t}{$module}s{/t}{/block}

{block name="head_elements"}

    {function name=edit}<img src="{url path='public/images/edit.png'}" alt="{t}edit{/t}" title="{t}edit{/t}" style="vertical-align: bottom;"/> {t}edit{/t}{/function}

<script>
    var editMode = false;

    function toggleEditMode() {
        if (editMode) {
            $(".editColumn").hide();
            $("#editModeContainer").html('{edit}');
        } else {
            $(".editColumn").show();
            $("#editModeContainer").html("{t}cancel.edit{/t}");
        }

        editMode = !editMode;
    }
</script>
<style type="text/css">
    textarea {
        width: auto;
        height: auto;
    }

    .editColumn {
        display: none;
    }
</style>
{/block}

{block name="loggedInBody"}

    {if $session->flashdata("added.`$module`") || $session->flashdata("updated.`$module`")
        || $session->flashdata("deleted.`$module`")}
        <div class="text-align-center">
            <div class="success-msg shrink-wrap">
                {if $session->flashdata("added.`$module`")}
                    {t}{$module}.added.successfully{/t}
                    {elseif $session->flashdata("updated.`$module`")}
                    {t}{$module}.updated.successfully{/t}
                    {elseif $session->flashdata("deleted.`$module`")}
                    {t}{$module}.deleted.successfully{/t}
                {/if}
            </div>
        </div>
    {/if}

    {function name=editModeContent}
        <a href="javascript:toggleEditMode()" id="editModeContainer">{edit}</a>
    {/function}

    {sectionbox title="{t}{$module}s{/t}" fullwidth=true
                titleActions="{if count($objectList) > 0}{editModeContent}{/if}"}
        {if count($objectList) > 0}
            <table class="style-table" style="margin: 25px auto">
                <tr>
                    <th>{t}date{/t}</th>
                    <th>{t}{$module}{/t}</th>
                    <th colspan="2" class="editColumn"></th>
                </tr>
                {foreach $objectList as $obj}
                    <tr>
                        <td style="vertical-align: top; text-align: center; white-space: nowrap">
                            {$obj->getPostedOn(true)}
                        </td>
                        <td>{$obj->getDetails()|nl2br}</td>
                        <td style="vertical-align: top; text-align: center" class="editColumn">
                            <a href="{url controller=$module action='edit' id=$obj->getId()}">
                                <img src="{url path='public/images/edit.png'}" alt="{t}edit{/t}" title="{t}edit{/t}"
                                     style="vertical-align: bottom;"/>
                                {t}edit{/t}
                            </a>
                        </td>
                        <td style="vertical-align: top; text-align: center" class="editColumn">
                            <a href="{url controller=$module action='delete' id=$obj->getId()}/{$patient->getId()}"
                               onclick="return confirm('{t}confirm.delete{/t}')">
                                <img src="{url path='public/images/delete.png'}" alt="{t}delete{/t}" title="{t}delete{/t}"
                                     style="vertical-align: bottom;"/>
                                {t}delete{/t}
                            </a>
                        </td>
                    </tr>
                {/foreach}
            </table>
        {else}
            <p class="text-align-center">{t}{$module}.not.available{/t}</p>
        {/if}
    {/sectionbox}

    {include file="simpleModule/form.tpl" action="add"}

{/block}
