{extends file="templates/loggedInUser.tpl"}

{block name="title"}{t}edit.therapy{/t}{/block}

{block name="head_elements"}
<script>
    $(function () {
        setEqualWidth(filterSameWidthInclusiveElements($("input[type='text'], textarea, select")));
    });
</script>
<style type="text/css">
    textarea {
        width: auto;
        height: auto;
    }
</style>
{/block}

{block name="loggedInBody"}
    {include file="protocols/form.tpl" action="update"}
{/block}
