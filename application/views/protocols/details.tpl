{extends file="templates/loggedInUser.tpl"}

{block name="title"}{t}protocols{/t}{/block}

{block name="head_elements"}

{function name=edit}<img src="{url path='public/images/edit.png'}" alt="{t}edit{/t}" title="{t}edit{/t}" style="vertical-align: bottom;"/> {t}edit{/t}{/function}

<script>
    var editMode = false;

    function toggleEditMode() {
        if (editMode) {
            $(".editColumn").hide();
            $("#editModeContainer").html('{edit}');
        } else {
            $(".editColumn").show();
            $("#editModeContainer").html("{t}cancel.edit{/t}");
        }

        editMode = !editMode;
    }

    $(function () {
        setEqualWidth(filterSameWidthInclusiveElements($("input[type='text'], textarea, select")));
    });
</script>
<style type="text/css">
    textarea {
        width: auto;
        height: auto;
    }

    .editColumn {
        display: none;
    }

    .style-table td {
        vertical-align: top;
        text-align: center;
    }
</style>
{/block}

{block name="loggedInBody"}

    {if $session->flashdata('added.protocol') || $session->flashdata('updated.protocol')
        || $session->flashdata('deleted.protocol') || $session->flashdata('file.upload.error')}
    <div class="text-align-center">
        <div class="success-msg shrink-wrap">
            {if $session->flashdata('added.protocol')}
                {t}protocol.added.successfully{/t}
            {elseif $session->flashdata('updated.protocol')}
                {t}protocol.updated.successfully{/t}
            {elseif $session->flashdata('deleted.protocol')}
                {t}protocol.deleted.successfully{/t}
            {/if}
        </div>
        {if $session->flashdata('file.upload.error')}
            <div style="margin-top: -10px"></div>
            <div class="warning-msg shrink-wrap">
                {t}protocol.file.upload.error{/t}: {$session->flashdata('file.upload.error')|substr:3:-4}
            </div>
        {/if}
    </div>
    {/if}

    {function name=editModeContent}
        <a href="javascript:toggleEditMode()" id="editModeContainer">{edit}</a>
    {/function}

    {sectionbox title="{t}protocols{/t}" fullwidth=true
                titleActions="{if count($patient->getProtocols()) > 0}{editModeContent}{/if}"}
        {if count($protocols) > 0}
            <table class="style-table" style="margin: 25px auto">
                <tr>
                    <th>{t}type{/t}</th>
                    <th>{t}cycle{/t}</th>
                    <th>{t}day{/t}</th>
                    <th>{t}date{/t}</th>
                    <th>{t}remarks{/t}</th>
                    <th>{t}attachment{/t}</th>
                    <th colspan="2" class="editColumn"></th>
                </tr>
                {foreach $protocols as $proto}
                    <tr>
                        <td style="text-align: left">{$proto->getType()}</td>
                        <td>C<sub>{$proto->getCycle()}</sub></td>
                        <td>D<sub>{$proto->getDay()}</sub></td>
                        <td>{$proto->getProtocolDate(true)}</td>
                        <td style="text-align: left">{$proto->getRemarks()|nl2br}</td>
                        <td>
                            {if $proto->getAttachmentName() != ""}
                                <a href="{url path="uploads/"}{$proto->getAttachmentName()}" target="_blank">{t}view{/t}</a>
                            {/if}
                        </td>
                        <td class="editColumn">
                            <a href="{url controller='protocol' action='edit' id=$proto->getId()}">
                                <img src="{url path='public/images/edit.png'}" alt="{t}edit{/t}" title="{t}edit{/t}"
                                     style="vertical-align: bottom;"/>
                                {t}edit{/t}
                            </a>
                        </td>
                        <td class="editColumn">
                            {$urlPathForAttachment = ""}
                            {if $proto->getAttachmentName() != ""}
                                {$urlPathForAttachment = "/?attachment={$proto->getAttachmentName()}"}
                            {/if}
                            <a href="{url controller='protocol' action='delete' id=$proto->getId()}/{$patient->getId()}{$urlPathForAttachment}"
                               onclick="return confirm('{t}confirm.delete{/t}')">
                                <img src="{url path='public/images/delete.png'}" alt="{t}delete{/t}" title="{t}delete{/t}"
                                     style="vertical-align: bottom;"/>
                                {t}delete{/t}
                            </a>
                        </td>
                    </tr>
                {/foreach}
            </table>
        {else}
            <p class="text-align-center">{t}protocols.not.available{/t}</p>
        {/if}
    {/sectionbox}

    {include file="protocols/form.tpl" action="add"}

{/block}
