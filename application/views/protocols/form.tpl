{php}echo form_open_multipart('protocol/{$action}');{/php}

<div class="section-box-full-width rounded-corners {if $protocol->getId() == 0}margin-top{/if}">
    <div class="section-box-header rounded-corners-top">
        {if $protocol->getId() > 0}{t}edit.therapy{/t}{else}{t}add.therapy{/t}{/if}
    </div>
    <div class="section-box-body">
        <table class="details">
            <tr>
                <th>{t}type{/t} {required}</th>
                <td>
                    <input type="text" name="type" value="{php}echo set_val('type', '{$protocol->getType()}');{/php}"/>
                </td>
                <td>{php}echo form_error('type');{/php}</td>
            </tr>
            <tr>
                <th>{t}cycle{/t} {required}</th>
                <td>
                    <input type="text" name="cycle" value="{php}echo set_val('cycle', '{$protocol->getCycle()}');{/php}"/>
                </td>
                <td>{php}echo form_error('cycle');{/php}</td>
            </tr>
            <tr>
                <th>{t}day{/t} {required}</th>
                <td>
                    <input type="text" name="day" value="{php}echo set_val('day', '{$protocol->getDay()}');{/php}"/>
                </td>
                <td>{php}echo form_error('day');{/php}</td>
            </tr>
            <tr>
                <th>{t}date{/t} {required}</th>
                <td>
                    <input type="text" name="date" class="date"
                           value="{php}echo set_val('date', '{$protocol->getProtocolDate(true)}');{/php}"/>
                </td>
                <td>{php}echo form_error('date');{/php}</td>
            </tr>
            <tr>
                <th>{t}remarks{/t}</th>
                <td>
                    <textarea rows="3" cols="30"
                              name="remarks">{php}echo set_val('remarks', '{$protocol->getRemarks()}');{/php}</textarea>
                </td>
                <td>{php}echo form_error('remarks');{/php}</td>
            </tr>
            <tr>
                <th>{t}attachment{/t}</th>
                <td>
                    {if $protocol->getAttachmentName() != ""}
                        <a href="{url path='uploads/'}{$protocol->getAttachmentName()}" target="_blank">
                            {t}view{/t}
                        </a>
                        <input type="checkbox" name="removeAttachment" style="margin-left: 20px"
                                {php}echo set_chkbox("removeAttachment");{/php}/>
                        {t}remove.attachment{/t}
                        <br/>
                    {/if}
                    <input type="file" name="attachment"/>
                    <br/><span style="color: gray; font-size: smaller">{t}allowed.file.types{/t}</span>
                </td>
                <td>{php}echo form_error('attachment');{/php}</td>
            </tr>
        </table>
    </div>
    <div class="section-box-footer rounded-corners-bottom">
        <input type="hidden" name="patientId" value="{$patient->getId()}"/>
        <input type="hidden" name="protocolId" value="{$protocol->getId()}"/>
        <input type="submit" name="submit" value="{if $protocol->getId() > 0}{t}update{/t}{else}{t}add{/t}{/if}"/>
            <span style="float:right">
                {if $protocol->getId() > 0}
                    <input type="button" value="{t}cancel{/t}"
                           onclick="window.location='{url controller='protocol' action='details' id=$patient->getId()}'"/>
                {else}
                    <input type="button" value="{t}clear.form{/t}"
                           onClick="$('input[type=text]').val(''); $('input[type=text]')[0].focus()"/>
                {/if}
            </span>
    </div>
</div>

</form>
