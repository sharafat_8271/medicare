{extends file="templates/loggedInUser.tpl"}

{block name="title"}{t}dashboard{/t}{/block}

{block name=head_elements}
    <link type="text/css" rel="stylesheet" href="{url path="public/css/tablesorter/tablesorter-2.0.5.css"}"/>
    <script type="text/javascript" src="{url path="public/js/jquery.tablesorter.js"}"></script>
    <script type="text/javascript">
        $(function () {
            $("#followup").tablesorter();
        });
    </script>
{/block}

{block name="loggedInBody"}

{$admin = $session->userdata('login')->isAdmin()}

<table class="dashboard">
    <tr>
        <td class="dashboard-box" style="width: 15em">
            {sectionbox title="{t}patient{/t}"}
                <a href="{url controller='patient' action='register'}">{t}register{/t}</a><br/>
                <a href="{url controller='patient' action='list'}">{t}list{/t}</a>
            {/sectionbox}
        </td>

        <td rowspan="{if $admin}3{else}2{/if}" class="dashboard-box" style="padding-right: 0; vertical-align: top">
            {sectionbox title="{t}todays.followups{/t}" fullwidth=true}
                {if count($followups) > 0}
                    <table id="followup" class="style-table tablesorter">
                        <thead>
                            <tr>
                                <th>{t}id{/t}</th>
                                <th>{t}name{/t}</th>
                                <th style="padding-right: 20px">{t}indoor{/t}</th>
                                <th>{t}followup{/t}</th>
                            </tr>
                        </thead>
                        <tbody>
                            {foreach $followups as $followup}
                                <tr>
                                    <td style="white-space: nowrap; vertical-align: top">
                                        <a href="{url controller='patient' action='details' id=$followup->getPatient()->getId()}">
                                            {$followup->getPatient()->getId(true)}
                                        </a>
                                    </td>
                                    <td style="vertical-align: top">{$followup->getPatient()->getName()}</td>
                                    <td style="vertical-align: top; text-align: center">
                                        {boolean value=$followup->getPatient()->isIndoor()}
                                    </td>
                                    <td style="vertical-align: top">{$followup->getNextPlan()}</td>
                                </tr>
                            {/foreach}
                        </tbody>
                    </table>
                {else}
                    <div class="text-align-center" style="padding: 9px">{t}no.followups{/t}</div>
                {/if}
            {/sectionbox}
        </td>
    </tr>

    {if $admin}
        <tr>
            <td class="dashboard-box">
                {sectionbox title="{t}admin{/t}"}
                    <a href="{url controller='doctor' action='register'}">{t}register.doctor{/t}</a><br/>
                    <a href="{url controller='doctor' action='list'}">{t}list.doctors{/t}</a><br/>
                    <br/>
                    <a href="{url controller='admin' action='register'}">{t}register.admin{/t}</a><br/>
                    <a href="{url controller='admin' action='list'}">{t}list.admins{/t}</a><br/>
                    <br/>
                    <a href="{url controller='administer' action='list'}">{t}administer.person{/t}</a>
                {/sectionbox}
            </td>
        </tr>
    {/if}

    <tr>
        <td class="dashboard-box" style="vertical-align: top">
            {sectionbox title="{t}categories{/t}"}
                <table style="width: 100%">
                    {foreach $categories as $categoryId => $patientCount}
                        <tr>
                            <td>
                                {if $patientCount > 0}
                                    <a href="{url controller='patient' action='list'}?{Search::SEARCH_FIELD_CATEGORY}={$categoryId}">{Patient::getCategoryName($categoryId)}</a>
                                    {else}
                                    {Patient::getCategoryName($categoryId)}
                                {/if}
                            </td>
                            <td style="padding-left: 5px">
                                {$patientCount}
                            </td>
                        </tr>
                    {/foreach}
                </table>
            {/sectionbox}
        </td>
    </tr>
</table>

{/block}
