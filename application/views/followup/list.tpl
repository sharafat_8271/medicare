{extends file="templates/loggedInUser.tpl"}

{block name="title"}{t}followup.history{/t}{/block}

{block name="head_elements"}
<script type="text/javascript">
    $(function () {
        $(".styleTable").styleTable();
    });
</script>
<style type="text/css">
    td {
        vertical-align: top;
    }

    td.nowrap {
        white-space: nowrap;
    }
</style>
{/block}

{block name="loggedInBody"}

    {sectionbox title="{t}followup.history{/t}" fullwidth=true}
    {if count($patient->getFollowupHistory()) > 0}
        <table class="styleTable" style="margin: 25px auto">
            <tr>
                <th>{t}followup.date{/t}</th>
                <th>{t}followup.plan{/t}</th>
                <th>{t}closing.date{/t}</th>
                <th>{t}closing.remarks{/t}</th>
            </tr>

            {foreach $patient->getFollowupHistory() as $followup}
                <tr>
                    <td class="nowrap text-align-center">{$followup->getFollowupDate(true)}</td>
                    <td>{$followup->getNextPlan()|nl2br}</td>
                    <td class="nowrap text-align-center">{$followup->getClosingDate(true)}</td>
                    <td>{$followup->getClosingRemarks()|nl2br}</td>
                </tr>
            {/foreach}
        </table>
    {else}
        <p class="text-align-center">{t}followup.history.not.available{/t}</p>
    {/if}
    {/sectionbox}
{/block}
