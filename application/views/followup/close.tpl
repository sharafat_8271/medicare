<!--suppress ALL -->
{extends file="templates/loggedInUser.tpl"}

{block name="title"}{t}close.followup{/t}{/block}

{block name="head_elements"}
<script type="text/javascript" src="{url path='public/js/jquery.autoresize.js'}"></script>
<style type="text/css">
    textarea {
        height: auto;
    }
</style>
{/block}

{block name="loggedInBody"}

    {php}echo form_open_multipart('followup/doClose');{/php}

<div class="section-box-full-width rounded-corners">
<div class="section-box-header rounded-corners-top">{t}close.followup{/t}</div>
<div class="section-box-body" style="vertical-align: top">
    <table>
        <tr>
            <th>{t}followup.date{/t}</th>
            <td>{$lastFollowup->getFollowupDate(true)}</td>
        </tr>
        <tr>
            <th>{t}followup.plan{/t}</th>
            <td>{$lastFollowup->getNextPlan()|nl2br}</td>
        </tr>
        <tr>
            <th>{t}closing.remarks{/t} {required}</th>
            <td><textarea name="closingRemarks" rows="3" cols="20"></textarea></td>
            <td>{php}echo form_error('closingRemarks');{/php}</td>
        </tr>
    </table>
</div>

<div class="section-box-footer rounded-corners-bottom">
    <input type="hidden" name="patientId" value="{$patient->getId()}"/>
    <input type="hidden" name="lastFollowupId" value="{$lastFollowup->getId()}"/>
    <input type="submit" name="submit" value="{t}close.followup{/t}"/>
    <span style="float:right">
        <input type="button" value="{t}cancel{/t}"
               onclick="window.location='{url controller='patient' action='details' id=$patient->getId()}'"/>
    </span>
</div>
</div>

</form>

{/block}
