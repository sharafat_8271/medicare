<!--suppress ALL -->
{extends file="templates/loggedInUser.tpl"}

{block name="title"}{t}add.followup{/t}{/block}

{block name="head_elements"}
<script type="text/javascript" src="{url path='public/js/jquery.autoresize.js'}"></script>
<script>
    $(function () {
        setEqualWidth(filterSameWidthInclusiveElements($("input[type='text'], textarea, select")));

        $(".date").datepicker("option", "maxDate", null)
                  .datepicker("option", "minDate", 1)
                  .datepicker("option", "yearRange", "-0:+10");
    });
</script>
<style type="text/css">
    textarea {
        height: auto;
    }
</style>
{/block}

{block name="loggedInBody"}

    {php}echo form_open_multipart('followup/add');{/php}

<div class="section-box-full-width rounded-corners">
<div class="section-box-header rounded-corners-top">{t}add.followup{/t}</div>
<div class="section-box-body" style="vertical-align: top">
    <table>
        <tr>
            <th>{t}followup.date{/t} {required}</th>
            <td>
                <input type="text" name="followupDate" class="date" value="{php}echo set_val('followupDate', '');{/php}"/>
            </td>
            <td>{php}echo form_error('followupDate');{/php}</td>
        </tr>
        <tr>
            <th>{t}followup.plan{/t} {required}</th>
            <td><textarea name="followupPlan" rows="3" cols="20">{php}echo set_val('followupPlan', '');{/php}</textarea></td>
            <td>{php}echo form_error('followupPlan');{/php}</td>
        </tr>
    </table>
</div>

<div class="section-box-footer rounded-corners-bottom">
    <input type="hidden" name="patientId" value="{$patient->getId()}"/>
    <input type="submit" name="submit" value="{t}submit{/t}"/>
    <span style="float:right">
        <input type="button" value="{t}cancel{/t}"
               onclick="window.location='{url controller='patient' action='details' id=$patient->getId()}'"/>
    </span>
</div>
</div>

</form>

{/block}
