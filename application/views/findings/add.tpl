{extends file="templates/loggedInUser.tpl"}

{block name="title"}{t}add.findings{/t}{/block}

{block name="head_elements"}
<script type="text/javascript" src="{url path='public/js/jquery.autoresize.js'}"></script>
<script type="text/javascript">
    $(function () {
        $(".date").attr("size", 10);
        $("textarea").autoResize();
    });
</script>
{/block}

{block name="loggedInBody"}

    {php}echo form_open('findings/add');{/php}

<div class="section-box-full-width rounded-corners">
    <div class="section-box-header rounded-corners-top">{t}add.findings{/t}</div>
    <div class="section-box-body">
        <table>
            <tr class="tableHeader">
                <th></th>
                <th>{t}date{/t}</th>
                <th>{t}remarks{/t}</th>
            </tr>
            <tr>
                <th>General Exam</th>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <th class="subheading">Body-build</th>
                <td><input type="text" name="bodyBuildDate" class="date"/></td>
                <td><textarea name="bodyBuild"></textarea></td>
            </tr>
            <tr>
                <th class="subheading">Anaemia</th>
                <td><input type="text" name="anaemiaDate" class="date"/></td>
                <td><textarea name="anaemia"></textarea></td>
            </tr>
            <tr>
                <th class="subheading">Jaundice</th>
                <td><input type="text" name="jaundiceDate" class="date"/></td>
                <td><textarea name="jaundice"></textarea></td>
            </tr>
            <tr>
                <th class="subheading">Cyanosis</th>
                <td><input type="text" name="cyanosisDate" class="date"/></td>
                <td><textarea name="cyanosis"></textarea></td>
            </tr>
            <tr>
                <th class="subheading">Clubbing</th>
                <td><input type="text" name="clubbingDate" class="date"/></td>
                <td><textarea name="clubbing"></textarea></td>
            </tr>
            <tr>
                <th class="subheading">Koilonychia</th>
                <td><input type="text" name="koilonychiaDate" class="date"/></td>
                <td><textarea name="koilonychia"></textarea></td>
            </tr>
            <tr>
                <th class="subheading">Leukonyclia</th>
                <td><input type="text" name="leukonycliaDate" class="date"/></td>
                <td><textarea name="leukonyclia"></textarea></td>
            </tr>
            <tr>
                <th class="subheading">Oedema</th>
                <td><input type="text" name="oedemaDate" class="date"/></td>
                <td><textarea name="oedema"></textarea></td>
            </tr>
            <tr>
                <th class="subheading">Lymph node</th>
                <td><input type="text" name="lymphNodeDate" class="date"/></td>
                <td><textarea name="lymphNode"></textarea></td>
            </tr>
            <tr>
                <th class="subheading">Thyroid</th>
                <td><input type="text" name="thyroidDate" class="date"/></td>
                <td><textarea name="thyroid"></textarea></td>
            </tr>
            <tr>
                <th class="subheading">Pulse</th>
                <td><input type="text" name="pulseDate" class="date"/></td>
                <td><textarea name="pulse"></textarea></td>
            </tr>
            <tr>
                <th class="subheading">BP</th>
                <td><input type="text" name="bpDate" class="date"/></td>
                <td><textarea name="bp"></textarea></td>
            </tr>
            <tr>
                <th class="subheading">Temp</th>
                <td><input type="text" name="tempDate" class="date"/></td>
                <td><textarea name="temp"></textarea></td>
            </tr>
            <tr>
                <th class="subheading">Resp. rate</th>
                <td><input type="text" name="respRateDate" class="date"/></td>
                <td><textarea name="respRate"></textarea></td>
            </tr>
            <tr>
                <th class="subheading">Skin</th>
                <td><input type="text" name="skinDate" class="date"/></td>
                <td><textarea name="skin"></textarea></td>
            </tr>
            <tr>
                <th>Resp Exam</th>
                <td><input type="text" name="respExamDate" class="date"/></td>
                <td><textarea name="respExam"></textarea></td>
            </tr>
            <tr>
                <th>CVS Exam</th>
                <td><input type="text" name="cvsExamDate" class="date"/></td>
                <td><textarea name="cvsExam"></textarea></td>
            </tr>
            <tr>
                <th>Abdomen Exam</th>
                <td><input type="text" name="abdomenExamDate" class="date"/></td>
                <td><textarea name="abdomenExam"></textarea></td>
            </tr>
            <tr>
                <th>CNS &amp; Fundus Exam</th>
                <td><input type="text" name="cnsAndFundusExamDate" class="date"/></td>
                <td><textarea name="cnsAndFundusExam"></textarea></td>
            </tr>
            <tr>
                <th>ECOG</th>
                <td><input type="text" name="ecogDate" class="date"/></td>
                <td><textarea name="ecog"></textarea></td>
            </tr>
            <tr>
                <th>Others</th>
                <td><input type="text" name="othersDate" class="date"/></td>
                <td><textarea name="others"></textarea></td>
            </tr>
        </table>
    </div>

    <div class="section-box-footer rounded-corners-bottom">
        <input type="hidden" name="patientId" value="{$patient->getId()}"/>
        <input type="submit" name="submit" value="{t}submit{/t}"/>
        <span style="float:right">
            <input type="button" value="{t}cancel{/t}"
                   onclick="window.location='{url controller='patient' action='details' id=$patient->getId()}'"/>
        </span>
    </div>
</div>

</form>

{/block}
