{extends file="templates/loggedInUser.tpl"}

{block name="title"}{t}findings{/t}{/block}

{block name="loggedInBody"}

    {function name=editModeContent}
        <a href="{url controller='findings' action='list' id=$patient->getId()}{if $action == 'edit'}?editMode=1{/if}">
            {if $action == 'edit'}
                <img src="{url path='public/images/edit.png'}" alt="{t}edit{/t}" title="{t}edit{/t}" style="vertical-align: bottom;"/>
                {t}edit{/t}
            {else}
                {t}cancel.edit{/t}
            {/if}
        </a>
    {/function}

    {if !$editMode}
        {$titleActions="{editModeContent action='edit'}"}
    {else}
        {$titleActions="{editModeContent action=''}"}
    {/if}

    {function name=deleteCellContent}
        <a href="{url controller='findings' action='delete' id=$finding->getId()}/{$targetPerson->getId()}">
            <img src="{url path='public/images/delete.png'}" alt="{t}delete{/t}" title="{t}delete{/t}"
                    onclick="return confirm('{t}confirm.delete{/t}')"/>
        </a>
    {/function}

    {if $session->flashdata('success')}
        <div class="text-align-center">
            <div class="success-msg shrink-wrap">
                {t}finding.record.successfully.deleted{/t}
            </div>
        </div>
    {/if}

    {if count($findings) == 0}
        {$titleActions=""}
    {/if}

    {sectionbox title="{t}findings{/t}" fullwidth=true titleActions=$titleActions}
        {if count($findings) > 0}
            <table class="style-table" style="margin: 10px 0">
                <tr class="tableHeader">
                    <th></th>
                    <th>{t}date{/t}</th>
                    <th>{t}details{/t}</th>
                    {if $editMode}
                        <th>{t}delete{/t}</th>
                    {/if}
                </tr>
                {if array_key_exists(Findings::GENERAL_EXAM, $findings)}
                    <tr>
                        <td><strong>{Findings::getUserVisibleNameOfType(Findings::GENERAL_EXAM)}</strong></td>
                        <td></td>
                        <td></td>
                        {if $editMode}
                            <td></td>
                        {/if}
                    </tr>
                    {foreach $findings[Findings::GENERAL_EXAM] as $type => $findingList}
                        {$findingsTypeDisplayed = false}
                        {foreach $findingList as $finding}
                            <tr>
                                <td class="subheading" style="vertical-align: top">
                                    {if !$findingsTypeDisplayed}
                                        {Findings::getUserVisibleNameOfType($type)}
                                    {$findingsTypeDisplayed = true}
                                {/if}
                                </td>
                                <td style="vertical-align: top">{$finding->getFindingsDate(true)}</td>
                                <td>{$finding->getDetails()|nl2br}</td>
                                {if $editMode}
                                    <td class="text-align-center">{deleteCellContent}</td>
                                {/if}
                            </tr>
                        {/foreach}
                    {/foreach}
                {/if}
                {foreach $findings as $type => $findingList}
                    {if $type != Findings::GENERAL_EXAM}
                        {$findingsTypeDisplayed = false}
                        {foreach $findingList as $finding}
                            <tr>
                                <td style="vertical-align: top">
                                    {if !$findingsTypeDisplayed}
                                        <strong>{Findings::getUserVisibleNameOfType($type)}</strong>
                                        {$findingsTypeDisplayed = true}
                                    {/if}
                                </td>
                                <td style="vertical-align: top; white-space: nowrap;">{$finding->getPostedOn(true)}</td>
                                <td>{$finding->getDetails()|nl2br}</td>
                                {if $editMode}
                                    <td class="text-align-center">{deleteCellContent}</td>
                                {/if}
                            </tr>
                        {/foreach}
                    {/if}
                {/foreach}
            </table>
        {else}
            <p class="text-align-center">{t}no.finding.records.available{/t}</p>
        {/if}
    {/sectionbox}

{/block}
