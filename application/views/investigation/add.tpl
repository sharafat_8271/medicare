<!--suppress ALL -->
{extends file="templates/loggedInUser.tpl"}

{block name="title"}{t}add.investigation{/t}{/block}

{block name="head_elements"}
<script type="text/javascript" src="{url path='public/js/jquery.autoresize.js'}"></script>
<script type="text/javascript">
    $(function () {
        $("textarea").autoResize();
        $(".date").attr("size", 10);
    });
</script>
<style type="text/css">
    form td, form th {
        white-space: nowrap;
    }
</style>
{/block}

{block name="loggedInBody"}

    {if $session->flashdata('success')}
        <div class="text-align-center">
            <div class="success-msg shrink-wrap">
                {t}investigations.recorded.successfully{/t}
                <a href="{url controller='investigation' action='list' id=$patient->getId()}">{t}view.investigations{/t}</a>
            </div>
        </div>
    {elseif !empty($validationErrors)}
        <div class="text-align-center">
            <div class="error-msg shrink-wrap">
                {t}correct.errors{/t}
            </div>
        </div>
    {elseif $session->flashdata('file_upload_errors')}
        <div class="text-align-center">
            <div class="warning-msg shrink-wrap" style="text-align: left">
                {t}investigation.file.upload.error{/t}<br/>
                {foreach $session->flashdata('file_upload_errors') as $error}
                    {$error}
                {/foreach}
            </div>
        </div>
    {/if}

    {php}echo form_open_multipart('investigation/add');{/php}

<div class="section-box-full-width rounded-corners">
<div class="section-box-header rounded-corners-top">{t}add.investigation{/t}</div>
<div class="section-box-body">

<div class="text-align-center" style="margin: 15px">
    <div class="info-msg shrink-wrap">
        {t}allowed.file.types.notice{/t}
    </div>
</div>

<fieldset>
    <legend>Haematopathology</legend>
    <table>
        <tr class="tableHeader">
            <th></th>
            <th>{t}date{/t}</th>
            <th>{t}remarks{/t}</th>
            <th>{t}attachment{/t}</th>
            <th></th>
        </tr>
        <tr>
            <th>Bone Marrow Study</th>
            <td>
                <input type="text" name="boneMarrowStudyDate" class="date"
                       value="{php}echo set_val('boneMarrowStudyDate', '');{/php}"/>
            </td>
            <td>
                <textarea name="boneMarrowStudyValue">{php}echo set_val('boneMarrowStudyValue', '');{/php}</textarea>
            </td>
            <td><input type="file" name="boneMarrowStudyAttachment"/></td>
            <td>{php}echo form_error('boneMarrowStudyDate');{/php}</td>
        </tr>
        <tr>
            <th>Bone Marrow Trephine</th>
            <td>
                <input type="text" name="boneMarrowTrephineDate" class="date"
                       value="{php}echo set_val('boneMarrowTrephineDate', '');{/php}"/>
            </td>
            <td>
                <textarea name="boneMarrowTrephineValue">{php}echo set_val('boneMarrowTrephineValue', '');{/php}</textarea>
            </td>
            <td><input type="file" name="boneMarrowTrephineAttachment"/></td>
            <td>{php}echo form_error('boneMarrowTrephineDate');{/php}</td>
        </tr>
        <tr>
            <th>Immunophenotyping</th>
            <td>
                <input type="text" name="immunophenotypingDate" class="date"
                       value="{php}echo set_val('immunophenotypingDate', '');{/php}"/>
            </td>
            <td>
                <textarea name="immunophenotypingValue">{php}echo set_val('immunophenotypingValue', '');{/php}</textarea>
            </td>
            <td><input type="file" name="immunophenotypingAttachment"/></td>
            <td>{php}echo form_error('immunophenotypingDate');{/php}</td>
        </tr>
        <tr>
            <th>Karyotyping</th>
            <td>
                <input type="text" name="karyotypingDate" class="date"
                       value="{php}echo set_val('karyotypingDate', '');{/php}"/>
            </td>
            <td>
                <textarea name="karyotypingValue">{php}echo set_val('karyotypingValue', '');{/php}</textarea>
            </td>
            <td><input type="file" name="karyotypingAttachment"/></td>
            <td>{php}echo form_error('karyotypingDate');{/php}</td>
        </tr>
        <tr>
            <th>CBC</th>
            <td>
                <input type="text" name="cbcDate" class="date"
                       value="{php}echo set_val('cbcDate', '');{/php}"/>
            </td>
            <td colspan="2">
                <table class="horizontal">
                    <tr>
                        <th>Hb%</th>
                        <th>ESR</th>
                        <th>TC</th>
                        <th>N%</th>
                        <th>L%</th>
                        <th>M%</th>
                        <th>E%</th>
                        <th>B%</th>
                        <th>Blast</th>
                        <th>Myelocytes</th>
                        <th></th>
                    </tr>
                    <tr>
                        <td><input type="text" size="4" name="cbcHb" value="{php}echo set_val('cbcHb', '');{/php}"/></td>
                        <td><input type="text" size="4" name="cbcEsr" value="{php}echo set_val('cbcEsr', '');{/php}"/></td>
                        <td><input type="text" size="4" name="cbcTc" value="{php}echo set_val('cbcTc', '');{/php}"/></td>
                        <td><input type="text" size="4" name="cbcN" value="{php}echo set_val('cbcN', '');{/php}"/></td>
                        <td><input type="text" size="4" name="cbcL" value="{php}echo set_val('cbcL', '');{/php}"/></td>
                        <td><input type="text" size="4" name="cbcM" value="{php}echo set_val('cbcM', '');{/php}"/></td>
                        <td><input type="text" size="4" name="cbcE" value="{php}echo set_val('cbcE', '');{/php}"/></td>
                        <td><input type="text" size="4" name="cbcB" value="{php}echo set_val('cbcB', '');{/php}"/></td>
                        <td><input type="text" size="7" name="cbcBlast" value="{php}echo set_val('cbcBlast', '');{/php}"/></td>
                        <td><input type="text" size="7" name="cbcMyelocytes" value="{php}echo set_val('cbcMyelocytes', '');{/php}"/></td>
                        <td></td>
                    </tr>
                    <tr>
                        <th>PBF</th>
                        <td colspan="10" style="text-align: left">
                            <textarea name="cbcPbf">{php}echo set_val('cbcPbf', '');{/php}</textarea>
                        </td>
                        <td>{php}echo form_error('cbcDate');{/php}</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <th>Others</th>
            <td>
                <input type="text" name="haematopathologyOthersDate" class="date"
                       value="{php}echo set_val('haematopathologyOthersDate', '');{/php}"/>
            </td>
            <td>
                <textarea name="haematopathologyOthersValue">{php}echo set_val('haematopathologyOthersValue', '');{/php}</textarea>
            </td>
            <td><input type="file" name="haematopathologyOthersAttachment"/></td>
            <td>{php}echo form_error('haematopathologyOthersDate');{/php}</td>
        </tr>
    </table>
</fieldset>

<fieldset>
    <legend>Biochemistry</legend>
    <table>
        <tr class="tableHeader">
            <th></th>
            <th>{t}date{/t}</th>
            <th>{t}remarks{/t}</th>
            <th>{t}attachment{/t}</th>
            <th></th>
        </tr>
        <tr>
            <th>S. Creatinine</th>
            <td>
                <input type="text" name="sCreatinineDate" class="date"
                       value="{php}echo set_val('sCreatinineDate', '');{/php}"/>
            </td>
            <td colspan="2"><textarea name="sCreatinineValue">{php}echo set_val('sCreatinineValue', '');{/php}</textarea></td>
            <td>{php}echo form_error('sCreatinineDate');{/php}</td>
        </tr>
        <tr>
            <th>S. Electrolyte</th>
            <td>
                <input type="text" name="sElectrolyteDate" class="date"
                       value="{php}echo set_val('sElectrolyteDate', '');{/php}"/>
            </td>
            <td colspan="2">
                <table class="horizontal">
                    <tr>
                        <th>Na<sup>+</sup></th>
                        <th>K<sup>+</sup></th>
                        <th>Cl<sup>-</sup></th>
                        <th>HCO<sub>3</sub><sup>-</sup></th>
                        <th></th>
                    </tr>
                    <tr>
                        <td>
                            <input type="text" size="4" name="sElectrolyteNa"
                                   value="{php}echo set_val('sElectrolyteNa', '');{/php}"/>
                        </td>
                        <td>
                            <input type="text" size="4" name="sElectrolyteK"
                                   value="{php}echo set_val('sElectrolyteK', '');{/php}"/>
                        </td>
                        <td>
                            <input type="text" size="4" name="sElectrolyteCl"
                                   value="{php}echo set_val('sElectrolyteCl', '');{/php}"/>
                        </td>
                        <td>
                            <input type="text" size="4" name="sElectrolyteHCO3"
                                   value="{php}echo set_val('sElectrolyteHCO3', '');{/php}"/>
                        </td>
                        <td>{php}echo form_error('sElectrolyteDate');{/php}</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <th>S. Bilirubin</th>
            <td>
                <input type="text" name="sBilirubinDate" class="date"
                       value="{php}echo set_val('sBilirubinDate', '');{/php}"/>
            </td>
            <td colspan="2"><textarea name="sBilirubinValue">{php}echo set_val('sBilirubinValue', '');{/php}</textarea></td>
            <td>{php}echo form_error('sBilirubinDate');{/php}</td>
        </tr>
        <tr>
            <th>S. SGPT</th>
            <td>
                <input type="text" name="sSgptDate" class="date"
                       value="{php}echo set_val('sSgptDate', '');{/php}"/>
            </td>
            <td colspan="2"><textarea name="sSgptValue">{php}echo set_val('sSgptValue', '');{/php}</textarea></td>
            <td>{php}echo form_error('sSgptDate');{/php}</td>
        </tr>
        <tr>
            <th>S. Alkaline Phosphatase</th>
            <td>
                <input type="text" name="sAlkalinePhosphateDate" class="date"
                       value="{php}echo set_val('sAlkalinePhosphateDate', '');{/php}"/>
            </td>
            <td colspan="2"><textarea name="sAlkalinePhosphateValue">{php}echo set_val('sAlkalinePhosphateValue', '');{/php}</textarea></td>
            <td>{php}echo form_error('sAlkalinePhosphateDate');{/php}</td>
        </tr>
        <tr>
            <th>S. Urea</th>
            <td>
                <input type="text" name="sUreaDate" class="date"
                       value="{php}echo set_val('sUreaDate', '');{/php}"/>
            </td>
            <td colspan="2"><textarea name="sUreaValue">{php}echo set_val('sUreaValue', '');{/php}</textarea></td>
            <td>{php}echo form_error('sUreaDate');{/php}</td>
        </tr>
        <tr>
            <th>S. Uric Acid</th>
            <td>
                <input type="text" name="sUricAcidDate" class="date"
                       value="{php}echo set_val('sUricAcidDate', '');{/php}"/>
            </td>
            <td colspan="2"><textarea name="sUricAcidValue">{php}echo set_val('sUricAcidValue', '');{/php}</textarea></td>
            <td>{php}echo form_error('sUricAcidDate');{/php}</td>
        </tr>
        <tr>
            <th>S. Albumin</th>
            <td>
                <input type="text" name="sAlbuminDate" class="date"
                       value="{php}echo set_val('sAlbuminDate', '');{/php}"/>
            </td>
            <td colspan="2"><textarea name="sAlbuminValue">{php}echo set_val('sAlbuminValue', '');{/php}</textarea></td>
            <td>{php}echo form_error('sAlbuminDate');{/php}</td>
        </tr>
        <tr>
            <th>S. Calcium</th>
            <td>
                <input type="text" name="sCalciumDate" class="date"
                       value="{php}echo set_val('sCalciumDate', '');{/php}"/>
            </td>
            <td colspan="2"><textarea name="sCalciumValue">{php}echo set_val('sCalciumValue', '');{/php}</textarea></td>
            <td>{php}echo form_error('sCalciumDate');{/php}</td>
        </tr>
        <tr>
            <th>S. Magnesium</th>
            <td>
                <input type="text" name="sMagnesiumDate" class="date"
                       value="{php}echo set_val('sMagnesiumDate', '');{/php}"/>
            </td>
            <td colspan="2"><textarea name="sMagnesiumValue">{php}echo set_val('sMagnesiumValue', '');{/php}</textarea></td>
            <td>{php}echo form_error('sMagnesiumDate');{/php}</td>
        </tr>
        <tr>
            <th>S. Phosphate</th>
            <td>
                <input type="text" name="sPhosphateDate" class="date"
                       value="{php}echo set_val('sPhosphateDate', '');{/php}"/>
            </td>
            <td colspan="2"><textarea name="sPhosphateValue">{php}echo set_val('sPhosphateValue', '');{/php}</textarea></td>
            <td>{php}echo form_error('sPhosphateDate');{/php}</td>
        </tr>
        <tr>
            <th>S. LDH</th>
            <td>
                <input type="text" name="sLdhDate" class="date"
                       value="{php}echo set_val('sLdhDate', '');{/php}"/>
            </td>
            <td colspan="2"><textarea name="sLdhValue">{php}echo set_val('sLdhValue', '');{/php}</textarea></td>
            <td>{php}echo form_error('sLdhDate');{/php}</td>
        </tr>
        <tr>
            <th>S. Amylase</th>
            <td>
                <input type="text" name="sAmylaseDate" class="date"
                       value="{php}echo set_val('sAmylaseDate', '');{/php}"/>
            </td>
            <td colspan="2"><textarea name="sAmylaseValue">{php}echo set_val('sAmylaseValue', '');{/php}</textarea></td>
            <td>{php}echo form_error('sAmylaseDate');{/php}</td>
        </tr>
        <tr>
            <th>S. Ferritin</th>
            <td>
                <input type="text" name="sFerritinDate" class="date"
                       value="{php}echo set_val('sFerritinDate', '');{/php}"/>
            </td>
            <td colspan="2"><textarea name="sFerritinValue">{php}echo set_val('sFerritinValue', '');{/php}</textarea></td>
            <td>{php}echo form_error('sFerritinDate');{/php}</td>
        </tr>
        <tr>
            <th>S. Vit. B<sub>12</sub></th>
            <td>
                <input type="text" name="sVitaminB12Date" class="date"
                       value="{php}echo set_val('sVitaminB12Date', '');{/php}"/>
            </td>
            <td colspan="2"><textarea name="sVitaminB12Value">{php}echo set_val('sVitaminB12Value', '');{/php}</textarea></td>
            <td>{php}echo form_error('sVitaminB12Date');{/php}</td>
        </tr>
        <tr>
            <th>S. Troponin-I</th>
            <td>
                <input type="text" name="sTroponinIDate" class="date"
                       value="{php}echo set_val('sTroponinIDate', '');{/php}"/>
            </td>
            <td colspan="2"><textarea name="sTroponinIValue">{php}echo set_val('sTroponinIValue', '');{/php}</textarea></td>
            <td>{php}echo form_error('sTroponinIDate');{/php}</td>
        </tr>
        <tr>
            <th>HbA<sub>1</sub>C</th>
            <td>
                <input type="text" name="hbA1CDate" class="date"
                       value="{php}echo set_val('hbA1CDate', '');{/php}"/>
            </td>
            <td colspan="2"><textarea name="hbA1CValue">{php}echo set_val('hbA1CValue', '');{/php}</textarea></td>
            <td>{php}echo form_error('hbA1CDate');{/php}</td>
        </tr>
        <tr>
            <th>HBsAg</th>
            <td>
                <input type="text" name="hBsAgDate" class="date"
                       value="{php}echo set_val('hBsAgDate', '');{/php}"/>
            </td>
            <td colspan="2"><textarea name="hBsAgValue">{php}echo set_val('hBsAgValue', '');{/php}</textarea></td>
            <td>{php}echo form_error('hBsAgDate');{/php}</td>
        </tr>
        <tr>
            <th>HBe IgM</th>
            <td>
                <input type="text" name="hBeIgMDate" class="date"
                       value="{php}echo set_val('hBeIgMDate', '');{/php}"/>
            </td>
            <td colspan="2"><textarea name="hBeIgMValue">{php}echo set_val('hBeIgMValue', '');{/php}</textarea></td>
            <td>{php}echo form_error('hBeIgMDate');{/php}</td>
        </tr>
        <tr>
            <th>HBc Total</th>
            <td>
                <input type="text" name="hBcTotalDate" class="date"
                       value="{php}echo set_val('hBcTotalDate', '');{/php}"/>
            </td>
            <td colspan="2"><textarea name="hBcTotalValue">{php}echo set_val('hBcTotalValue', '');{/php}</textarea></td>
            <td>{php}echo form_error('hBcTotalDate');{/php}</td>
        </tr>
        <tr>
            <th>HBV-DNA</th>
            <td>
                <input type="text" name="hBvDnaDate" class="date"
                       value="{php}echo set_val('hBvDnaDate', '');{/php}"/>
            </td>
            <td colspan="2"><textarea name="hBvDnaValue">{php}echo set_val('hBvDnaValue', '');{/php}</textarea></td>
            <td>{php}echo form_error('hBvDnaDate');{/php}</td>
        </tr>
        <tr>
            <th>Anti HCV</th>
            <td>
                <input type="text" name="antiHcvDate" class="date"
                       value="{php}echo set_val('antiHcvDate', '');{/php}"/>
            </td>
            <td colspan="2"><textarea name="antiHcvValue">{php}echo set_val('antiHcvValue', '');{/php}</textarea></td>
            <td>{php}echo form_error('antiHcvDate');{/php}</td>
        </tr>
        <tr>
            <th>Protein Electrophoresis</th>
            <td>
                <input type="text" name="proteinElectrophoresisDate" class="date"
                       value="{php}echo set_val('proteinElectrophoresisDate', '');{/php}"/>
            </td>
            <td><textarea name="proteinElectrophoresisValue">{php}echo set_val('proteinElectrophoresisValue', '');{/php}</textarea></td>
            <td><input type="file" name="proteinElectrophoresisAttachment"/></td>
            <td>{php}echo form_error('proteinElectrophoresisDate');{/php}</td>
        </tr>
        <tr>
            <th>Hb Electrophoresis</th>
            <td>
                <input type="text" name="hbElectrophoresisDate" class="date"
                       value="{php}echo set_val('hbElectrophoresisDate', '');{/php}"/>
            </td>
            <td><textarea name="hbElectrophoresisValue">{php}echo set_val('hbElectrophoresisValue', '');{/php}</textarea></td>
            <td><input type="file" name="hbElectrophoresisAttachment"/></td>
            <td>{php}echo form_error('hbElectrophoresisDate');{/php}</td>
        </tr>
        <tr>
            <th>Others</th>
            <td>
                <input type="text" name="biochemistryOthersDate" class="date"
                       value="{php}echo set_val('biochemistryOthersDate', '');{/php}"/>
            </td>
            <td><textarea name="biochemistryOthersValue">{php}echo set_val('biochemistryOthersValue', '');{/php}</textarea></td>
            <td><input type="file" name="biochemistryOthersAttachment"/></td>
            <td>{php}echo form_error('biochemistryOthersDate');{/php}</td>
        </tr>
    </table>
</fieldset>

<fieldset>
    <legend>Microbiology</legend>
    <table>
        <tr class="tableHeader">
            <th></th>
            <th>{t}date{/t}</th>
            <th>{t}remarks{/t}</th>
            <th></th>
        </tr>
        <tr>
            <th>Sputum - C/S</th>
            <td>
                <input type="text" name="sputumCsDate" class="date"
                       value="{php}echo set_val('sputumCsDate', '');{/php}"/>
            </td>
            <td><textarea name="sputumCsValue">{php}echo set_val('sputumCsValue', '');{/php}</textarea></td>
            <td>{php}echo form_error('sputumCsDate');{/php}</td>
        </tr>
        <tr>
            <th>Sputum - Gram Stain</th>
            <td>
                <input type="text" name="sputumGramStainDate" class="date"
                       value="{php}echo set_val('sputumGramStainDate', '');{/php}"/>
            </td>
            <td><textarea name="sputumGramStainValue">{php}echo set_val('sputumGramStainValue', '');{/php}</textarea></td>
            <td>{php}echo form_error('sputumGramStainDate');{/php}</td>
        </tr>
        <tr>
            <th>Sputum - AFB Stain</th>
            <td>
                <input type="text" name="sputumAfbStainDate" class="date"
                       value="{php}echo set_val('sputumAfbStainDate', '');{/php}"/>
            </td>
            <td><textarea name="sputumAfbStainValue">{php}echo set_val('sputumAfbStainValue', '');{/php}</textarea></td>
            <td>{php}echo form_error('sputumAfbStainDate');{/php}</td>
        </tr>
        <tr>
            <th>Sputum - Malignant Cell</th>
            <td>
                <input type="text" name="sputumMalignantCellDate" class="date"
                       value="{php}echo set_val('sputumMalignantCellDate', '');{/php}"/>
            </td>
            <td><textarea name="sputumMalignantCellValue">{php}echo set_val('sputumMalignantCellValue', '');{/php}</textarea></td>
            <td>{php}echo form_error('sputumMalignantCellDate');{/php}</td>
        </tr>
        <tr>
            <th>Blood - C/S</th>
            <td>
                <input type="text" name="bloodCsDate" class="date"
                       value="{php}echo set_val('bloodCsDate', '');{/php}"/>
            </td>
            <td><textarea name="bloodCsValue">{php}echo set_val('bloodCsValue', '');{/php}</textarea></td>
            <td>{php}echo form_error('bloodCsDate');{/php}</td>
        </tr>
        <tr>
            <th>Urine - C/S</th>
            <td>
                <input type="text" name="urineCsDate" class="date"
                       value="{php}echo set_val('urineCsDate', '');{/php}"/>
            </td>
            <td><textarea name="urineCsValue">{php}echo set_val('urineCsValue', '');{/php}</textarea></td>
            <td>{php}echo form_error('urineCsDate');{/php}</td>
        </tr>
        <tr>
            <th>CSF - C/S</th>
            <td>
                <input type="text" name="csfCsDate" class="date"
                       value="{php}echo set_val('csfCsDate', '');{/php}"/>
            </td>
            <td><textarea name="csfCsValue">{php}echo set_val('csfCsValue', '');{/php}</textarea></td>
            <td>{php}echo form_error('csfCsDate');{/php}</td>
        </tr>
        <tr>
            <th>Urine R/M/E</th>
            <td>
                <input type="text" name="urineRmeDate" class="date"
                       value="{php}echo set_val('urineRmeDate', '');{/php}"/>
            </td>
            <td><textarea name="urineRmeValue">{php}echo set_val('urineRmeValue', '');{/php}</textarea></td>
            <td>{php}echo form_error('urineRmeDate');{/php}</td>
        </tr>
        <tr>
            <th>Others</th>
            <td>
                <input type="text" name="microbiologyOthersDate" class="date"
                       value="{php}echo set_val('microbiologyOthersDate', '');{/php}"/>
            </td>
            <td><textarea name="microbiologyOthersValue">{php}echo set_val('microbiologyOthersValue', '');{/php}</textarea></td>
            <td>{php}echo form_error('microbiologyOthersDate');{/php}</td>
        </tr>
    </table>
</fieldset>

<fieldset>
    <legend>Radiology</legend>
    <table>
        <tr class="tableHeader">
            <th></th>
            <th>{t}date{/t}</th>
            <th>{t}remarks{/t}</th>
            <th>{t}attachment{/t}</th>
            <th></th>
        </tr>
        <tr>
            <th>X-Ray Chest</th>
            <td>
                <input type="text" name="xrayChestDate" class="date"
                       value="{php}echo set_val('xrayChestDate', '');{/php}"/>
            </td>
            <td><textarea name="xrayChestValue">{php}echo set_val('xrayChestValue', '');{/php}</textarea></td>
            <td><input type="file" name="xrayChestAttachment"/></td>
            <td>{php}echo form_error('xrayChestDate');{/php}</td>
        </tr>
        <tr>
            <th>X-Ray Spine</th>
            <td>
                <input type="text" name="xraySpineDate" class="date"
                       value="{php}echo set_val('xraySpineDate', '');{/php}"/>
            </td>
            <td><textarea name="xraySpineValue">{php}echo set_val('xraySpineValue', '');{/php}</textarea></td>
            <td><input type="file" name="xraySpineAttachment"/></td>
            <td>{php}echo form_error('xraySpineDate');{/php}</td>
        </tr>
        <tr>
            <th>X-Ray Skull</th>
            <td>
                <input type="text" name="xraySkullDate" class="date"
                       value="{php}echo set_val('xraySkullDate', '');{/php}"/>
            </td>
            <td><textarea name="xraySkullValue">{php}echo set_val('xraySkullValue', '');{/php}</textarea></td>
            <td><input type="file" name="xraySkullAttachment"/></td>
            <td>{php}echo form_error('xraySkullDate');{/php}</td>
        </tr>
        <tr>
            <th>X-Ray Abdomen</th>
            <td>
                <input type="text" name="xrayAbdomenDate" class="date"
                       value="{php}echo set_val('xrayAbdomenDate', '');{/php}"/>
            </td>
            <td><textarea name="xrayAbdomenValue">{php}echo set_val('xrayAbdomenValue', '');{/php}</textarea></td>
            <td><input type="file" name="xrayAbdomenAttachment"/></td>
            <td>{php}echo form_error('xrayAbdomenDate');{/php}</td>
        </tr>
        <tr>
            <th>X-Ray KUB</th>
            <td>
                <input type="text" name="xrayKubDate" class="date"
                       value="{php}echo set_val('xrayKubDate', '');{/php}"/>
            </td>
            <td><textarea name="xrayKubValue">{php}echo set_val('xrayKubValue', '');{/php}</textarea></td>
            <td><input type="file" name="xrayKubAttachment"/></td>
            <td>{php}echo form_error('xrayKubDate');{/php}</td>
        </tr>
        <tr>
            <th>X-Ray Pelvis</th>
            <td>
                <input type="text" name="xrayPelvisDate" class="date"
                       value="{php}echo set_val('xrayPelvisDate', '');{/php}"/>
            </td>
            <td><textarea name="xrayPelvisValue">{php}echo set_val('xrayPelvisValue', '');{/php}</textarea></td>
            <td><input type="file" name="xrayPelvisAttachment"/></td>
            <td>{php}echo form_error('xrayPelvisDate');{/php}</td>
        </tr>
        <tr>
            <th>X-Ray [Forearm / Hand /<br/> Thigh / Leg / Foot]</th>
            <td>
                <input type="text" name="xrayArmDate" class="date"
                       value="{php}echo set_val('xrayArmDate', '');{/php}"/>
            </td>
            <td><textarea name="xrayArmValue">{php}echo set_val('xrayArmValue', '');{/php}</textarea></td>
            <td><input type="file" name="xrayArmAttachment"/></td>
            <td>{php}echo form_error('xrayArmDate');{/php}</td>
        </tr>
        <tr>
            <th>USG - W/A</th>
            <td>
                <input type="text" name="usgWaDate" class="date"
                       value="{php}echo set_val('usgWaDate', '');{/php}"/>
            </td>
            <td><textarea name="usgWaValue">{php}echo set_val('usgWaValue', '');{/php}</textarea></td>
            <td><input type="file" name="usgWaAttachment"/></td>
            <td>{php}echo form_error('usgWaDate');{/php}</td>
        </tr>
        <tr>
            <th>CT Scan - [Brain /<br/> Chest/ Abdomen]</th>
            <td>
                <input type="text" name="ctScanDate" class="date"
                       value="{php}echo set_val('ctScanDate', '');{/php}"/>
            </td>
            <td><textarea name="ctScanValue">{php}echo set_val('ctScanValue', '');{/php}</textarea></td>
            <td><input type="file" name="ctScanAttachment"/></td>
            <td>{php}echo form_error('ctScanDate');{/php}</td>
        </tr>
        <tr>
            <th>MRI - Brain / Spine</th>
            <td>
                <input type="text" name="mriBrainSpineDate" class="date"
                       value="{php}echo set_val('mriBrainSpineDate', '');{/php}"/>
            </td>
            <td><textarea name="mriBrainSpineValue">{php}echo set_val('mriBrainSpineValue', '');{/php}</textarea></td>
            <td><input type="file" name="mriBrainSpineAttachment"/></td>
            <td>{php}echo form_error('mriBrainSpineDate');{/php}</td>
        </tr>
        <tr>
            <th>MRS - Brain</th>
            <td>
                <input type="text" name="mrsBrainDate" class="date"
                       value="{php}echo set_val('mrsBrainDate', '');{/php}"/>
            </td>
            <td><textarea name="mrsBrainValue">{php}echo set_val('mrsBrainValue', '');{/php}</textarea></td>
            <td><input type="file" name="mrsBrainAttachment"/></td>
            <td>{php}echo form_error('mrsBrainDate');{/php}</td>
        </tr>
        <tr>
            <th>Others</th>
            <td>
                <input type="text" name="radiologyOthersDate" class="date"
                       value="{php}echo set_val('radiologyOthersDate', '');{/php}"/>
            </td>
            <td><textarea name="radiologyOthersValue">{php}echo set_val('radiologyOthersValue', '');{/php}</textarea></td>
            <td><input type="file" name="radiologyOthersAttachment"/></td>
            <td>{php}echo form_error('radiologyOthersDate');{/php}</td>
        </tr>
    </table>
</fieldset>

<fieldset>
    <legend>Pathology</legend>
    <table>
        <tr class="tableHeader">
            <th></th>
            <th>{t}date{/t}</th>
            <th>{t}remarks{/t}</th>
            <th>{t}attachment{/t}</th>
            <th></th>
        </tr>
        <tr>
            <th>FNAC / Biopsy</th>
            <td>
                <input type="text" name="fnacBiopsyDate" class="date"
                       value="{php}echo set_val('fnacBiopsyDate', '');{/php}"/>
            </td>
            <td><textarea name="fnacBiopsyValue">{php}echo set_val('fnacBiopsyValue', '');{/php}</textarea></td>
            <td><input type="file" name="fnacBiopsyAttachment"/></td>
            <td>{php}echo form_error('fnacBiopsyDate');{/php}</td>
        </tr>
        <tr>
            <th>Pleural Fluid Study</th>
            <td>
                <input type="text" name="pleuralFluidStudyDate" class="date"
                       value="{php}echo set_val('pleuralFluidStudyDate', '');{/php}"/>
            </td>
            <td><textarea name="pleuralFluidStudyValue">{php}echo set_val('pleuralFluidStudyValue', '');{/php}</textarea></td>
            <td><input type="file" name="pleuralFluidStudyAttachment"/></td>
            <td>{php}echo form_error('pleuralFluidStudyDate');{/php}</td>
        </tr>
        <tr>
            <th>Ascitic Fluid Study</th>
            <td>
                <input type="text" name="asciticFluidStudyDate" class="date"
                       value="{php}echo set_val('asciticFluidStudyDate', '');{/php}"/>
            </td>
            <td><textarea name="asciticFluidStudyValue">{php}echo set_val('asciticFluidStudyValue', '');{/php}</textarea></td>
            <td><input type="file" name="asciticFluidStudyAttachment"/></td>
            <td>{php}echo form_error('asciticFluidStudyDate');{/php}</td>
        </tr>
        <tr>
            <th>CSF Fluid Study</th>
            <td>
                <input type="text" name="csfFluidStudyDate" class="date"
                       value="{php}echo set_val('csfFluidStudyDate', '');{/php}"/>
            </td>
            <td><textarea name="csfFluidStudyValue">{php}echo set_val('csfFluidStudyValue', '');{/php}</textarea></td>
            <td><input type="file" name="csfFluidStudyAttachment"/></td>
            <td>{php}echo form_error('csfFluidStudyDate');{/php}</td>
        </tr>
        <tr>
            <th>Others</th>
            <td>
                <input type="text" name="pathologyOthersDate" class="date"
                       value="{php}echo set_val('pathologyOthersDate', '');{/php}"/>
            </td>
            <td><textarea name="pathologyOthersValue">{php}echo set_val('pathologyOthersValue', '');{/php}</textarea></td>
            <td><input type="file" name="pathologyOthersAttachment"/></td>
            <td>{php}echo form_error('pathologyOthersDate');{/php}</td>
        </tr>
    </table>
</fieldset>

<fieldset>
    <legend>Cardiology</legend>
    <table>
        <tr class="tableHeader">
            <th></th>
            <th>{t}date{/t}</th>
            <th>{t}remarks{/t}</th>
            <th>{t}attachment{/t}</th>
            <th></th>
        </tr>
        <tr>
            <th>ECG</th>
            <td>
                <input type="text" name="ecgDate" class="date"
                       value="{php}echo set_val('ecgDate', '');{/php}"/>
            </td>
            <td><textarea name="ecgValue">{php}echo set_val('ecgValue', '');{/php}</textarea></td>
            <td><input type="file" name="ecgAttachment"/></td>
            <td>{php}echo form_error('ecgDate');{/php}</td>
        </tr>
        <tr>
            <th>Echocardiograph</th>
            <td>
                <input type="text" name="echocardiographDate" class="date"
                       value="{php}echo set_val('echocardiographDate', '');{/php}"/>
            </td>
            <td><textarea name="echocardiographValue">{php}echo set_val('echocardiographValue', '');{/php}</textarea></td>
            <td><input type="file" name="echocardiographAttachment"/></td>
            <td>{php}echo form_error('echocardiographDate');{/php}</td>
        </tr>
        <tr>
            <th>Others</th>
            <td>
                <input type="text" name="cardiologyOthersDate" class="date"
                       value="{php}echo set_val('cardiologyOthersDate', '');{/php}"/>
            </td>
            <td><textarea name="cardiologyOthersValue">{php}echo set_val('cardiologyOthersValue', '');{/php}</textarea></td>
            <td><input type="file" name="cardiologyOthersAttachment"/></td>
            <td>{php}echo form_error('cardiologyOthersDate');{/php}</td>
        </tr>
    </table>
</fieldset>

<fieldset>
    <legend>Respiratory</legend>
    <table>
        <tr class="tableHeader">
            <th></th>
            <th>{t}date{/t}</th>
            <th>{t}remarks{/t}</th>
            <th>{t}attachment{/t}</th>
            <th></th>
        </tr>
        <tr>
            <th>Bronchoscopy</th>
            <td>
                <input type="text" name="bronchoscopyDate" class="date"
                       value="{php}echo set_val('bronchoscopyDate', '');{/php}"/>
            </td>
            <td><textarea name="bronchoscopyValue">{php}echo set_val('bronchoscopyValue', '');{/php}</textarea></td>
            <td><input type="file" name="bronchoscopyAttachment"/></td>
            <td>{php}echo form_error('bronchoscopyDate');{/php}</td>
        </tr>
        <tr>
            <th>Others</th>
            <td>
                <input type="text" name="respiratoryOthersDate" class="date"
                       value="{php}echo set_val('respiratoryOthersDate', '');{/php}"/>
            </td>
            <td><textarea name="respiratoryOthersValue">{php}echo set_val('respiratoryOthersValue', '');{/php}</textarea></td>
            <td><input type="file" name="respiratoryOthersAttachment"/></td>
            <td>{php}echo form_error('respiratoryOthersDate');{/php}</td>
        </tr>
    </table>
</fieldset>

<fieldset>
    <legend>Gastroenterology</legend>
    <table>
        <tr class="tableHeader">
            <th></th>
            <th>{t}date{/t}</th>
            <th>{t}remarks{/t}</th>
            <th>{t}attachment{/t}</th>
            <th></th>
        </tr>
        <tr>
            <th>Endoscopy</th>
            <td>
                <input type="text" name="endoscopyDate" class="date"
                       value="{php}echo set_val('endoscopyDate', '');{/php}"/>
            </td>
            <td><textarea name="endoscopyValue">{php}echo set_val('endoscopyValue', '');{/php}</textarea></td>
            <td><input type="file" name="endoscopyAttachment"/></td>
            <td>{php}echo form_error('endoscopyDate');{/php}</td>
        </tr>
        <tr>
            <th>Colonoscopy</th>
            <td>
                <input type="text" name="colonoscopyDate" class="date"
                       value="{php}echo set_val('colonoscopyDate', '');{/php}"/>
            </td>
            <td><textarea name="colonoscopyValue">{php}echo set_val('colonoscopyValue', '');{/php}</textarea></td>
            <td><input type="file" name="colonoscopyAttachment"/></td>
            <td>{php}echo form_error('colonoscopyDate');{/php}</td>
        </tr>
        <tr>
            <th>ERCP</th>
            <td>
                <input type="text" name="ercpDate" class="date"
                       value="{php}echo set_val('ercpDate', '');{/php}"/>
            </td>
            <td><textarea name="ercpValue">{php}echo set_val('ercpValue', '');{/php}</textarea></td>
            <td><input type="file" name="ercpAttachment"/></td>
            <td>{php}echo form_error('ercpDate');{/php}</td>
        </tr>
        <tr>
            <th>Others</th>
            <td>
                <input type="text" name="gastroenterologyOthersDate" class="date"
                       value="{php}echo set_val('gastroenterologyOthersDate', '');{/php}"/>
            </td>
            <td><textarea name="gastroenterologyOthersValue">{php}echo set_val('gastroenterologyOthersValue', '');{/php}</textarea></td>
            <td><input type="file" name="gastroenterologyOthersAttachment"/></td>
            <td>{php}echo form_error('gastroenterologyOthersDate');{/php}</td>
        </tr>
    </table>
</fieldset>

<fieldset>
    <legend>Others</legend>
    <table>
        <tr class="tableHeader">
            <th>{t}date{/t}</th>
            <th>{t}remarks{/t}</th>
            <th>{t}attachment{/t}</th>
            <th></th>
        </tr>
        <tr>
            <td>
                <input type="text" name="othersDate" class="date"
                       value="{php}echo set_val('othersDate', '');{/php}"/>
            </td>
            <td><textarea name="othersValue">{php}echo set_val('othersValue', '');{/php}</textarea></td>
            <td><input type="file" name="othersAttachment"/></td>
            <td>{php}echo form_error('othersDate');{/php}</td>
        </tr>
    </table>
</fieldset>
</div>

<div class="section-box-footer rounded-corners-bottom">
    <input type="hidden" name="patientId" value="{$patient->getId()}"/>
    <input type="submit" name="submit" value="{t}submit{/t}"/>
    <span style="float:right">
        <input type="button" value="{t}cancel{/t}"
               onclick="window.location='{url controller='patient' action='details' id=$patient->getId()}'"/>
    </span>
</div>
</div>

</form>

{/block}
