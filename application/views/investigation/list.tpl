{extends file="templates/loggedInUser.tpl"}

{block name="title"}{t}investigations{/t}{/block}

{block name="loggedInBody"}

    {function name=editModeContent}
        <a href="{url controller='investigation' action='list' id=$patient->getId()}{if $action == 'edit'}?editMode=1{/if}">
            {if $action == 'edit'}
                <img src="{url path='public/images/edit.png'}" alt="{t}edit{/t}" title="{t}edit{/t}" style="vertical-align: bottom;"/>
                {t}edit{/t}
            {else}
                {t}cancel.edit{/t}
            {/if}
        </a>
    {/function}

    {if !$editMode}
        {$titleActions="{editModeContent action='edit'}"}
    {else}
        {$titleActions="{editModeContent action=''}"}
    {/if}

    {function name=deleteCellContent}
        {$urlPathForAttachment = ""}
        {if $investigation->getAttachmentName() != ""}
            {$urlPathForAttachment = "/?attachment={$investigation->getAttachmentName()}"}
        {/if}
        <a href="{url controller='investigation' action='delete' id=$investigation->getId()}/{$targetPerson->getId()}{$urlPathForAttachment}">
            <img src="{url path='public/images/delete.png'}" alt="{t}delete{/t}" title="{t}delete{/t}"
                    onclick="return confirm('{t}confirm.delete{/t}')"/>
        </a>
    {/function}

    {function name=printInvestigationRecords}
        {foreach $investigations[$investigationGroup] as $investigationTypes}
            {$investigationTypeDisplayed = false}
            {foreach $investigationTypes as $investigation}
            <tr>
                <td>
                    {if !$investigationTypeDisplayed}
                        {Investigation::getUserVisibleNameOfType($investigation->getType())}
                        {$investigationTypeDisplayed = true}
                    {/if}
                </td>
                <td class="text-align-center">{$investigation->getInvestigationDate(true)}</td>
                <td>{$investigation->getValue()}</td>
                <td class="text-align-center">
                    {if $investigation->getAttachmentName() != ""}
                        <a href="{url path='uploads'}/{$investigation->getAttachmentName()}"
                           target="_blank">{t}view{/t}</a>
                    {/if}
                </td>
                {if $editMode}
                    <td class="text-align-center">{deleteCellContent}</td>
                {/if}
            </tr>
            {/foreach}
        {/foreach}
    {/function}

    {function name=printInvestigationGroup}
        {if array_key_exists($investigationGroup, $investigations)}
            {sectionbox title="$title" fullwidth=true class="margin-top" titleActions=$titleActions}
            <table class="style-table" style="margin: 10px 0">
                <tr class="tableHeader">
                    <th></th>
                    <th>{t}date{/t}</th>
                    <th>{t}remarks{/t}</th>
                    <th>{t}attachment{/t}</th>
                    {if $editMode}
                        <th>{t}delete{/t}</th>
                    {/if}
                </tr>

                {printInvestigationRecords investigationGroup=$investigationGroup}
            </table>
            {/sectionbox}
        {/if}
    {/function}

    {if $session->flashdata('success')}
        <div class="text-align-center">
            <div class="success-msg shrink-wrap">
                {t}investigation.record.successfully.deleted{/t}
            </div>
        </div>
    {/if}

    {if array_key_exists(Investigation::HAEMATOPATHOLOGY, $investigations)
        || array_key_exists(Investigation::CBC, $investigations)}
        {sectionbox title="Haematopathology" fullwidth=true titleActions=$titleActions}
            <table class="style-table" style="margin: 10px 0">
                {if array_key_exists(Investigation::HAEMATOPATHOLOGY, $investigations)}
                    <tr class="tableHeader">
                        <th></th>
                        <th>{t}date{/t}</th>
                        <th>{t}remarks{/t}</th>
                        <th>{t}attachment{/t}</th>
                        {if $editMode}
                            <th>{t}delete{/t}</th>
                        {/if}
                    </tr>
                {/if}

                {if array_key_exists(Investigation::HAEMATOPATHOLOGY, $investigations)}
                    {printInvestigationRecords investigationGroup=Investigation::HAEMATOPATHOLOGY}
                {/if}

                {if array_key_exists(Investigation::CBC, $investigations)}
                    <tr>
                        <td style="vertical-align: top">CBC</td>
                        <td colspan="3">
                            <table style="border-collapse: collapse; white-space: nowrap;">
                                <tr>
                                    <th>{t}date{/t}</th>
                                    <th>Hb%</th>
                                    {if $editMode} <th></th> {/if}
                                    <th>ESR</th>
                                    {if $editMode} <th></th> {/if}
                                    <th>TC</th>
                                    {if $editMode} <th></th> {/if}
                                    <th>N%</th>
                                    {if $editMode} <th></th> {/if}
                                    <th>L%</th>
                                    {if $editMode} <th></th> {/if}
                                    <th>M%</th>
                                    {if $editMode} <th></th> {/if}
                                    <th>E%</th>
                                    {if $editMode} <th></th> {/if}
                                    <th>B%</th>
                                    {if $editMode} <th></th> {/if}
                                    <th>Blast</th>
                                    {if $editMode} <th></th> {/if}
                                    <th>Myelocytes</th>
                                    {if $editMode} <th></th> {/if}
                                    <th>PBF</th>
                                    {if $editMode} <th></th> {/if}
                                </tr>

                                {foreach $investigations[Investigation::CBC] as $date => $cbcTypes}
                                    <tr>
                                        <td class="text-align-center">{$date}</td>
                                        <td class="text-align-center">
                                            {if array_key_exists(Investigation::CBC_HB, $cbcTypes)}
                                                {$cbcTypes[Investigation::CBC_HB]->getValue()}
                                            {/if}
                                        </td>
                                        {if $editMode}
                                            <td>
                                                {if array_key_exists(Investigation::CBC_HB, $cbcTypes)}
                                                    {deleteCellContent investigation=$cbcTypes[Investigation::CBC_HB]}
                                                {/if}
                                            </td>
                                        {/if}
                                        <td class="text-align-center">
                                            {if array_key_exists(Investigation::CBC_ESR, $cbcTypes)}
                                                {$cbcTypes[Investigation::CBC_ESR]->getValue()}
                                            {/if}
                                        </td>
                                        {if $editMode}
                                            <td>
                                                {if array_key_exists(Investigation::CBC_ESR, $cbcTypes)}
                                                    {deleteCellContent investigation=$cbcTypes[Investigation::CBC_ESR]}
                                                {/if}
                                            </td>
                                        {/if}
                                        <td class="text-align-center">
                                            {if array_key_exists(Investigation::CBC_TC, $cbcTypes)}
                                                {$cbcTypes[Investigation::CBC_TC]->getValue()}
                                            {/if}
                                        </td>
                                        {if $editMode}
                                            <td>
                                                {if array_key_exists(Investigation::CBC_TC, $cbcTypes)}
                                                    {deleteCellContent investigation=$cbcTypes[Investigation::CBC_TC]}
                                                {/if}
                                            </td>
                                        {/if}
                                        <td class="text-align-center">
                                            {if array_key_exists(Investigation::CBC_N, $cbcTypes)}
                                                {$cbcTypes[Investigation::CBC_N]->getValue()}
                                            {/if}
                                        </td>
                                        {if $editMode}
                                            <td>
                                                {if array_key_exists(Investigation::CBC_N, $cbcTypes)}
                                                    {deleteCellContent investigation=$cbcTypes[Investigation::CBC_N]}
                                                {/if}
                                            </td>
                                        {/if}
                                        <td class="text-align-center">
                                            {if array_key_exists(Investigation::CBC_L, $cbcTypes)}
                                                {$cbcTypes[Investigation::CBC_L]->getValue()}
                                            {/if}
                                        </td>
                                        {if $editMode}
                                            <td>
                                                {if array_key_exists(Investigation::CBC_L, $cbcTypes)}
                                                    {deleteCellContent investigation=$cbcTypes[Investigation::CBC_L]}
                                                {/if}
                                            </td>
                                        {/if}
                                        <td class="text-align-center">
                                            {if array_key_exists(Investigation::CBC_M, $cbcTypes)}
                                                {$cbcTypes[Investigation::CBC_M]->getValue()}
                                            {/if}
                                        </td>
                                        {if $editMode}
                                            <td>
                                                {if array_key_exists(Investigation::CBC_M, $cbcTypes)}
                                                    {deleteCellContent investigation=$cbcTypes[Investigation::CBC_M]}
                                                {/if}
                                            </td>
                                        {/if}
                                        <td class="text-align-center">
                                            {if array_key_exists(Investigation::CBC_E, $cbcTypes)}
                                                {$cbcTypes[Investigation::CBC_E]->getValue()}
                                            {/if}
                                        </td>
                                        {if $editMode}
                                            <td>
                                                {if array_key_exists(Investigation::CBC_E, $cbcTypes)}
                                                    {deleteCellContent investigation=$cbcTypes[Investigation::CBC_E]}
                                                {/if}
                                            </td>
                                        {/if}
                                        <td class="text-align-center">
                                            {if array_key_exists(Investigation::CBC_B, $cbcTypes)}
                                                {$cbcTypes[Investigation::CBC_B]->getValue()}
                                            {/if}
                                        </td>
                                        {if $editMode}
                                            <td>
                                                {if array_key_exists(Investigation::CBC_B, $cbcTypes)}
                                                    {deleteCellContent investigation=$cbcTypes[Investigation::CBC_B]}
                                                {/if}
                                            </td>
                                        {/if}
                                        <td class="text-align-center">
                                            {if array_key_exists(Investigation::CBC_BLAST, $cbcTypes)}
                                                {$cbcTypes[Investigation::CBC_BLAST]->getValue()}
                                            {/if}
                                        </td>
                                        {if $editMode}
                                            <td>
                                                {if array_key_exists(Investigation::CBC_BLAST, $cbcTypes)}
                                                    {deleteCellContent investigation=$cbcTypes[Investigation::CBC_BLAST]}
                                                {/if}
                                            </td>
                                        {/if}
                                        <td class="text-align-center">
                                            {if array_key_exists(Investigation::CBC_MYELOCYTES, $cbcTypes)}
                                                {$cbcTypes[Investigation::CBC_MYELOCYTES]->getValue()}
                                            {/if}
                                        </td>
                                        {if $editMode}
                                            <td>
                                                {if array_key_exists(Investigation::CBC_MYELOCYTES, $cbcTypes)}
                                                    {deleteCellContent investigation=$cbcTypes[Investigation::CBC_MYELOCYTES]}
                                                {/if}
                                            </td>
                                        {/if}
                                        <td class="text-align-center">
                                            {if array_key_exists(Investigation::CBC_PBF, $cbcTypes)}
                                                {$cbcTypes[Investigation::CBC_PBF]->getValue()}
                                            {/if}
                                        </td>
                                        {if $editMode}
                                            <td>
                                                {if array_key_exists(Investigation::CBC_PBF, $cbcTypes)}
                                                    {deleteCellContent investigation=$cbcTypes[Investigation::CBC_PBF]}
                                                {/if}
                                            </td>
                                        {/if}
                                    </tr>
                                {/foreach}
                            </table>
                        </td>
                    </tr>
                {/if}
            </table>
        {/sectionbox}
    {/if}

    {if array_key_exists(Investigation::BIOCHEMISTRY, $investigations)
    || array_key_exists(Investigation::S_ELECTROLYTE, $investigations)}
        {sectionbox title="Biochemistry" class="margin-top" fullwidth=true titleActions=$titleActions}
        <table class="style-table" style="margin: 10px 0">
            {if array_key_exists(Investigation::BIOCHEMISTRY, $investigations)}
                <tr class="tableHeader">
                    <th></th>
                    <th>{t}date{/t}</th>
                    <th>{t}remarks{/t}</th>
                    <th>{t}attachment{/t}</th>
                    {if $editMode}
                        <th>{t}delete{/t}</th>
                    {/if}
                </tr>
            {/if}

            {if array_key_exists(Investigation::S_ELECTROLYTE, $investigations)}
                <tr>
                    <td style="vertical-align: top">S. Electrolyte</td>
                    <td colspan="3">
                        <table style="border-collapse: collapse; white-space: nowrap;">
                            <tr>
                                <th>{t}date{/t}</th>
                                <th>Na<sup>+</sup></th>
                                {if $editMode} <th></th> {/if}
                                <th>K<sup>+</sup></th>
                                {if $editMode} <th></th> {/if}
                                <th>Cl<sup>-</sup></th>
                                {if $editMode} <th></th> {/if}
                                <th>HCO<sub>3</sub><sup>-</sup></th>
                                {if $editMode} <th></th> {/if}
                            </tr>

                            {foreach $investigations[Investigation::S_ELECTROLYTE] as $date => $sElectrolyteTypes}
                                <tr>
                                    <td class="text-align-center">{$date}</td>
                                    <td class="text-align-center">
                                        {if array_key_exists(Investigation::S_ELECTROLITE_NA, $sElectrolyteTypes)}
                                            {$sElectrolyteTypes[Investigation::S_ELECTROLITE_NA]->getValue()}
                                        {/if}
                                    </td>
                                    {if $editMode}
                                        <td>
                                            {if array_key_exists(Investigation::S_ELECTROLITE_NA, $sElectrolyteTypes)}
                                                {deleteCellContent investigation=$sElectrolyteTypes[Investigation::S_ELECTROLITE_NA]}
                                            {/if}
                                        </td>
                                    {/if}
                                    <td class="text-align-center">
                                        {if array_key_exists(Investigation::S_ELECTROLITE_K, $sElectrolyteTypes)}
                                            {$sElectrolyteTypes[Investigation::S_ELECTROLITE_K]->getValue()}
                                        {/if}
                                    </td>
                                    {if $editMode}
                                        <td>
                                            {if array_key_exists(Investigation::S_ELECTROLITE_K, $sElectrolyteTypes)}
                                                {deleteCellContent investigation=$sElectrolyteTypes[Investigation::S_ELECTROLITE_K]}
                                            {/if}
                                        </td>
                                    {/if}
                                    <td class="text-align-center">
                                        {if array_key_exists(Investigation::S_ELECTROLITE_CL, $sElectrolyteTypes)}
                                            {$sElectrolyteTypes[Investigation::S_ELECTROLITE_CL]->getValue()}
                                        {/if}
                                    </td>
                                    {if $editMode}
                                        <td>
                                            {if array_key_exists(Investigation::S_ELECTROLITE_CL, $sElectrolyteTypes)}
                                                {deleteCellContent investigation=$sElectrolyteTypes[Investigation::S_ELECTROLITE_CL]}
                                            {/if}
                                        </td>
                                    {/if}
                                    <td class="text-align-center">
                                        {if array_key_exists(Investigation::S_ELECTROLITE_HCO3, $sElectrolyteTypes)}
                                            {$sElectrolyteTypes[Investigation::S_ELECTROLITE_HCO3]->getValue()}
                                        {/if}
                                    </td>
                                    {if $editMode}
                                        <td>
                                            {if array_key_exists(Investigation::S_ELECTROLITE_HCO3, $sElectrolyteTypes)}
                                                {deleteCellContent investigation=$sElectrolyteTypes[Investigation::S_ELECTROLITE_HCO3]}
                                            {/if}
                                        </td>
                                    {/if}
                                </tr>
                            {/foreach}
                        </table>
                    </td>
                </tr>
            {/if}

            {printInvestigationRecords investigationGroup=Investigation::BIOCHEMISTRY}
        </table>
        {/sectionbox}
    {/if}

    {printInvestigationGroup title="Microbiology" investigationGroup=Investigation::MICROBIOLOGY}
    {printInvestigationGroup title="Radiology" investigationGroup=Investigation::RADIOLOGY}
    {printInvestigationGroup title="Pathology" investigationGroup=Investigation::PATHOLOGY}
    {printInvestigationGroup title="Cardiology" investigationGroup=Investigation::CARDIOLOGY}
    {printInvestigationGroup title="Respiratory" investigationGroup=Investigation::RESPIRATORY}
    {printInvestigationGroup title="Gastroenterology" investigationGroup=Investigation::GASTROENTEROLOGY}
    {printInvestigationGroup title="Others" investigationGroup=Investigation::GROUP_OTHERS}

    {if count($investigations) == 0}
        {sectionbox title="{t}investigations{/t}" fullwidth=true class='text-align-center'}
            <p>{t}no.investigation.records.available{/t}</p>
        {/sectionbox}
    {/if}
{/block}
