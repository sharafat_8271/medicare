{extends file="templates/loggedInUser.tpl"}

{block name="title"}{t}presenting.features{/t}{/block}

{block name="loggedInBody"}

    {function name=editModeContent}
        <a href="{url controller='presentingFeatures' action='edit' id=$patient->getId()}">
            <img src="{url path='public/images/edit.png'}" alt="{t}edit{/t}" title="{t}edit{/t}"
                 style="vertical-align: bottom;"/>
            {t}edit{/t}
        </a>
    {/function}

    {sectionbox title="{t}presenting.features{/t}" fullwidth=true titleActions="{editModeContent}"}
        <table class="details">
            <tr>
                <th>Fever</th>
                <td>{$presentingFeatures->getFever()|nl2br}</td>
            </tr>
            <tr>
                <th>Cough</th>
                <td>{$presentingFeatures->getCough()|nl2br}</td>
            </tr>
            <tr>
                <th>Weakness</th>
                <td>{$presentingFeatures->getWeakness()|nl2br}</td>
            </tr>
            <tr>
                <th>Bodyache</th>
                <td>{$presentingFeatures->getBodyache()|nl2br}</td>
            </tr>
            <tr>
                <th>Dyspnea</th>
                <td>{$presentingFeatures->getDyspnea()|nl2br}</td>
            </tr>
            <tr>
                <th>Gum Bleeding</th>
                <td>{$presentingFeatures->getGumbleeding()|nl2br}</td>
            </tr>
            <tr>
                <th>Rash</th>
                <td>{$presentingFeatures->getRash()|nl2br}</td>
            </tr>
            <tr>
                <th>Abdominal Distension</th>
                <td>{$presentingFeatures->getAbdominalDistension()|nl2br}</td>
            </tr>
            <tr>
                <th>Bowel &amp; Bladder Incontinence</th>
                <td>{$presentingFeatures->getBowelAndBladderIncontinence()|nl2br}</td>
            </tr>
            <tr>
                <th>Blurring of Vision</th>
                <td>{$presentingFeatures->getBlurringOfVision()|nl2br}</td>
            </tr>
            <tr>
                <th>Others</th>
                <td>{$presentingFeatures->getOthers()|nl2br}</td>
            </tr>
        </table>
    {/sectionbox}

{/block}
