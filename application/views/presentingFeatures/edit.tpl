{extends file="templates/loggedInUser.tpl"}

{block name="title"}{t}edit.presenting.features{/t}{/block}

{block name="head_elements"}
<script type="text/javascript" src="{url path='public/js/jquery.autoresize.js'}"></script>
<script type="text/javascript">
    $(function () {
        $("textarea").autoResize();
    });
</script>
{/block}

{block name="loggedInBody"}

    {php}echo form_open('presentingFeatures/update');{/php}

<div class="section-box-full-width rounded-corners">
    <div class="section-box-header rounded-corners-top">{t}edit.presenting.features{/t}</div>
    <div class="section-box-body">
        <table>
            <tr>
                <th>Fever</th>
                <td><textarea name="fever">{php}echo set_val('fever', '{$presentingFeatures->getFever()}');{/php}</textarea></td>
            </tr>
            <tr>
                <th>Cough</th>
                <td><textarea name="cough">{php}echo set_val('cough', '{$presentingFeatures->getCough()}');{/php}</textarea></td>
            </tr>
            <tr>
                <th>Weakness</th>
                <td><textarea name="weakness">{php}echo set_val('weakness', '{$presentingFeatures->getWeakness()}');{/php}</textarea></td>
            </tr>
            <tr>
                <th>Bodyache</th>
                <td><textarea name="bodyache">{php}echo set_val('bodyache', '{$presentingFeatures->getBodyache()}');{/php}</textarea></td>
            </tr>
            <tr>
                <th>Dyspnea</th>
                <td><textarea name="dyspnea">{php}echo set_val('dyspnea', '{$presentingFeatures->getDyspnea()}');{/php}</textarea></td>
            </tr>
            <tr>
                <th>Gum Bleeding</th>
                <td><textarea name="gumbleeding">{php}echo set_val('gumbleeding', '{$presentingFeatures->getGumbleeding()}');{/php}</textarea></td>
            </tr>
            <tr>
                <th>Rash</th>
                <td><textarea name="rash">{php}echo set_val('rash', '{$presentingFeatures->getRash()}');{/php}</textarea></td>
            </tr>
            <tr>
                <th>Abdominal Distension</th>
                <td><textarea name="abdominalDistension">{php}echo set_val('abdominalDistension', '{$presentingFeatures->getAbdominalDistension()}');{/php}</textarea></td>
            </tr>
            <tr>
                <th>Bowel &amp; Bladder Incontinence</th>
                <td><textarea name="bowelAndBladderIncontinence">{php}echo set_val('bowelAndBladderIncontinence', '{$presentingFeatures->getBowelAndBladderIncontinence()}');{/php}</textarea></td>
            </tr>
            <tr>
                <th>Blurring of Vision</th>
                <td><textarea name="blurringOfVision">{php}echo set_val('blurringOfVision', '{$presentingFeatures->getBlurringOfVision()}');{/php}</textarea></td>
            </tr>
            <tr>
                <th>Others</th>
                <td><textarea name="others">{php}echo set_val('others', '{$presentingFeatures->getOthers()}');{/php}</textarea></td>
            </tr>
        </table>
    </div>

    <div class="section-box-footer rounded-corners-bottom">
        <input type="hidden" name="patientId" value="{$patient->getId()}"/>
        <input type="submit" name="submit" value="{t}update{/t}"/>
        <span style="float:right">
            <input type="button" value="{t}cancel{/t}"
                   onclick="window.location='{url controller='patient' action='details' id=$patient->getId()}'"/>
        </span>
    </div>
</div>

</form>

{/block}
