{extends file="templates/loggedInUser.tpl"}

{block name="title"}{t}admission.discharge.history{/t}{/block}

{block name="head_elements"}
<script type="text/javascript">
    $(function () {
        $(".styleTable").styleTable();
    });
</script>
{/block}

{block name="loggedInBody"}

    {function name=editModeContent}
        <a href="{url controller='admission' action='list' id=$patient->getId()}{if $action == 'edit'}?editMode=1{/if}">
            {if $action == 'edit'}
                <img src="{url path='public/images/edit.png'}" alt="{t}edit{/t}" title="{t}edit{/t}" style="vertical-align: bottom;"/>
                {t}edit{/t}
            {else}
                {t}cancel.edit{/t}
            {/if}
        </a>
    {/function}

    {if count($admissionHistory) > 0}
        {if !$editMode}
            {$titleActions="{editModeContent action='edit'}"}
        {else}
            {$titleActions="{editModeContent action=''}"}
        {/if}
    {else}
        {$titleActions=""}
    {/if}

    {if $session->flashdata('success')}
        <div class="text-align-center">
            <div class="success-msg shrink-wrap">
                {t}admission.discharge.record.successfully.deleted{/t}
            </div>
        </div>
    {/if}

    {sectionbox title="{t}admission.discharge.history{/t}" fullwidth=true titleActions=$titleActions}
        {if count($admissionHistory) > 0}
            <table class="styleTable" style="margin: 25px auto">
                {include file="admission/list_table_rows.tpl"}
            </table>
        {else}
            <p class="text-align-center">{t}admission.discharge.history.not.available{/t}</p>
        {/if}
    {/sectionbox}
{/block}
