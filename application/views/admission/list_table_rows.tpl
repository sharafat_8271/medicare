<style type="text/css">
    td {
        vertical-align: top;
    }

    td.nowrap {
        white-space: nowrap;
    }
</style>

{function name=deleteCellContent}
    <a href="{url controller='admission' action='delete' id=$record->getId()}/{$targetPerson->getId()}">
        <img src="{url path='public/images/delete.png'}" alt="{t}delete{/t}" title="{t}delete{/t}"
             onclick="return confirm('{t}confirm.delete{/t}')"/>
    </a>
{/function}

<tr>
    <th colspan="{if $editMode}3{else}2{/if}">{t}admission{/t}</th>
    <th colspan="{if $editMode}3{else}2{/if}">{t}discharge{/t}</th>
</tr>
<tr>
    <th>{t}date{/t}</th>
    <th>{t}discussion{/t}</th>
    {if $editMode}
        <th>{t}delete{/t}</th>
    {/if}
        <th>{t}date{/t}</th>
        <th>{t}discussion{/t}</th>
    {if $editMode}
        <th>{t}delete{/t}</th>
    {/if}
</tr>

{foreach $admissionHistory as $serial => $admission}
    <tr>
        <td class="nowrap">{$admission->getEventDate(true)}</td>
        <td>{$admission->getDiscussion()|nl2br}</td>
        {if $editMode}
            <td style="text-align: center">
                {if ($serial == count($admissionHistory) - 1) && !array_key_exists($serial, $dischargeHistory)}
                    {deleteCellContent record=$admission}
                {/if}
            </td>
        {/if}
        <td class="nowrap">
            {if array_key_exists($serial, $dischargeHistory)}
                {$dischargeHistory[$serial]->getEventDate(true)}
            {/if}
        </td>
        <td>
            {if array_key_exists($serial, $dischargeHistory)}
                {$dischargeHistory[$serial]->getDiscussion()|nl2br}
            {/if}
        </td>
        {if $editMode}
            <td style="text-align: center">
                {if ($serial == count($admissionHistory) - 1) && array_key_exists($serial, $dischargeHistory)}
                    {deleteCellContent record=$dischargeHistory[$serial]}
                {/if}
            </td>
        {/if}
    </tr>
{/foreach}
