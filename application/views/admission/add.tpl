<!--suppress ALL -->
{extends file="templates/loggedInUser.tpl"}

{block name="title"}
    {if $patient->isIndoor()}
        {t}discharge.patient{/t}
        {$pageTitle = 'discharge.patient'}
    {else}
        {t}admit.patient{/t}
        {$pageTitle = 'admit.patient'}
    {/if}
{/block}

{block name="head_elements"}
<script type="text/javascript" src="{url path='public/js/jquery.autoresize.js'}"></script>
<script type="text/javascript">
    $(function () {
        setEqualWidth(filterSameWidthInclusiveElements($("input[type='text'], textarea")));
    });
</script>
<style type="text/css">
    textarea {
        height: auto;
    }
</style>
{/block}

{block name="loggedInBody"}

    {php}echo form_open_multipart('admission/add');{/php}

<div class="section-box-full-width rounded-corners">
<div class="section-box-header rounded-corners-top">{t}{$pageTitle}{/t}</div>
<div class="section-box-body">
    <table>
        <tr>
            <th>{t}date{/t} {required}</th>
            <td>
                <input type="text" name="date" class="date" readonly="readonly"
                       value="{php}echo set_val('date', '{$smarty.now|date_format:Person::DOB_INPUT_FORMAT}');{/php}"/>
            </td>
            <td>{php}echo form_error('date');{/php}</td>
        </tr>
        <tr>
            <th>{t}discussion{/t}</th>
            <td><textarea name="discussion" rows="3" cols="20">{php}echo set_val('discussion', '');{/php}</textarea></td>
        </tr>
    </table>
</div>

<div class="section-box-footer rounded-corners-bottom">
    <input type="hidden" name="patientId" value="{$patient->getId()}"/>
    <input type="submit" name="submit" value="{t}{$pageTitle}{/t}"/>
    <span style="float:right">
        <input type="button" value="{t}cancel{/t}"
               onclick="window.location='{url controller='patient' action='details' id=$patient->getId()}'"/>
    </span>
</div>
</div>

</form>

{/block}
