<?php
/**
 * Author:  sharafat
 * Created: 5/5/13 5:53 AM
 */

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/*
 * http://stackoverflow.com/questions/11813009/disabling-error-logging-in-codeigniter-for-a-specific-helper-library
 */
class Cryptic_Exceptions extends CI_Exceptions {

    function log_exception($severity, $message, $filepath, $line) {
        $filesExcludedFromLogging = "/mpdf.php/";

        if (!preg_match($filesExcludedFromLogging, $filepath)) {
            parent::log_exception($severity, $message, $filepath, $line);
        }
    }
}
