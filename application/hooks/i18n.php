<?php
/**
 * Author:  sharafat
 * Created: 8/29/12 7:08 AM
 */

/*
 * http://stackoverflow.com/questions/2222558/codeigniter-language
 */
function pickLanguage() {
    require_once(APPPATH . '/config/language.php');

    // Lang set in URL via ?lang=something
    if (!empty($_GET['lang'])) {
//         Turn en-gb into en
//        $lang = substr($_GET['lang'], 0, 2);
        $lang = $_GET['lang'];
        $_SESSION['lang_code'] = $lang;
    } // Lang has already been set and is stored in a session
    elseif (!empty($_SESSION['lang_code'])) {
        $lang = $_SESSION['lang_code'];
    }
    // Lang is picked by a user.
    // Set it to a session variable so we are only checking one place most of the time
    elseif (!empty($_COOKIE['lang_code'])) {
        $lang = $_SESSION['lang_code'] = $_COOKIE['lang_code'];
    }
    // Still no Lang. Lets try some browser detection then
    else {
        if (!empty($_SERVER['HTTP_ACCEPT_LANGUAGE'])) {
            // explode languages into array
            $accept_langs = explode(',', $_SERVER['HTTP_ACCEPT_LANGUAGE']);

            log_message('debug', 'Checking browser languages: ' . implode(', ', $accept_langs));

            // Check them all, until we find a match
            foreach ($accept_langs as $lang) {
//                 Turn en-gb into en
//                $lang = substr($lang, 0, 2);

                // Check whether it's in the array. If so, break the loop, we have one!
                if (in_array($lang, array_keys($config['supported_languages']))) {
                    break;
                }
            }
        }
    }

    // If no language has been worked out - or it is not supported - use the default
    if (empty($lang) or !in_array($lang, array_keys($config['supported_languages']))) {
        $lang = $config['default_language'];
    }

    // Whatever we decided the lang was, save it for next time to avoid working it out again
    $_SESSION['lang_code'] = $lang;

    // Load CI config class
    $CI_config =& load_class('Config');

    // Set the language config. Selects the messages.ini file language from its key of $lang
    $CI_config->set_item('language', $config['supported_languages'][$lang]['lang']);

    // Sets a constant to use throughout ALL of CI.
    define('CURRENT_LANGUAGE', $lang);
}
