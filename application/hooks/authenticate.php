<?php
/**
 * Author:  sharafat
 * Created: 12/7/12 8:38 PM
 */
function authenticate() {
    $ci =& get_instance();
    $controller = get_class($ci);
    if (!in_array($controller, array('Login', 'Logout')) && !array_key_exists('login', $_SESSION)) {
        redirect('/login');
        return;
    }
}
