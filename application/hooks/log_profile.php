<?php
/**
 * Author:  sharafat
 * Created: 5/6/13 7:47 PM
 */

function log_uri() {
    $requestUri = $_SERVER['REQUEST_URI'];
    Logger::getLogger("URI")->info("RequestURI=$requestUri");
}

function log_profile() {
    $ci =& get_instance();

    $requestUri = $_SERVER['REQUEST_URI'];
    $elapsedTime = $ci->benchmark->elapsed_time('total_execution_time_start', 'total_execution_time_end');
    $memoryUsage = (!function_exists('memory_get_usage')) ? '0' : round(memory_get_usage() / 1024 / 1024, 2);

    Logger::getLogger("PROFILER")->info("RequestURI=$requestUri"
                                            . " elapsedTime=$elapsedTime seconds; memoryUsage=$memoryUsage MB");
}
