<?php
/**
 * Author:  sharafat
 * Created: 5/1/13 7:48 PM
 */
function globalExceptionHandler(Exception $exception) {
    $log = Logger::getLogger("GlobalErrorHandler");
    $log->error("", $exception);

    require_once APPPATH . '/errors/error_general.php';

    exit;
}

function handleShutdown() {
    if (($error = error_get_last()) && error_reporting() !== 0 /* suppressed warnings with @ */) {
        $log = Logger::getLogger("GlobalErrorHandler");
        $log->error(getErrorDescription($error['type']) . " {$error['message']} {$error['file']} {$error['line']}");

        require_once APPPATH . '/errors/error_general.php';
    }
}

function getErrorDescription($errorCode) {
    switch ($errorCode) {
        case E_ERROR:
            return "E_ERROR";
        case E_WARNING:
            return "E_WARNING";
        case E_PARSE:
            return "E_PARSE";
        case E_NOTICE:
            return "E_NOTICE";
        case E_CORE_ERROR:
            return "E_CORE_ERROR";
        case E_CORE_WARNING:
            return "E_CORE_WARNING";
        case E_COMPILE_ERROR:
            return "E_COMPILE_ERROR";
        case E_COMPILE_WARNING:
            return "E_COMPILE_WARNING";
        case E_USER_ERROR:
            return "E_USER_ERROR";
        case E_USER_WARNING:
            return "E_USER_WARNING";
        case E_USER_NOTICE:
            return "E_USER_NOTICE";
        default:
            return $errorCode;
    }
}

function setGlobalExceptionHandler() {
    set_exception_handler('globalExceptionHandler');
    register_shutdown_function('handleShutdown');
}
