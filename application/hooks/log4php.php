<?php
require_once APPPATH . 'libraries/log4php/Logger.php';

function configureLog4php() {
    Logger::configure(APPPATH . "config/log4php.xml");
}
