<?php

namespace models;

/**
 * Author:  sharafat
 * Created: 1/8/13 11:44 PM
 *
 * @Entity
 */
class PresentingFeatures {
    /**
     * @Id
     * @GeneratedValue
     * @Column(type="integer")
     * @var int
     */
    private $id;

    /**
     * @Column(type="string")
     * @var string
     */
    private $fever;

    /**
     * @Column(type="string")
     * @var string
     */
    private $cough;

    /**
     * @Column(type="string")
     * @var string
     */
    private $weakness;

    /**
     * @Column(type="string")
     * @var string
     */
    private $bodyache;

    /**
     * @Column(type="string")
     * @var string
     */
    private $dyspnea;

    /**
     * @Column(type="string")
     * @var string
     */
    private $gumbleeding;

    /**
     * @Column(type="string")
     * @var string
     */
    private $rash;

    /**
     * @Column(type="string")
     * @var string
     */
    private $abdominalDistension;

    /**
     * @Column(type="string")
     * @var string
     */
    private $bowelAndBladderIncontinence;

    /**
     * @Column(type="string")
     * @var string
     */
    private $blurringOfVision;

    /**
     * @Column(type="string")
     * @var string
     */
    private $others;

    /**
     * @param string $abdominalDistension
     */
    public function setAbdominalDistension($abdominalDistension) {
        $this->abdominalDistension = $abdominalDistension;
    }

    /**
     * @return string
     */
    public function getAbdominalDistension() {
        return $this->abdominalDistension;
    }

    /**
     * @param string $blurringOfVision
     */
    public function setBlurringOfVision($blurringOfVision) {
        $this->blurringOfVision = $blurringOfVision;
    }

    /**
     * @return string
     */
    public function getBlurringOfVision() {
        return $this->blurringOfVision;
    }

    /**
     * @param string $bodyache
     */
    public function setBodyache($bodyache) {
        $this->bodyache = $bodyache;
    }

    /**
     * @return string
     */
    public function getBodyache() {
        return $this->bodyache;
    }

    /**
     * @param string $bowelAndBladderIncontinence
     */
    public function setBowelAndBladderIncontinence($bowelAndBladderIncontinence) {
        $this->bowelAndBladderIncontinence = $bowelAndBladderIncontinence;
    }

    /**
     * @return string
     */
    public function getBowelAndBladderIncontinence() {
        return $this->bowelAndBladderIncontinence;
    }

    /**
     * @param string $cough
     */
    public function setCough($cough) {
        $this->cough = $cough;
    }

    /**
     * @return string
     */
    public function getCough() {
        return $this->cough;
    }

    /**
     * @param string $dyspnea
     */
    public function setDyspnea($dyspnea) {
        $this->dyspnea = $dyspnea;
    }

    /**
     * @return string
     */
    public function getDyspnea() {
        return $this->dyspnea;
    }

    /**
     * @param string $fever
     */
    public function setFever($fever) {
        $this->fever = $fever;
    }

    /**
     * @return string
     */
    public function getFever() {
        return $this->fever;
    }

    /**
     * @param string $gumbleeding
     */
    public function setGumbleeding($gumbleeding) {
        $this->gumbleeding = $gumbleeding;
    }

    /**
     * @return string
     */
    public function getGumbleeding() {
        return $this->gumbleeding;
    }

    /**
     * @param int $id
     */
    public function setId($id) {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @param string $others
     */
    public function setOthers($others) {
        $this->others = $others;
    }

    /**
     * @return string
     */
    public function getOthers() {
        return $this->others;
    }

    /**
     * @param string $rash
     */
    public function setRash($rash) {
        $this->rash = $rash;
    }

    /**
     * @return string
     */
    public function getRash() {
        return $this->rash;
    }

    /**
     * @param string $weakness
     */
    public function setWeakness($weakness) {
        $this->weakness = $weakness;
    }

    /**
     * @return string
     */
    public function getWeakness() {
        return $this->weakness;
    }
}
