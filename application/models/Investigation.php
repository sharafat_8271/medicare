<?php

namespace models;

use DateTime;

/**
 * Author:  sharafat
 * Created: 12/07/12 08:52 AM
 *
 * @Entity
 */
class Investigation {
    const HAEMATOPATHOLOGY = 1;
    const CBC = 2;
    const S_ELECTROLYTE = 3;
    const BIOCHEMISTRY = 4;
    const MICROBIOLOGY = 5;
    const RADIOLOGY = 6;
    const PATHOLOGY = 7;
    const CARDIOLOGY = 8;
    const RESPIRATORY = 9;
    const GASTROENTEROLOGY = 10;
    const GROUP_OTHERS = 11;

    const BONE_MARROW_STUDY = 1;
    const BONE_MARROW_TREPHINE = 2;
    const IMMUNOPHENOTYPING = 3;
    const KARYOTYPING = 4;
    const HAEMATOPATHOLOGY_OTHERS = 5;
    const CBC_HB = 6;
    const CBC_ESR = 7;
    const CBC_TC = 8;
    const CBC_N = 9;
    const CBC_L = 10;
    const CBC_M = 11;
    const CBC_E = 12;
    const CBC_B = 13;
    const CBC_BLAST = 14;
    const CBC_MYELOCYTES = 15;
    const CBC_PBF = 16;
    const S_ELECTROLITE_NA = 17;
    const S_ELECTROLITE_K = 18;
    const S_ELECTROLITE_CL = 19;
    const S_ELECTROLITE_HCO3 = 20;
    const S_CREATININE = 21;
    const S_BILIRUBIN = 22;
    const S_SGPT = 23;
    const S_ALKALINE_PHOSPHATASE = 24;
    const S_UREA = 25;
    const S_URIC_ACID = 26;
    const S_ALBUMIN = 27;
    const S_CALCIUM = 28;
    const S_MAGNESIUM = 29;
    const S_PHOSPHATE = 30;
    const S_LDH = 31;
    const S_AMYLASE = 32;
    const S_FERRITIN = 33;
    const S_VITAMIN_B = 34;
    const S_TROPONIN_I = 35;
    const HBA1C = 36;
    const HBS_AG = 37;
    const HBE_IG_M = 38;
    const HBC_TOTAL = 39;
    const HBV_DNA = 40;
    const ANTI_HCV = 41;
    const PROTEIN_ELECTROPHORESIS = 42;
    const HB_ELECTROPHORESIS = 43;
    const BIOCHEMISTRY_OTHERS = 44;
    const SPUTUM_CS = 45;
    const SPUTUM_GRAM_STAIN = 46;
    const SPUTUM_AFB_STAIN = 47;
    const SPUTUM_MALIGNANT_CELL = 48;
    const BLOOD_CS = 49;
    const URINE_CS = 50;
    const CSF_CS = 51;
    const URINE_RME = 52;
    const MICROBIOLOGY_OTHERS = 53;
    const XRAY_CHEST = 54;
    const XRAY_SPINE = 55;
    const XRAY_SKULL = 56;
    const XRAY_ABDOMEN = 57;
    const XRAY_KUB = 58;
    const XRAY_PELVIS = 59;
    const XRAY_ARM = 60;
    const USG_WA = 61;
    const CT_SCAN = 62;
    const MRI = 63;
    const MRS = 64;
    const RADIOLOGY_OTHERS = 65;
    const FNAC_BIOPSY = 66;
    const PLEURAL_FLUID_STUDY = 67;
    const ASCITIC_FLUID_STUDY = 68;
    const CSF_FLUID_STUDY = 69;
    const PATHOLOGY_OTHERS = 70;
    const ECG = 71;
    const ECHOCARDIOGRAPH = 72;
    const CARDIOLOGY_OTHERS = 73;
    const BRONCHOSCOPY = 74;
    const RESPIRATORY_OTHERS = 75;
    const ENDOSCOPY = 76;
    const COLONOSCOPY = 77;
    const ERCP = 78;
    const GASTROENTEROLOGY_OTHERS = 79;
    const OTHERS = 80;

    const INVESTIGATION_DATE_DISPLAY_FORMAT = "d-m-Y";

    public static function getGroupByType($type) {
        if ($type >= 1 && $type <= 5) {
            return self::HAEMATOPATHOLOGY;
        } elseif ($type >= 6 && $type <= 16) {
            return self::CBC;
        } elseif ($type >= 17 && $type <= 20) {
            return self::S_ELECTROLYTE;
        } elseif ($type >= 21 && $type <= 44) {
            return self::BIOCHEMISTRY;
        } elseif ($type >= 45 && $type <= 53) {
            return self::MICROBIOLOGY;
        } elseif ($type >= 54 && $type <= 65) {
            return self::RADIOLOGY;
        } elseif ($type >= 66 && $type <= 70) {
            return self::PATHOLOGY;
        } elseif ($type >= 71 && $type <= 73) {
            return self::CARDIOLOGY;
        } elseif ($type >= 74 && $type <= 75) {
            return self::RESPIRATORY;
        } elseif ($type >= 76 && $type <= 79) {
            return self::GASTROENTEROLOGY;
        } elseif ($type == 80) {
            return self::GROUP_OTHERS;
        } else {
            return null;
        }
    }

    public static function getTypeByName($name) {
        switch ($name) {
            case "boneMarrowStudy":
                return self::BONE_MARROW_STUDY;
            case "boneMarrowTrephine":
                return self::BONE_MARROW_TREPHINE;
            case "immunophenotyping":
                return self::IMMUNOPHENOTYPING;
            case "karyotyping":
                return self::KARYOTYPING;
            case "haematopathologyOthers":
                return self::HAEMATOPATHOLOGY_OTHERS;

            case "cbcHb":
                return self::CBC_HB;
            case "cbcEsr":
                return self::CBC_ESR;
            case "cbcTc":
                return self::CBC_TC;
            case "cbcN":
                return self::CBC_N;
            case "cbcL":
                return self::CBC_L;
            case "cbcM":
                return self::CBC_M;
            case "cbcE":
                return self::CBC_E;
            case "cbcB":
                return self::CBC_B;
            case "cbcBlast":
                return self::CBC_BLAST;
            case "cbcMyelocytes":
                return self::CBC_MYELOCYTES;
            case "cbcPbf":
                return self::CBC_PBF;

            case "sElectrolyteNa":
                return self::S_ELECTROLITE_NA;
            case "sElectrolyteK":
                return self::S_ELECTROLITE_K;
            case "sElectrolyteCl":
                return self::S_ELECTROLITE_CL;
            case "sElectrolyteHCO3":
                return self::S_ELECTROLITE_HCO3;

            case "sCreatinine":
                return self::S_CREATININE;
            case "sBilirubin":
                return self::S_BILIRUBIN;
            case "sSgpt":
                return self::S_SGPT;
            case "sAlkalinePhosphate":
                return self::S_ALKALINE_PHOSPHATASE;
            case "sUrea":
                return self::S_UREA;
            case "sUricAcid":
                return self::S_URIC_ACID;
            case "sAlbumin":
                return self::S_ALBUMIN;
            case "sCalcium":
                return self::S_CALCIUM;
            case "sMagnesium":
                return self::S_MAGNESIUM;
            case "sPhosphate":
                return self::S_PHOSPHATE;
            case "sLdh":
                return self::S_LDH;
            case "sAmylase":
                return self::S_AMYLASE;
            case "sFerritin":
                return self::S_FERRITIN;
            case "sVitaminB12":
                return self::S_VITAMIN_B;
            case "sTroponinI":
                return self::S_TROPONIN_I;
            case "hbA1C":
                return self::HBA1C;
            case "hBsAg":
                return self::HBS_AG;
            case "hBeIgM":
                return self::HBE_IG_M;
            case "hBcTotal":
                return self::HBC_TOTAL;
            case "hBvDna":
                return self::HBV_DNA;
            case "antiHcv":
                return self::ANTI_HCV;
            case "proteinElectrophoresis":
                return self::PROTEIN_ELECTROPHORESIS;
            case "hbElectrophoresis":
                return self::HB_ELECTROPHORESIS;
            case "biochemistryOthers":
                return self::BIOCHEMISTRY_OTHERS;

            case "sputumCs":
                return self::SPUTUM_CS;
            case "sputumGramStain":
                return self::SPUTUM_GRAM_STAIN;
            case "sputumAfbStain":
                return self::SPUTUM_AFB_STAIN;
            case "sputumMalignantCell":
                return self::SPUTUM_MALIGNANT_CELL;
            case "bloodCs":
                return self::BLOOD_CS;
            case "urineCs":
                return self::URINE_CS;
            case "csfCs":
                return self::CSF_CS;
            case "urineRme":
                return self::URINE_RME;
            case "microbiologyOthers":
                return self::MICROBIOLOGY_OTHERS;

            case "xrayChest":
                return self::XRAY_CHEST;
            case "xraySpine":
                return self::XRAY_SPINE;
            case "xraySkull":
                return self::XRAY_SKULL;
            case "xrayAbdomen":
                return self::XRAY_ABDOMEN;
            case "xrayKub":
                return self::XRAY_KUB;
            case "xrayPelvis":
                return self::XRAY_PELVIS;
            case "xrayArm":
                return self::XRAY_ARM;
            case "usgWa":
                return self::USG_WA;
            case "ctScan":
                return self::CT_SCAN;
            case "mriBrainSpine":
                return self::MRI;
            case "mrsBrain":
                return self::MRS;
            case "radiologyOthers":
                return self::RADIOLOGY_OTHERS;

            case "fnacBiopsy":
                return self::FNAC_BIOPSY;
            case "pleuralFluidStudy":
                return self::PLEURAL_FLUID_STUDY;
            case "asciticFluidStudy":
                return self::ASCITIC_FLUID_STUDY;
            case "csfFluidStudy":
                return self::CSF_FLUID_STUDY;
            case "pathologyOthers":
                return self::PATHOLOGY_OTHERS;

            case "ecg":
                return self::ECG;
            case "echocardiograph":
                return self::ECHOCARDIOGRAPH;
            case "cardiologyOthers":
                return self::CARDIOLOGY_OTHERS;

            case "bronchoscopy":
                return self::BRONCHOSCOPY;
            case "respiratoryOthers":
                return self::RESPIRATORY_OTHERS;

            case "endoscopy":
                return self::ENDOSCOPY;
            case "colonoscopy":
                return self::COLONOSCOPY;
            case "ercp":
                return self::ERCP;
            case "gastroenterologyOthers":
                return self::GASTROENTEROLOGY_OTHERS;

            case "others":
                return self::OTHERS;

            default:
                return null;
        }
    }

    public static function getUserVisibleNameOfType($type) {
        switch ($type) {
            case self::BONE_MARROW_STUDY:
                return "Bone Marrow Study";
            case self::BONE_MARROW_TREPHINE:
                return "Bone Marrow Trephine";
            case self::IMMUNOPHENOTYPING:
                return "Immunophenotyping";
            case self::KARYOTYPING:
                return "Karyotyping";

            case self::S_CREATININE:
                return "S. Creatinine";
            case self::S_BILIRUBIN:
                return "S. Bilirubin";
            case self::S_SGPT:
                return "S. SGPT";
            case self::S_ALKALINE_PHOSPHATASE:
                return "S. Alkaline Phosphatase";
            case self::S_UREA:
                return "S. Urea";
            case self::S_URIC_ACID:
                return "S. Uric Acid";
            case self::S_ALBUMIN:
                return "S. Albumin";
            case self::S_CALCIUM:
                return "S. Calcium";
            case self::S_MAGNESIUM:
                return "S. Magnesium";
            case self::S_PHOSPHATE:
                return "S. Phosphate";
            case self::S_LDH:
                return "S. LDH";
            case self::S_AMYLASE:
                return "S. Amylase";
            case self::S_FERRITIN:
                return "S. Ferritin";
            case self::S_VITAMIN_B:
                return "S. Vit. B<sub>12</sub>";
            case self::S_TROPONIN_I:
                return "S. Troponin-I";
            case self::HBA1C:
                return "HbA<sup>1</sup>C";
            case self::HBS_AG:
                return "HBsAg";
            case self::HBE_IG_M:
                return "HBe IgM";
            case self::HBC_TOTAL:
                return "HBc Total";
            case self::HBV_DNA:
                return "HBV-DNA";
            case self::ANTI_HCV:
                return "Anti HCV";
            case self::PROTEIN_ELECTROPHORESIS:
                return "Protein Electrophoresis";
            case self::HB_ELECTROPHORESIS:
                return "Hb Electrophoresis";

            case self::SPUTUM_CS:
                return "Sputum - C/S";
            case self::SPUTUM_GRAM_STAIN:
                return "Sputum - Gram Stain";
            case self::SPUTUM_AFB_STAIN:
                return "Sputum - AFB Stain";
            case self::SPUTUM_MALIGNANT_CELL:
                return "Sputum - Malignant Cell";
            case self::BLOOD_CS:
                return "Blood - C/S";
            case self::URINE_CS:
                return "Urine - C/S";
            case self::CSF_CS:
                return "CSF - C/S";
            case self::URINE_RME:
                return "Urine R/M/E";

            case self::XRAY_CHEST:
                return "X-Ray Chest";
            case self::XRAY_SPINE:
                return "X-Ray Spine";
            case self::XRAY_SKULL:
                return "X-Ray Skull";
            case self::XRAY_ABDOMEN:
                return "X-Ray Abdomen";
            case self::XRAY_KUB:
                return "X-Ray KUB";
            case self::XRAY_PELVIS:
                return "X-Ray Pelvis";
            case self::XRAY_ARM:
                return "X-Ray [Forearm / Hand / Thigh / Leg / Foot]";
            case self::USG_WA:
                return "USG - W/A";
            case self::CT_SCAN:
                return "CT Scan - [Brain / Chest/ Abdomen]";
            case self::MRI:
                return "MRI - Brain / Spine";
            case self::MRS:
                return "MRS - Brain";

            case self::FNAC_BIOPSY:
                return "FNAC / Biopsy";
            case self::PLEURAL_FLUID_STUDY:
                return "Pleural Fluid Study";
            case self::ASCITIC_FLUID_STUDY:
                return "Ascitic Fluid Study";
            case self::CSF_FLUID_STUDY:
                return "CSF Fluid Study";

            case self::ECG:
                return "ECG";
            case self::ECHOCARDIOGRAPH:
                return "Echocardiograph";

            case self::BRONCHOSCOPY:
                return "Bronchoscopy";

            case self::ENDOSCOPY:
                return "Endoscopy";
            case self::COLONOSCOPY:
                return "Colonoscopy";
            case self::ERCP:
                return "ERCP";

            case self::HAEMATOPATHOLOGY_OTHERS:
                //fall-through
            case self::BIOCHEMISTRY_OTHERS:
                //fall-through
            case self::RADIOLOGY_OTHERS:
                //fall-through
            case self::PATHOLOGY_OTHERS:
                //fall-through
            case self::CARDIOLOGY_OTHERS:
                //fall-through
            case self::RESPIRATORY_OTHERS:
                //fall-through
            case self::GASTROENTEROLOGY_OTHERS:
                //fall-through
            case self::OTHERS:
                return "Others";

            default:
                return null;
        }
    }

    /**
     * @Id
     * @GeneratedValue
     * @Column(type="integer")
     * @var int
     */
    private $id;

    /**
     * @Column(type="integer")
     * @var int
     */
    private $type;

    /**
     * @Column(type="date")
     * @var DateTime
     */
    private $investigationDate;

    /**
     * @Column(type="string")
     * @var string
     */
    private $value;

    /**
     * @Column(type="string", nullable=true)
     * @var string
     */
    private $attachmentName;

    /**
     * @ManyToOne(targetEntity="Patient", inversedBy="investigations")
     * @var \models\Patient
     */
    private $patient;

    /**
     * @Column(type="datetime")
     * @var DateTime
     */
    private $postedOn;

    /**
     * @ManyToOne(targetEntity="Doctor")
     * @var \models\Doctor
     */
    private $postedBy;

    /**
     * @param string $attachmentName
     */
    public function setAttachmentName($attachmentName) {
        $this->attachmentName = $attachmentName;
    }

    /**
     * @return string
     */
    public function getAttachmentName() {
        return $this->attachmentName;
    }

    /**
     * @param int $id
     */
    public function setId($id) {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @param DateTime $investigationDate
     */
    public function setInvestigationDate($investigationDate) {
        $this->investigationDate = $investigationDate;
    }

    /**
     * @param bool   $format
     * @param string $formatter
     *
     * @return DateTime or string
     */
    public function getInvestigationDate($format = false, $formatter = self::INVESTIGATION_DATE_DISPLAY_FORMAT) {
        return $format ? $this->investigationDate->format($formatter) : $this->investigationDate;
    }

    /**
     * @param string $value
     */
    public function setValue($value) {
        $this->value = $value;
    }

    /**
     * @return string
     */
    public function getValue() {
        return $this->value;
    }

    /**
     * @param \models\Patient $patient
     */
    public function setPatient($patient) {
        $this->patient = $patient;
    }

    /**
     * @return \models\Patient
     */
    public function getPatient() {
        return $this->patient;
    }

    /**
     * @param \models\Doctor $postedBy
     */
    public function setPostedBy($postedBy) {
        $this->postedBy = $postedBy;
    }

    /**
     * @return \models\Doctor
     */
    public function getPostedBy() {
        return $this->postedBy;
    }

    /**
     * @param DateTime $postedOn
     */
    public function setPostedOn($postedOn) {
        $this->postedOn = $postedOn;
    }

    /**
     * @return DateTime
     */
    public function getPostedOn() {
        return $this->postedOn;
    }

    /**
     * @param int $type
     */
    public function setType($type) {
        $this->type = $type;
    }

    /**
     * @return int
     */
    public function getType() {
        return $this->type;
    }
}
