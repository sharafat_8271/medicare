<?php

namespace models;

use DateTime;

/**
 * Author:  sharafat
 * Created: 1/14/13 8:45 PM
 *
 * @Entity
 */
class Findings {
    const BODY_BUILD = 1;
    const ANAEMIA = 2;
    const JAUNDICE = 3;
    const CYANOSIS = 4;
    const CLUBBING = 5;
    const KOILONYCHIA = 6;
    const LEUKONYCLIA = 7;
    const OEDEMA = 8;
    const LYMPH_NODE = 9;
    const THYROID = 10;
    const PULSE = 11;
    const BP = 12;
    const TEMP = 13;
    const RESP_RATE = 14;
    const SKIN = 15;
    const RESP_EXAM = 16;
    const CVS_EXAM = 17;
    const ABDOMEN_EXAM = 18;
    const CNS_AND_FUNDUS_EXAM = 19;
    const ECOG = 20;
    const OTHERS = 21;

    const GENERAL_EXAM = 22;

    public static function getGroupByType($type) {
        if ($type >= 1 && $type <= 15) {
            return self::GENERAL_EXAM;
        } else {
            return null;
        }
    }

    public static function getTypeByName($name) {
        switch ($name) {
            case "bodyBuild":
                return self::BODY_BUILD;
            case "anaemia":
                return self::ANAEMIA;
            case "jaundice":
                return self::JAUNDICE;
            case "cyanosis":
                return self::CYANOSIS;
            case "clubbing":
                return self::CLUBBING;
            case "koilonychia":
                return self::KOILONYCHIA;
            case "leukonyclia":
                return self::LEUKONYCLIA;
            case "oedema":
                return self::OEDEMA;
            case "lymphNode":
                return self::LYMPH_NODE;
            case "thyroid":
                return self::THYROID;
            case "pulse":
                return self::PULSE;
            case "bp":
                return self::BP;
            case "temp":
                return self::TEMP;
            case "respRate":
                return self::RESP_RATE;
            case "skin":
                return self::SKIN;
            case "respExam":
                return self::RESP_EXAM;
            case "cvsExam":
                return self::CVS_EXAM;
            case "abdomenExam":
                return self::ABDOMEN_EXAM;
            case "cnsAndFundusExam":
                return self::CNS_AND_FUNDUS_EXAM;
            case "ecog":
                return self::ECOG;
            case "others":
                return self::OTHERS;
            default:
                return null;
        }
    }

    public static function getUserVisibleNameOfType($type) {
        switch ($type) {
            case self::BODY_BUILD:
                return "Body-build";
            case self::ANAEMIA:
                return "Anaemia";
            case self::JAUNDICE:
                return "Jaundice";
            case self::CYANOSIS:
                return "Cyanosis";
            case self::CLUBBING:
                return "Clubbing";
            case self::KOILONYCHIA:
                return "Koilonychia";
            case self::LEUKONYCLIA:
                return "Leukonyclia";
            case self::OEDEMA:
                return "Oedema";
            case self::LYMPH_NODE:
                return "Lymph node";
            case self::THYROID:
                return "Thyroid";
            case self::PULSE:
                return "Pulse";
            case self::BP:
                return "BP";
            case self::TEMP:
                return "Temp";
            case self::RESP_RATE:
                return "Resp. rate";
            case self::SKIN:
                return "Skin";
            case self::RESP_EXAM:
                return "Resp Exam";
            case self::CVS_EXAM:
                return "CVS Exam";
            case self::ABDOMEN_EXAM:
                return "Abdomen Exam";
            case self::CNS_AND_FUNDUS_EXAM:
                return "CNS &amp; Fundus Exam";
            case self::ECOG:
                return "ECOG";
            case self::OTHERS:
                return "Others";
            case self::GENERAL_EXAM:
                return "General Exam";
            default:
                return null;
        }
    }

    /**
     * @Id
     * @GeneratedValue
     * @Column(type="integer")
     * @var int
     */
    private $id;

    /**
     * @Column(type="integer")
     * @var int
     */
    private $type;

    /**
     * @Column(type="date")
     * @var DateTime
     */
    private $findingsDate;

    /**
     * @Column(type="string")
     * @var string
     */
    private $details;

    /**
     * @ManyToOne(targetEntity="Patient", inversedBy="findings")
     * @var \models\Patient
     */
    private $patient;

    /**
     * @ManyToOne(targetEntity="Person")
     * @var \models\Person
     */
    private $postedBy;

    /**
     * @Column(type="datetime")
     * @var DateTime
     */
    private $postedOn;

    /**
     * @param int $id
     */
    public function setId($id) {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @param \models\Patient $patient
     */
    public function setPatient($patient) {
        $this->patient = $patient;
    }

    /**
     * @return \models\Patient
     */
    public function getPatient() {
        return $this->patient;
    }

    /**
     * @param \models\Person $postedBy
     */
    public function setPostedBy($postedBy) {
        $this->postedBy = $postedBy;
    }

    /**
     * @return \models\Person
     */
    public function getPostedBy() {
        return $this->postedBy;
    }

    /**
     * @param \DateTime $postedOn
     */
    public function setPostedOn($postedOn) {
        $this->postedOn = $postedOn;
    }

    /**
     * @param bool   $format
     * @param string $formatter
     *
     * @return \DateTime or string
     */
    public function getPostedOn($format = false, $formatter = Investigation::INVESTIGATION_DATE_DISPLAY_FORMAT) {
        return $format ? $this->postedOn->format($formatter) : $this->postedOn;
    }

    /**
     * @param int $type
     */
    public function setType($type) {
        $this->type = $type;
    }

    /**
     * @return int
     */
    public function getType() {
        return $this->type;
    }

    /**
     * @param string $value
     */
    public function setDetails($value) {
        $this->details = $value;
    }

    /**
     * @return string
     */
    public function getDetails() {
        return $this->details;
    }

    /**
     * @param \DateTime $findingsDate
     */
    public function setFindingsDate($findingsDate) {
        $this->findingsDate = $findingsDate;
    }

    /**
     * @param bool   $format
     * @param string $formatter
     *
     * @return \DateTime or string
     */
    public function getFindingsDate($format = false, $formatter = Investigation::INVESTIGATION_DATE_DISPLAY_FORMAT) {
        return $format ? $this->findingsDate->format($formatter) : $this->findingsDate;
    }
}
