<?php

namespace models;

use ReflectionClass;

/**
 * Author:  sharafat
 * Created: 4/22/13 10:00 PM
 *
 * @Entity
 */
class Provider {
    /**
     * @Id
     * @GeneratedValue
     * @Column(type="integer")
     * @var int
     */
    protected $id;

    /**
     * @Column(type="string")
     * @var string
     */
    protected $name;

    /**
     * @Column(type="string")
     * @var string
     */
    protected $departmentName;

    /**
     * @Column(type="string")
     * @var string
     */
    protected $address;

    /**
     * @Column(type="string")
     * @var string
     */
    protected $contactNo;

    /**
     * @Column(type="string")
     * @var string
     */
    protected $email;

    /**
     * @Column(type="string")
     * @var string
     */
    protected $logoFileName;

    /**
     * @param string $address
     */
    public function setAddress($address) {
        $this->address = $address;
    }

    /**
     * @return string
     */
    public function getAddress() {
        return $this->address;
    }

    /**
     * @param string $contactNo
     */
    public function setContactNo($contactNo) {
        $this->contactNo = $contactNo;
    }

    /**
     * @return string
     */
    public function getContactNo() {
        return $this->contactNo;
    }

    /**
     * @param string $departmentName
     */
    public function setDepartmentName($departmentName) {
        $this->departmentName = $departmentName;
    }

    /**
     * @return string
     */
    public function getDepartmentName() {
        return $this->departmentName;
    }

    /**
     * @param string $email
     */
    public function setEmail($email) {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getEmail() {
        return $this->email;
    }

    /**
     * @param int $id
     */
    public function setId($id) {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @param string $logoFileName
     */
    public function setLogoFileName($logoFileName) {
        $this->logoFileName = $logoFileName;
    }

    /**
     * @return string
     */
    public function getLogoFileName() {
        return $this->logoFileName;
    }

    /**
     * @param string $name
     */
    public function setName($name) {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    public function duplicate() {
        $provider = new Provider();

        $provider->id = $this->id;
        $provider->name = $this->name;
        $provider->departmentName = $this->departmentName;
        $provider->address = $this->address;
        $provider->email = $this->email;
        $provider->logoFileName = $this->logoFileName;
        $provider->contactNo = $this->contactNo;

        return $provider;
    }
}
