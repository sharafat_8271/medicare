<?php

namespace models;

/**
 * Author:  sharafat
 * Created: 12/07/12 08:52 AM
 *
 * @Entity
 */
class Doctor extends Person {
    /**
     * @Column(type="string")
     * @var string
     */
    private $degree;

    /**
     * @param string $degree
     */
    public function setDegree($degree) {
        $this->degree = $degree;
    }

    /**
     * @return string
     */
    public function getDegree() {
        return $this->degree;
    }
}
