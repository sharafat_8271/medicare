<?php

namespace models;

use DateTime;

/**
 * Author:  sharafat
 * Created: 1/6/13 3:52 PM
 *
 * @Entity
 */
class Followup {
    /**
     * @Id
     * @GeneratedValue
     * @Column(type="integer")
     * @var int
     */
    private $id;

    /**
     * @ManyToOne(targetEntity="Patient", inversedBy="followupHistory")
     * @var \models\Patient
     */
    private $patient;

    /**
     * @Column(type="string")
     * @var string
     */
    private $nextPlan;

    /**
     * @Column(type="date")
     * @var DateTime
     */
    private $followUpDate;

    /**
     * @Column(type="string", nullable=true)
     * @var string
     */
    private $closingRemarks;

    /**
     * @Column(type="date", nullable=true)
     * @var DateTime
     */
    private $closingDate;

    /**
     * @param \DateTime $closingDate
     */
    public function setClosingDate($closingDate) {
        $this->closingDate = $closingDate;
    }

    /**
     * @return \DateTime
     */
    public function getClosingDate($format = false, $formatter = Investigation::INVESTIGATION_DATE_DISPLAY_FORMAT) {
        return $format && $this->closingDate != "" ? $this->closingDate->format($formatter) : $this->closingDate;
    }

    /**
     * @param string $closingRemarks
     */
    public function setClosingRemarks($closingRemarks) {
        $this->closingRemarks = $closingRemarks;
    }

    /**
     * @return string
     */
    public function getClosingRemarks() {
        return $this->closingRemarks;
    }

    /**
     * @param \DateTime $followUpDate
     */
    public function setFollowUpDate($followUpDate) {
        $this->followUpDate = $followUpDate;
    }

    /**
     * @return \DateTime
     */
    public function getFollowUpDate($format = false, $formatter = Investigation::INVESTIGATION_DATE_DISPLAY_FORMAT) {
        return $format ? $this->followUpDate->format($formatter) : $this->followUpDate;
    }

    /**
     * @param int $id
     */
    public function setId($id) {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @param string $nextPlan
     */
    public function setNextPlan($nextPlan) {
        $this->nextPlan = $nextPlan;
    }

    /**
     * @return string
     */
    public function getNextPlan() {
        return $this->nextPlan;
    }

    /**
     * @param \models\Patient $patient
     */
    public function setPatient($patient) {
        $this->patient = $patient;
    }

    /**
     * @return \models\Patient
     */
    public function getPatient() {
        return $this->patient;
    }
}
