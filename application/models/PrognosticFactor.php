<?php

namespace models;

use ReflectionClass;

/**
 * Author:  sharafat
 * Created: 1/18/13 2:59 PM
 *
 * @Entity
 */

class PrognosticFactor {
    const CLL_SEX = 1;
    const CLL_CLINICAL_STAGE = 2;
    const CLL_INITIAL_LYMPHOCYTOSIS = 3;
    const CLL_PROLYMPHOCYTE = 4;
    const CLL_TREPHINE_BIOPSY_PATTERN = 5;
    const CLL_S_LDH = 6;
    const CLL_S_B2_MICROGLOBULIN = 7;
    const CLL_SOLUBLE_CD23 = 8;
    const CLL_CD38_EXPRESSION = 9;
    const CLL_ZAP70_EXPRESSION = 10;
    const CLL_P53_MUTATION = 11;
    const CLL_CYTOGENETIC = 12;
    const CLL_LYMPHOCYTE_DOUBLING_TIME = 13;
    const CLL_RESPONSE_TO_THERAPY = 14;

    const NHL_AGE = 15;
    const NHL_CLINICAL_STAGE = 16;
    const NHL_NO_OF_EXTRANODAL_SITES = 17;
    const NHL_PERFORMANCE_STATUS = 18;
    const NHL_S_LDH = 19;
    const NHL_B_SYMPTOM = 20;
    const NHL_BULKY_DISEASE = 21;
    const NHL_B2_MICROGLOBULIN = 22;
    const NHL_KI67 = 23;
    const NHL_TCELL_PHENOTYPE = 24;
    const NHL_LOWGRADE_TO_HIGH_GRADE_TRANSFORMATION = 25;
    const NHL_HISTOLOGIC_GRADE = 26;
    const NHL_HB = 27;
    const NHL_IPI_SCORE = 28;
    const NHL_FLIPI = 29;

    const HD_SEX = 30;
    const HD_S_ALBUMIN = 31;
    const HD_HB = 32;
    const HD_CLINICAL_STAGE = 33;
    const HD_WBC_COUNT = 34;
    const HD_LYMPHOPENIA = 35;
    const HD_B_SYMPTOM = 36;
    const HD_BULKY_DISEASE = 37;
    const HD_IPI_SCORE = 38;

    const MM_AGE = 39;
    const MM_PERFORMANCE_STATUS = 40;
    const MM_HB = 41;
    const MM_PARAPROTEIN_LEVEL = 42;
    const MM_S_CALCIUM = 43;
    const MM_S_ALBUMIN = 44;
    const MM_S_CREATININE = 45;
    const MM_B2_MICROGLOBULIN = 46;
    const MM_S_LDH = 47;
    const MM_CRP = 48;
    const MM_BM_PLASMACELLS = 49;
    const MM_PERIPHERAL_PLASMACELLS = 50;
    const MM_CYTOGENETICS = 51;
    const MM_ISS = 52;

    const PM_AGE = 53;
    const PM_HB = 54;
    const PM_WBC_COUNT = 55;
    const PM_NO_OF_BLAST_IN_PBF = 56;
    const PM_B_SYMPTOM = 57;
    const PM_CD34_COUNT = 58;
    const PM_JAK2_V617F_MUTATION = 59;
    const PM_CYTOGENETICS = 60;
    const PM_LILLE_PROGNOSTIC_SCORE = 61;

    const CML_BCR_ABL = 62;
    const CML_SOKAL_SCORE = 63;

    const WM_AGE = 64;
    const WM_SEX = 65;
    const WM_HB = 66;
    const WM_B2_MICROGLOBULIN = 67;
    const WM_IGM = 68;
    const WM_S_ALBUMIN = 69;
    const WM_B_SYMPTOM = 70;
    const WM_WBC_COUNT = 71;

    const ALL_AGE = 72;
    const ALL_WBC_COUNT = 73;
    const ALL_IMMUNOPHENOTYPE = 74;
    const ALL_CYTOGENETICS = 75;
    const ALL_RESPONSE_TO_THERAPY = 76;

    const AML_CLINICAL_AGE = 77;
    const AML_CLINICAL_ECOG_PERFORMANCE = 78;
    const AML_CLINICAL_LEUKEMIA = 79;
    const AML_CLINICAL_INFECTION = 80;
    const AML_CLINICAL_PRIOR_CHEMOTHERAPY = 81;
    const AML_CLINICAL_LEUKOCYTOSIS = 82;
    const AML_CLINICAL_S_LDH = 83;
    const AML_CLINICAL_EXTRAMEDULLARY_DISEASE = 84;
    const AML_CLINICAL_CNS_DISEASE = 85;
    const AML_CLINICAL_CYTOREDUCTION = 86;
    const AML_MORPHOLOGY_AUER_RODS = 87;
    const AML_MORPHOLOGY_EOSINOPHILS = 88;
    const AML_MORPHOLOGY_MEGALOBLASTIC_ERYTHROIDS = 89;
    const AML_MORPHOLOGY_DYSPLASTIC_MEGAKARYOCYTES = 90;
    const AML_MORPHOLOGY_FAB_TYPE = 91;
    const AML_SURFACE_MARKERS_MYELOID = 92;
    const AML_SURFACE_MARKERS_HLA_DR = 93;
    const AML_SURFACE_MARKERS_TDT = 94;
    const AML_SURFACE_MARKERS_LYMPHOID = 95;
    const AML_SURFACE_MARKERS_MDR_1 = 96;
    const AML_CYTOGENETICS = 97;
    const AML_MOLECULAR_MARKERS_FMS_RELATED_TYROSINE_KINASE_3_MUTATION = 98;
    const AML_MOLECULAR_MARKERS_ECOTROPIC_VIRAL_INTEGRATION_SITE_1_EXPRESSION = 99;
    const AML_MOLECULAR_MARKERS_MIXED_LINEAGE_LEUKEMIA_PARTIAL_TANDEM_DUPLICATION = 100;
    const AML_MOLECULAR_MARKERS_NUCLEOPHOSMIN_MUTATION = 101;
    const AML_MOLECULAR_MARKERS_CCAAT = 102;
    const AML_MOLECULAR_MARKERS_BRAIN_AND_ACUTE_LEUKEMIA_CYTOPLASMIC_GENE_EXPRESSION = 103;
    const AML_MOLECULAR_MARKERS_VASCULAR_ENDOTHELIAL_GROWTH_FACTOR_EXPRESSION = 104;

    public static function getFactorsByCategory($category) {
        switch ($category) {
            case Patient::ALL:
                return self::getPrognosticFactors("ALL_");
            case Patient::AML:
                return self::getPrognosticFactors("AML_");
            case Patient::APLASTIC_ANAEMIC:
                return self::getPrognosticFactors("AA_");
            case Patient::CML:
                return self::getPrognosticFactors("CML_");
            case Patient::CLL:
                return self::getPrognosticFactors("CLL_");
            case Patient::HD:
                return self::getPrognosticFactors("HD_");
            case Patient::MDS:
                return self::getPrognosticFactors("MDS_");
            case Patient::MULTIPLE_MYELOMA:
                return self::getPrognosticFactors("MM_");
            case Patient::MYELOFIBROSIN:
                return self::getPrognosticFactors("PM_");
            case Patient::NHL:
                return self::getPrognosticFactors("NHL_");
            case Patient::PRV:
                return self::getPrognosticFactors("PRV_");
            case Patient::WM:
                return self::getPrognosticFactors("WM_");
            default:
                return null;
        }
    }

    private static function getPrognosticFactors($prefix) {
        $thisClass = new ReflectionClass(__CLASS__);
        $prognosticFactorConstants = $thisClass->getConstants();

        $prognosticFactors = array();

        foreach ($prognosticFactorConstants as $factorName => $factorValue) {
            if (substr($factorName, 0, strlen($prefix)) === $prefix) {
                $prognosticFactors[] = $factorValue;
            }
        }

        sort($prognosticFactors);
        return $prognosticFactors;
    }

    public static function getUserVisibleNameOfFactor($prognosticFactor) {
        switch ($prognosticFactor) {
            case self::CLL_SEX:
            case self::HD_SEX:
            case self::WM_SEX:
                return "Sex";
            case self::CLL_CLINICAL_STAGE:
            case self::NHL_CLINICAL_STAGE:
            case self::HD_CLINICAL_STAGE:
                return "Clinical Stage";
            case self::CLL_INITIAL_LYMPHOCYTOSIS:
                return "Initial Lymphocytosis";
            case self::CLL_PROLYMPHOCYTE:
                return "Prolymphocyte";
            case self::CLL_TREPHINE_BIOPSY_PATTERN:
                return "Trephine Biopsy Pattern";
            case self::CLL_S_LDH:
            case self::NHL_S_LDH:
            case self::MM_S_LDH:
            case self::AML_CLINICAL_S_LDH:
                return "S. LDH";
            case self::CLL_S_B2_MICROGLOBULIN:
                return "S. &beta;<sub>2</sub>-Microglobulin";
            case self::CLL_SOLUBLE_CD23:
                return "Soluble CD23";
            case self::CLL_CD38_EXPRESSION:
                return "CD38 Expression";
            case self::CLL_ZAP70_EXPRESSION:
                return "ZAP-70 Expression";
            case self::CLL_P53_MUTATION:
                return "P53 Mutation";
            case self::CLL_CYTOGENETIC:
                return "Cytogenetic";
            case self::CLL_LYMPHOCYTE_DOUBLING_TIME:
                return "Lymphocyte Doubling Time";
            case self::CLL_RESPONSE_TO_THERAPY:
            case self::ALL_RESPONSE_TO_THERAPY:
                return "Response to Therapy";
            case self::NHL_AGE:
            case self::MM_AGE:
            case self::PM_AGE:
            case self::WM_AGE:
            case self::ALL_AGE:
            case self::AML_CLINICAL_AGE:
                return "Age";
            case self::NHL_NO_OF_EXTRANODAL_SITES:
                return "No. of Extranodal Sites";
            case self::NHL_PERFORMANCE_STATUS:
                return "Performance Status";
            case self::NHL_B_SYMPTOM:
            case self::HD_B_SYMPTOM:
            case self::PM_B_SYMPTOM:
            case self::WM_B_SYMPTOM:
                return "&quot;B&quot; Symptom";
            case self::NHL_BULKY_DISEASE:
            case self::HD_BULKY_DISEASE:
                return "Bulky Disease";
            case self::NHL_B2_MICROGLOBULIN:
            case self::MM_B2_MICROGLOBULIN:
            case self::WM_B2_MICROGLOBULIN:
                return "&beta;<sub>2</sub>-Microglobulin";
            case self::NHL_KI67:
                return "Ki-67";
            case self::NHL_TCELL_PHENOTYPE:
                return "T-Cell Phenotype";
            case self::NHL_LOWGRADE_TO_HIGH_GRADE_TRANSFORMATION:
                return "Low Grade to High Grade Transformation";
            case self::NHL_HISTOLOGIC_GRADE:
                return "Histologic Grade";
            case self::NHL_HB:
            case self::HD_HB:
            case self::MM_HB:
            case self::PM_HB:
            case self::WM_HB:
                return "Hb%";
            case self::NHL_IPI_SCORE:
            case self::HD_IPI_SCORE:
                return "IPI Score";
            case self::NHL_FLIPI:
                return "FLIPI";
            case self::HD_S_ALBUMIN:
            case self::MM_S_ALBUMIN:
            case self::WM_S_ALBUMIN:
                return "S. Albumin";
            case self::HD_WBC_COUNT:
            case self::PM_WBC_COUNT:
            case self::WM_WBC_COUNT:
            case self::ALL_WBC_COUNT:
                return "WBC Count";
            case self::HD_LYMPHOPENIA:
                return "Lymphopenia";
            case self::MM_PERFORMANCE_STATUS:
                return "Performance Status";
            case self::MM_PARAPROTEIN_LEVEL:
                return "Paraprotein Level";
            case self::MM_S_CALCIUM:
                return "S. Calcium";
            case self::MM_S_CREATININE:
                return "S. Creatinine";
            case self::MM_CRP:
                return "CRP";
            case self::MM_BM_PLASMACELLS:
                return "% BM Plasmacells";
            case self::MM_PERIPHERAL_PLASMACELLS:
                return "% Peripheral Plasmacells";
            case self::MM_CYTOGENETICS:
            case self::PM_CYTOGENETICS:
            case self::ALL_CYTOGENETICS:
            case self::AML_CYTOGENETICS:
                return "Cytogenetics";
            case self::MM_ISS:
                return "ISS";
            case self::PM_NO_OF_BLAST_IN_PBF:
                return "No. of Blast in PBF";
            case self::PM_CD34_COUNT:
                return "CD 34&apos; Count";
            case self::PM_JAK2_V617F_MUTATION:
                return "JAK2-V617F Mutation";
            case self::PM_LILLE_PROGNOSTIC_SCORE:
                return "Lille Prognostic Score";
            case self::CML_BCR_ABL:
                return "BCR-ABL";
            case self::CML_SOKAL_SCORE:
                return "Sokal Score";
            case self::WM_IGM:
                return "Ig M Level";
            case self::ALL_IMMUNOPHENOTYPE:
                return "Immunophenotype";
            case self::AML_CLINICAL_ECOG_PERFORMANCE:
                return "ECOG Performance";
            case self::AML_CLINICAL_LEUKEMIA:
                return "Leukemia";
            case self::AML_CLINICAL_INFECTION:
                return "Infection";
            case self::AML_CLINICAL_PRIOR_CHEMOTHERAPY:
                return "Prior Chemotherapy";
            case self::AML_CLINICAL_LEUKOCYTOSIS:
                return "Leukocytosis";
            case self::AML_CLINICAL_EXTRAMEDULLARY_DISEASE:
                return "Extramedullary Disease";
            case self::AML_CLINICAL_CNS_DISEASE:
                return "CNS Disease";
            case self::AML_CLINICAL_CYTOREDUCTION:
                return "Cytoreduction";
            case self::AML_MORPHOLOGY_AUER_RODS:
                return "Auer Rods";
            case self::AML_MORPHOLOGY_EOSINOPHILS:
                return "Eosinophils";
            case self::AML_MORPHOLOGY_MEGALOBLASTIC_ERYTHROIDS:
                return "Megaloblastic Erythroids";
            case self::AML_MORPHOLOGY_DYSPLASTIC_MEGAKARYOCYTES:
                return "Dysplastic Megakaryocytes";
            case self::AML_MORPHOLOGY_FAB_TYPE:
                return "FAB Type";
            case self::AML_SURFACE_MARKERS_MYELOID:
                return "Myeloid";
            case self::AML_SURFACE_MARKERS_HLA_DR:
                return "HLA-DR";
            case self::AML_SURFACE_MARKERS_TDT:
                return "TdT";
            case self::AML_SURFACE_MARKERS_LYMPHOID:
                return "Lymphoid";
            case self::AML_SURFACE_MARKERS_MDR_1:
                return "MDR-1";
            case self::AML_MOLECULAR_MARKERS_FMS_RELATED_TYROSINE_KINASE_3_MUTATION:
                return "Fms-Related Tyrosine Kinase-3 Mutation";
            case self::AML_MOLECULAR_MARKERS_ECOTROPIC_VIRAL_INTEGRATION_SITE_1_EXPRESSION:
                return "Ecotropic Viral Integration Site 1 Expression";
            case self::AML_MOLECULAR_MARKERS_MIXED_LINEAGE_LEUKEMIA_PARTIAL_TANDEM_DUPLICATION:
                return "Mixed-Lineage Leukemia Partial Tandem Duplication";
            case self::AML_MOLECULAR_MARKERS_NUCLEOPHOSMIN_MUTATION:
                return "Nucleophosmin Mutation";
            case self::AML_MOLECULAR_MARKERS_CCAAT:
                return "Ccaat/Enhancer-Binding Protein-A Mutation";
            case self::AML_MOLECULAR_MARKERS_BRAIN_AND_ACUTE_LEUKEMIA_CYTOPLASMIC_GENE_EXPRESSION:
                return "Brain And Acute Leukemia Cytoplasmic Gene Expression";
            case self::AML_MOLECULAR_MARKERS_VASCULAR_ENDOTHELIAL_GROWTH_FACTOR_EXPRESSION:
                return "Vascular Endothelial Growth Factor Expression";
            default:
                return null;
        }
    }


    /**
     * @Id
     * @GeneratedValue
     * @Column(type="integer")
     * @var int
     */
    private $id;

    /**
     * @Column(type="integer")
     * @var int
     */
    private $factor;

    /**
     * @Column(type="string", nullable=true)
     * @var string
     */
    private $status;

    /**
     * @Column(type="boolean", nullable=true)
     * @var boolean
     */
    private $favorable;

    /**
     * @ManyToOne(targetEntity="Patient", inversedBy="investigations")
     * @var \models\Patient
     */
    private $patient;

    /**
     * @param boolean $favorable
     */
    public function setFavorable($favorable) {
        $this->favorable = $favorable;
    }

    /**
     * @return boolean
     */
    public function isFavorable() {
        return $this->favorable;
    }

    /**
     * @param int $id
     */
    public function setId($id) {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @param \models\Patient $patient
     */
    public function setPatient($patient) {
        $this->patient = $patient;
    }

    /**
     * @return \models\Patient
     */
    public function getPatient() {
        return $this->patient;
    }

    /**
     * @param string $status
     */
    public function setStatus($status) {
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getStatus() {
        return $this->status;
    }

    /**
     * @param int $factor
     */
    public function setFactor($factor) {
        $this->factor = $factor;
    }

    /**
     * @return int
     */
    public function getFactor() {
        return $this->factor;
    }
}
