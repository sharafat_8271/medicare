<?php

namespace models;

use DateTime;

/**
 * Author:  sharafat
 * Created: 12/07/12 08:52 AM
 *
 * @Entity
 * @InheritanceType    ("SINGLE_TABLE")
 * @DiscriminatorColumn(name="type", type="string")
 * @DiscriminatorMap   ({"person" = "Person", "doctor" = "Doctor", "patient" = "Patient", "admin" = "Admin"})
 */
class Person {
    //religion constants
    const MUSLIM = 1;
    const HINDU = 2;
    const BUDDIST = 3;
    const CHRISTIAN = 4;
    const OTHER = 5;

    //sex constants
    const MALE = 6;
    const FEMALE = 7;

    //account status constants
    const ACTIVE = 8;
    const INACTIVE = 9;

    //other constants
    const MAX_LENGTH_OF_ID = 5;
    const ID_PREFIX = "HM-";
    const DOB_DISPLAY_FORMAT = "d F Y";
    const DOB_INPUT_FORMAT = "Y-m-d";
    const REGISTERED_TIME_DISPLAY_FORMAT = "l, d F Y, h:i:sa T";

    /**
     * @static
     *
     * @param $constant
     *
     * @return string
     */
    public static function msgKey($constant) {
        switch ($constant) {
            case self::MUSLIM:
                return "muslim";
            case self::HINDU:
                return "hindu";
            case self::BUDDIST:
                return "buddist";
            case self::CHRISTIAN:
                return "christian";
            case self::OTHER:
                return "other";
            case self::MALE:
                return "male";
            case self::FEMALE:
                return "female";
            case self::ACTIVE:
                return "active";
            case self::INACTIVE:
                return "inactive";
            default:
                return null;
        }
    }

    /**
     * @Id
     * @GeneratedValue
     * @Column(type="integer")
     * @var int
     */
    protected $id;

    /**
     * @Column(type="string")
     * @var string
     */
    protected $name;

    /**
     * @Column(type="integer")
     * @var int
     */
    protected $sex;

    /**
     * @Column(type="date")
     * @var DateTime
     */
    protected $dob;

    /**
     * @Column(type="integer", nullable=true)
     * @var int
     */
    protected $religion;

    /**
     * @Column(type="string", nullable=true)
     * @var string
     */
    protected $address;

    /**
     * @Column(type="string", nullable=true)
     * @var string
     */
    protected $mobile;

    /**
     * @Column(type="string", nullable=true)
     * @var string
     */
    protected $email;

    /**
     * @Column(type="string", nullable=true)
     * @var string
     */
    protected $password;

    /**
     * @Column(type="integer")
     * @var int
     */
    protected $accountStatus = self::ACTIVE;

    /**
     * @Column(type="datetime")
     * @var DateTime
     */
    protected $registrationDateTime;

    /**
     * @ManyToOne(targetEntity="Person")
     * @var \models\Person
     */
    private $registeredBy;

    /**
     * @ManyToOne(targetEntity="Provider")
     * @var \models\Provider
     */
    private $provider;

    /**
     * @Column(type="boolean")
     * @var boolean
     */
    protected $admin = false;

    /**
     * @param string $address
     */
    public function setAddress($address) {
        $this->address = $address;
    }

    /**
     * @return string
     */
    public function getAddress() {
        return $this->address;
    }

    /**
     * @param DateTime $dob
     */
    public function setDob(DateTime $dob) {
        $this->dob = $dob;
    }

    /**
     * @param bool      $format
     * @param string    $formatter
     *
     * @return DateTime if $format == false, string otherwise
     */
    public function getDob($format = false, $formatter = self::DOB_DISPLAY_FORMAT) {
        return !is_null($this->dob) && $format ? $this->dob->format($formatter) : $this->dob;
    }

    /**
     * @return array
     */
    public function getAge() {
        $interval = $this->dob->diff(new DateTime(), true);
        return explode(":", $interval->format("%y:%m:%d"));
    }

    public function getYearsInAge() {
        $age = $this->getAge();
        return $age[0];
    }

    public function getMonthsInAge() {
        $age = $this->getAge();
        return $age[1];
    }

    public function getDaysInAge() {
        $age = $this->getAge();
        return $age[2];
    }

    /**
     * @param string $email
     */
    public function setEmail($email) {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getEmail() {
        return $this->email;
    }

    /**
     * @param int $id
     */
    public function setId($id) {
        $this->id = $id;
    }

    /**
     * @param boolean $formatted
     *
     * @return int if $formatted == false, string otherwise
     */
    public function getId($formatted = false) {
        return $formatted ? self::ID_PREFIX . $this->getZeroPadded($this->id) : $this->id;
    }

    private function getZeroPadded($id) {
        return str_pad($id, self::MAX_LENGTH_OF_ID, "0", STR_PAD_LEFT);
    }

    /**
     * @param string $mobile
     */
    public function setMobile($mobile) {
        $this->mobile = $mobile;
    }

    /**
     * @return string
     */
    public function getMobile() {
        return $this->mobile;
    }

    /**
     * @param string $name
     */
    public function setName($name) {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * @param string $password
     */
    public function setPassword($password) {
        $this->password = $password;
    }

    /**
     * @return string
     */
    public function getPassword() {
        return $this->password;
    }

    /**
     * @param DateTime $registrationDateTime
     */
    public function setRegistrationDateTime($registrationDateTime) {
        $this->registrationDateTime = $registrationDateTime;
    }

    /**
     * @param bool      $format
     * @param string    $formatter
     *
     * @return DateTime if $formatter == null, string otherwise
     */
    public function getRegistrationDateTime($format = false, $formatter = self::REGISTERED_TIME_DISPLAY_FORMAT) {
        return $format ? $this->registrationDateTime->format($formatter) : $this->registrationDateTime;
    }

    /**
     * @param int $religion
     */
    public function setReligion($religion) {
        $this->religion = $religion;
    }

    /**
     * @return int
     */
    public function getReligion() {
        return $this->religion;
    }

    /**
     * @param int $sex
     */
    public function setSex($sex) {
        $this->sex = $sex;
    }

    /**
     * @return int
     */
    public function getSex() {
        return $this->sex;
    }

    /**
     * @param int $accountStatus
     */
    public function setAccountStatus($accountStatus) {
        $this->accountStatus = $accountStatus;
    }

    /**
     * @return int
     */
    public function getAccountStatus() {
        return $this->accountStatus;
    }

    /**
     * @param boolean $admin
     */
    public function setAdmin($admin) {
        $this->admin = $admin;
    }

    /**
     * @return boolean
     */
    public function isAdmin() {
        return $this->admin;
    }

    /**
     * @param \models\Person $registeredBy
     */
    public function setRegisteredBy(Person $registeredBy) {
        $this->registeredBy = $registeredBy;
    }

    /**
     * @return \models\Person
     */
    public function getRegisteredBy() {
        return $this->registeredBy;
    }

    /**
     * @param \models\Provider $provider
     */
    public function setProvider(Provider $provider) {
        $this->provider = $provider;
    }

    /**
     * @return \models\Provider
     */
    public function getProvider() {
        return $this->provider;
    }
}
