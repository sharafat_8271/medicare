<?php

namespace models;

use DateTime;

/**
 * Author:  sharafat
 * Created: 1/6/13 3:52 PM
 *
 * @Entity
 */
class AdmissionDischarge {
    const ADMISSION = 1;
    const DISCHARGE = 2;

    /**
     * @Id
     * @GeneratedValue
     * @Column(type="integer")
     * @var int
     */
    private $id;

    /**
     * @ManyToOne(targetEntity="Patient", inversedBy="admissionDischargeHistory")
     * @var \models\Patient
     */
    private $patient;

    /**
     * @Column(type="string")
     * @var string
     */
    private $discussion;

    /**
     * @Column(type="integer")
     * @var int
     */
    private $eventType;

    /**
     * @Column(type="date")
     * @var DateTime
     */
    private $eventDate;

    /**
     * @param string $discussion
     */
    public function setDiscussion($discussion) {
        $this->discussion = $discussion;
    }

    /**
     * @return string
     */
    public function getDiscussion() {
        return $this->discussion;
    }

    /**
     * @param \DateTime $eventDate
     */
    public function setEventDate($eventDate) {
        $this->eventDate = $eventDate;
    }

    /**
     * @return \DateTime or string
     */
    public function getEventDate($format = false, $formatter = Investigation::INVESTIGATION_DATE_DISPLAY_FORMAT) {
        return $format ? $this->eventDate->format($formatter) : $this->eventDate;
    }

    /**
     * @param int $eventType
     */
    public function setEventType($eventType) {
        $this->eventType = $eventType;
    }

    /**
     * @return int
     */
    public function getEventType() {
        return $this->eventType;
    }

    /**
     * @param int $id
     */
    public function setId($id) {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @param \models\Patient $patient
     */
    public function setPatient($patient) {
        $this->patient = $patient;
    }

    /**
     * @return \models\Patient
     */
    public function getPatient() {
        return $this->patient;
    }
}
