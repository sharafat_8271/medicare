<?php

namespace models;

/**
 * Author:  sharafat
 * Created: 12/07/12 08:52 AM
 *
 * @Entity
 * @Table(indexes={@index(name="category_idx", columns={"category"})})
 */
class Patient extends Person {
    //patient type constants
    const INDOOR = 1;
    const OUTDOOR = 2;

    //category constants
    const ALL = 1;
    const AML = 2;
    const APLASTIC_ANAEMIC = 3;
    const CML= 4;
    const CLL = 5;
    const HD = 6;
    const MDS= 7;
    const MULTIPLE_MYELOMA = 8;
    const MYELOFIBROSIN = 9;
    const NHL = 10;
    const PRV = 11;
    const WM = 12;

    //other constants
    const DECIMAL_PRECISION_DIGITS = 2;

    public static function enumerateCategories() {
        return array(
            self::ALL => "ALL",
            self::AML => "AML",
            self::APLASTIC_ANAEMIC => "Aplastic Anaemic",
            self::CML => "CML",
            self::CLL => "CLL",
            self::HD => "HD",
            self::MDS => "MDS",
            self::MULTIPLE_MYELOMA => "Multiple Myeloma",
            self::MYELOFIBROSIN => "Myelofibrosin",
            self::NHL => "NHL",
            self::PRV => "PRV",
            self::WM => "WM"
        );
    }

    public static function getCategoryName($categoryId) {
        if ($categoryId == 0) {
            return "";
        } else {
            $categories = self::enumerateCategories();
            return $categories[$categoryId];
        }
    }

    /**
     * @Column(type="float", nullable=true)
     * @var float
     */
    private $heightInCentimeters;

    /**
     * @Column(type="float", nullable=true)
     * @var float
     */
    private $weightInKilograms;

    /**
     * @Column(type="string", nullable=true)
     * @var string
     */
    private $currentStatus;

    /**
     * @Column(type="string", nullable=true)
     * @var string
     */
    private $diagnosis;

    /**
     * @Column(type="boolean")
     * @var bool
     */
    private $indoor = false;

    /**
     * @Column(type="integer", nullable=true)
     * @var int
     */
    private $category;

    /**
     * @OneToOne(targetEntity="PresentingFeatures", cascade={"ALL"})
     * @var \models\PresentingFeatures
     */
    protected $presentingFeatures;

    /**
     * @OneToMany(targetEntity="PrognosticFactor", mappedBy="patient", cascade={"ALL"}, orphanRemoval=true)
     * @var array
     **/
    private $prognosticFactors;

    /**
     * @OneToMany(targetEntity="Investigation", mappedBy="patient")
     * @var array
     **/
    private $investigations;

    /**
     * @OneToMany(targetEntity="Findings", mappedBy="patient")
     * @var array
     **/
    private $findings;

    /**
     * @OneToMany(targetEntity="Protocol", mappedBy="patient")
     * @var array
     **/
    private $protocols;

    /**
     * @OneToMany(targetEntity="AdmissionDischarge", mappedBy="patient")
     * @var array
     **/
    private $admissionDischargeHistory;

    /**
     * @OneToMany(targetEntity="Followup", mappedBy="patient")
     * @var array
     **/
    private $followupHistory;

    /**
     * @OneToMany(targetEntity="Summary", mappedBy="patient")
     * @var array
     **/
    private $summaryHistory;

    /**
     * @OneToMany(targetEntity="Discussion", mappedBy="patient")
     * @var array
     **/
    private $discussionHistory;

    /**
     * @OneToMany(targetEntity="Advice", mappedBy="patient")
     * @var array
     **/
    private $adviceHistory;

    /**
     * @OneToMany(targetEntity="Drug", mappedBy="patient")
     * @var array
     **/
    private $drugHistory;

    /**
     * @param string $diagnosis
     */
    public function setDiagnosis($diagnosis) {
        $this->diagnosis = $diagnosis;
    }

    /**
     * @return string
     */
    public function getDiagnosis() {
        return $this->diagnosis;
    }

    /**
     * @param int $category
     */
    public function setCategory($category) {
        $this->category = $category;
    }

    /**
     * @return int
     */
    public function getCategory() {
        return $this->category;
    }

    /**
     * @param string $status
     */
    public function setCurrentStatus($status) {
        $this->currentStatus = $status;
    }

    /**
     * @return string
     */
    public function getCurrentStatus() {
        return $this->currentStatus;
    }

    /**
     * @param float $heightInCentimeters
     */
    public function setHeightInCentimeters($heightInCentimeters) {
        $this->heightInCentimeters = $heightInCentimeters;
    }

    /**
     * @return float
     */
    public function getHeightInCentimeters() {
        return $this->heightInCentimeters != "" ? $this->roundUp($this->heightInCentimeters) : "";
    }

    /**
     * @param float $heightInInches
     */
    public function setHeightInInches($heightInInches) {
        $this->heightInCentimeters = $heightInInches * 2.54;
    }

    /**
     * @return float
     */
    public function getHeightInInches() {
        return $this->heightInCentimeters != "" ? $this->roundUp($this->heightInCentimeters / 2.54) : "";
    }

    /**
     * @return array with indices 'feet' and 'inches'
     */
    public function getHeightInFeetInches() {
        $inches = $this->getHeightInInches();
        return $inches != "" ? array('feet' => (int) ($inches / 12), 'inches' => $inches % 12) : "";
    }

    /**
     * @param float $weightInKilograms
     */
    public function setWeightInKilograms($weightInKilograms) {
        $this->weightInKilograms = $weightInKilograms;
    }

    /**
     * @return float
     */
    public function getWeightInKilograms() {
        return $this->weightInKilograms != "" ? $this->roundUp($this->weightInKilograms) : "";
    }

    /**
     * @param float $weightInPounds
     */
    public function setWeightInPounds($weightInPounds) {
        $this->weightInKilograms = $weightInPounds * 0.45359;
    }

    /**
     * @return float
     */
    public function getWeightInPounds() {
        return $this->weightInKilograms != "" ? $this->roundUp($this->weightInKilograms * 2.20462) : "";
    }

    /**
     * @return float
     */
    public function getBodySurfaceArea() {
//        return ($this->weightInKilograms != "" && $this->heightInCentimeters != "") ?
//            $this->roundUp(0.007184 * pow($this->weightInKilograms, 0.425) * pow($this->heightInCentimeters, 0.725)) : "";
        return ($this->weightInKilograms != "") ?
            $this->roundUp(($this->weightInKilograms * 4 + 7) / ($this->weightInKilograms + 90)) : "";
    }

    /**
     * @return bool
     */
    public function isIndoor() {
        return $this->indoor;
    }

    /**
     * @param bool $indoor
     */
    public function setIndoor($indoor) {
        $this->indoor = $indoor;
    }

    private function roundUp($value, $precision = self::DECIMAL_PRECISION_DIGITS) {
        return round($value, $precision);
    }

    /**
     * @param \models\PresentingFeatures $presentingFeatures
     */
    public function setPresentingFeatures($presentingFeatures) {
        $this->presentingFeatures = $presentingFeatures;
    }

    /**
     * @return \models\PresentingFeatures
     */
    public function getPresentingFeatures() {
        return $this->presentingFeatures;
    }

    /**
     * @param array $investigations
     */
    public function setInvestigations(array $investigations) {
        $this->investigations = $investigations;
    }

    /**
     * @return array
     */
    public function getInvestigations() {
        return $this->investigations;
    }

    /**
     * @param array $findings
     */
    public function setFindings($findings) {
        $this->findings = $findings;
    }

    /**
     * @return array
     */
    public function getFindings() {
        return $this->findings;
    }

    /**
     * @param array $protocols
     */
    public function setProtocols($protocols) {
        $this->protocols = $protocols;
    }

    /**
     * @return array
     */
    public function getProtocols() {
        return $this->protocols;
    }

    /**
     * @param array $admissionDischargeHistory
     */
    public function setAdmissionDischargeHistory($admissionDischargeHistory) {
        $this->admissionDischargeHistory = $admissionDischargeHistory;
    }

    /**
     * @return array
     */
    public function getAdmissionDischargeHistory() {
        return $this->admissionDischargeHistory;
    }

    /**
     * @param array $followupHistory
     */
    public function setFollowupHistory($followupHistory) {
        $this->followupHistory = $followupHistory;
    }

    /**
     * @return array
     */
    public function getFollowupHistory() {
        return $this->followupHistory;
    }

    /**
     * @return bool
     */
    public function isLastFollowupClosed() {
        $followupHistory = $this->getFollowupHistory();
        if (count($followupHistory) == 0) {
            return true;
        }

        $lastFollowup = $followupHistory[count($this->getFollowupHistory()) - 1];
        if ($lastFollowup->getClosingDate() != null) {
            return true;
        }

        return false;
    }

    /**
     * @param array $prognosticFactors
     */
    public function setPrognosticFactors($prognosticFactors) {
        $this->prognosticFactors = $prognosticFactors;
    }

    /**
     * @return array
     */
    public function getPrognosticFactors() {
        return $this->prognosticFactors;
    }

    /**
     * @param array $summaryHistory
     */
    public function setSummaryHistory($summaryHistory) {
        $this->summaryHistory = $summaryHistory;
    }

    /**
     * @return array
     */
    public function getSummaryHistory() {
        return $this->summaryHistory;
    }

    /**
     * @param array $discussionHistory
     */
    public function setDiscussionHistory($discussionHistory) {
        $this->discussionHistory = $discussionHistory;
    }

    /**
     * @return array
     */
    public function getDiscussionHistory() {
        return $this->discussionHistory;
    }

    /**
     * @param array $adviceHistory
     */
    public function setAdviceHistory($adviceHistory) {
        $this->adviceHistory = $adviceHistory;
    }

    /**
     * @return array
     */
    public function getAdviceHistory() {
        return $this->adviceHistory;
    }

    /**
     * @param array $drugHistory
     */
    public function setDrugHistory($drugHistory) {
        $this->drugHistory = $drugHistory;
    }

    /**
     * @return array
     */
    public function getDrugHistory() {
        return $this->drugHistory;
    }
}
