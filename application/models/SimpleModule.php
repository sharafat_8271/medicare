<?php

namespace models;

use DateTime;

/**
 * Author:  sharafat
 * Created: 2/6/13 4:16 PM
 *
 * @MappedSuperclass
 */
abstract class SimpleModule {
    /**
     * @Id
     * @GeneratedValue
     * @Column(type="integer")
     * @var int
     */
    protected $id;

    /**
     * @ManyToOne(targetEntity="Patient", inversedBy="discussionHistory")
     * @var \models\Patient
     */
    protected $patient;

    /**
     * @Column(type="string")
     * @var string
     */
    protected $details;

    /**
     * @Column(type="datetime")
     * @var DateTime
     */
    protected $postedOn;

    /**
     * @param string $details
     */
    public function setDetails($details) {
        $this->details = $details;
    }

    /**
     * @return string
     */
    public function getDetails() {
        return $this->details;
    }

    /**
     * @param int $id
     */
    public function setId($id) {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @param \models\Patient $patient
     */
    public function setPatient($patient) {
        $this->patient = $patient;
    }

    /**
     * @return \models\Patient
     */
    public function getPatient() {
        return $this->patient;
    }

    /**
     * @param \DateTime $postedOn
     */
    public function setPostedOn($postedOn) {
        $this->postedOn = $postedOn;
    }

    /**
     * @param bool   $format
     * @param string $formatter
     *
     * @return \DateTime
     */
    public function getPostedOn($format = false, $formatter = Investigation::INVESTIGATION_DATE_DISPLAY_FORMAT) {
        return $format ? $this->postedOn->format($formatter) : $this->postedOn;
    }
}
