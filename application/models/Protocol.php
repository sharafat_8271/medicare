<?php

namespace models;

use DateTime;

/**
 * Author:  sharafat
 * Created: 1/11/13 12:38 PM
 *
 * @Entity
 */
class Protocol {
    /**
     * @Id
     * @GeneratedValue
     * @Column(type="integer")
     * @var int
     */
    private $id;

    /**
     * @Column(type="string")
     * @var string
     */
    private $type;

    /**
     * @Column(type="integer")
     * @var int
     */
    private $cycle;

    /**
     * @Column(type="integer")
     * @var int
     */
    private $day;

    /**
     * @Column(type="date")
     * @var DateTime
     */
    private $protocolDate;

    /**
     * @Column(type="string")
     * @var string
     */
    private $remarks;

    /**
     * @ManyToOne(targetEntity="Patient", inversedBy="protocols")
     * @var \models\Patient
     */
    private $patient;

    /**
     * @Column(type="string", nullable=true)
     * @var string
     */
    private $attachmentName;

    /**
     * @param int $cycle
     */
    public function setCycle($cycle) {
        $this->cycle = $cycle;
    }

    /**
     * @return int
     */
    public function getCycle() {
        return $this->cycle;
    }

    /**
     * @param int $day
     */
    public function setDay($day) {
        $this->day = $day;
    }

    /**
     * @return int
     */
    public function getDay() {
        return $this->day;
    }

    /**
     * @param int $id
     */
    public function setId($id) {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @param DateTime $protocolDate
     */
    public function setProtocolDate(DateTime $protocolDate) {
        $this->protocolDate = $protocolDate;
    }

    /**
     * @param bool   $format
     * @param string $formatter
     *
     * @return mixed
     */
    public function getProtocolDate($format = false, $formatter = Investigation::INVESTIGATION_DATE_DISPLAY_FORMAT) {
        return $format ? $this->protocolDate->format($formatter) : $this->protocolDate;
    }

    /**
     * @param string $remarks
     */
    public function setRemarks($remarks) {
        $this->remarks = $remarks;
    }

    /**
     * @return string
     */
    public function getRemarks() {
        return $this->remarks;
    }

    /**
     * @param string $type
     */
    public function setType($type) {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getType() {
        return $this->type;
    }

    /**
     * @param string $attachmentName
     */
    public function setAttachmentName($attachmentName) {
        $this->attachmentName = $attachmentName;
    }

    /**
     * @return string
     */
    public function getAttachmentName() {
        return $this->attachmentName;
    }

    /**
     * @param \models\Patient $patient
     */
    public function setPatient($patient) {
        $this->patient = $patient;
    }

    /**
     * @return \models\Patient
     */
    public function getPatient() {
        return $this->patient;
    }

    /**
     * @return Protocol
     */
    public function _clone() {
        $protocol = new Protocol();

        $protocol->setId($this->getId());
        $protocol->setType($this->getType());
        $protocol->setCycle($this->getCycle());
        $protocol->setDay($this->getDay());
        $protocol->setProtocolDate($this->getProtocolDate());
        $protocol->setRemarks($this->getRemarks());
        $protocol->setPatient($this->getPatient());

        return $protocol;
    }
}
