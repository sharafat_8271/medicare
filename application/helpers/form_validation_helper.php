<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Form Value
 *
 * http://codeigniter.com/forums/viewthread/159535/#775628
 *
 * Grabs a value from the POST array for the specified field so you can
 * re-populate an input field or textarea.  If Form Validation
 * is active it retrieves the info from the validation class
 *
 * @access   public
 *
 * @param   string
 *
 * @return   mixed
 */
function set_val($field = '', $default = '') {
    $OBJ =& _get_validation_object();

    if ($OBJ === TRUE && isset($OBJ->_field_data[$field])) {
        return $OBJ->set_value($field, $default);
    } else {
        if (!isset($_POST[$field])) {
            return $default;
        }

        return $_POST[$field];
    }
}

function set_chkbox($field = '', $value = '', $default = FALSE) {
    $OBJ =& _get_validation_object();

    if ($OBJ === TRUE && isset($OBJ->_field_data[$field])) {
        return $OBJ->set_checkbox($field, $value, $default);
    } else {
        if (!isset($_POST[$field])) {
            if (!$default) {
                return '';
            }
        } else {
            if (is_array($_POST[$field])) {
                if (!in_array($value, $_POST[$field])) {
                    return '';
                }
            }
        }

        return ' checked="checked"';
    }
}

function set_selct($field = '', $value = '', $default = FALSE) {
    $OBJ =& _get_validation_object();

    if ($OBJ === TRUE && isset($OBJ->_field_data[$field])) {
        $OBJ->set_select($field, $value, $default);
    } else {
        if (!isset($_POST[$field])) {
            if (count($_POST) === 0 AND $default == TRUE) {
                return ' selected="selected"';
            }
            return '';
        }

        $field = $_POST[$field];

        if (is_array($field)) {
            if (!in_array($value, $field)) {
                return '';
            }
        } else {
            if (($field == '' OR $value == '') OR ($field != $value)) {
                return '';
            }
        }

        return ' selected="selected"';
    }

    return $OBJ->set_select($field, $value, $default);
}

/* End of file MY_form_helper.php */
/* Location: ./application/helpers/form_validation_helper.php */
