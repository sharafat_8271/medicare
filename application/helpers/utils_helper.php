<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * @param DateTime $dateTime
 * @param DateTime $from
 * @param DateTime $to
 *
 * @return bool
 */
function isDateTimeWithinRange(DateTime $dateTime, DateTime $from = null, DateTime $to = null) {
    if ($from != null) {
        if ($dateTime < $from) {
            return false;
        }

        if ($to == null) {
            $to = new DateTime();
        }

        if ($dateTime > $to) {
            return false;
        }
    }

    return true;
}

/**
 * @param string $attachmentName
 */
function deleteAttachment($attachmentName) {
    @unlink(UPLOADS_PATH . "/" . $attachmentName);
}

/**
 * @param array $dateTimes
 * @return DateTime
 */
function minDateTime(array $dateTimes) {
    $min = $dateTimes[0];

    foreach ($dateTimes as $date) {
        if ($date < $min) {
            $min = $date;
        }
    }

    return $min;
}
