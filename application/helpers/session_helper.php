<?php
/**
 * Author:  sharafat
 * Created: 12/7/12 7:19 PM
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

if (!function_exists('set_userdata')) {
    function set_userdata($key, $value) {
        $_SESSION[$key] = $value;
    }
}

if (!function_exists('userdata')) {
    function userdata($key) {
        return array_key_exists($key, $_SESSION) ? $_SESSION[$key] : null;
    }
}

if (!function_exists('unset_userdata')) {
    function unset_userdata($key) {
        unset($_SESSION[$key]);
    }
}

if (!function_exists('sess_destroy')) {
    function sess_destroy() {
        @session_unset();
        @session_destroy();
    }
}

if (!function_exists('all_userdata')) {
    function all_userdata() {
        return $_SESSION;
    }
}
