<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

function encryptPassword($password, $salt = "", $hashAlgorithm = "sha512") {
    return hash($hashAlgorithm, $password . $salt);
}
