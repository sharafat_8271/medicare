<?php
/**
 * Author:  sharafat
 * Created: 1/17/13 7:01 PM
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

function getPaginationConfigurations($pageUrl, $totalRows, $limitPerPage, I18N $i18n) {
    $configuration['base_url'] = base_url() . $pageUrl;
    $configuration['total_rows'] = $totalRows;
    $configuration['per_page'] = $limitPerPage;
    $configuration['first_link'] = "&lt;&lt; " . $i18n->_('first');
    $configuration['last_link'] = $i18n->_('last') . " &gt;&gt;";

    if (count($_GET) > 0) {
        $configuration['suffix'] = '?' . http_build_query($_GET, '', "&");
        $configuration['first_url'] = $configuration['base_url'] . '?' . http_build_query($_GET);
    }

    return $configuration;
}
