<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

require_once APPPATH . 'libraries/search.php';

/**
 * Author:  sharafat
 * Created: 10/28/12 11:51 PM
 */
class Controller extends CI_Controller {
    protected $defaultRepositoryName;

    function __construct($defaultRepositoryName = null, $className = null) {
        parent::__construct();

        if ($defaultRepositoryName) {
            $this->defaultRepositoryName = $defaultRepositoryName;
        }

        if ($className) {
            $this->log = Logger::getLogger($className);
        }

        $this->entityManager = $this->doctrine->getEntityManager();

        $this->smarty->registerClass("Patient", "models\Patient");
        $this->smarty->registerClass("Doctor", "models\Doctor");
        $this->smarty->registerClass("Person", "models\Person");
        $this->smarty->registerClass("Investigation", "models\Investigation");
        $this->smarty->registerClass("Findings", "models\Findings");
        $this->smarty->registerClass("PrognosticFactor", "models\PrognosticFactor");
        $this->smarty->registerClass("Search", "Search");

        $this->smarty->assign("controller", $this);
        $this->smarty->assign("session", $this->session);
    }

    public function _remap($method, $params = array()) {
        switch ($method) {
            case 'new':
                $method = 'createForm';
                break;
            case 'list':
                $method = 'listItems';
                break;
            case 'delete':
                $method = 'deleteItem';
                break;
        }

        if (method_exists($this, $method)) {
            call_user_func_array(array($this, $method), $params);
        } else {
            show_404();
        }
    }

    /**
     * Loads a service.
     *
     * The service class must be named xService, with x being started with a uppercase letter.
     * The xService service class must be placed into a file named x.php, with x being started with a lowercase letter.
     * The x.php must be placed in APPPATH/services directory.
     * The service methods can be accessed somewhat like this: $this->xService->someServiceMethod().
     *
     * Example: If the service class is MyAwesomeService, then the service class containing file is named myAwesome.php
     * and is placed at /application/services/myAwesome.php.
     * The service methods can be accessed somewhat like this: $this->myAwesomeService->someServiceMethod().
     *
     * @param $service string The service class name
     */
    public function loadService($service) {
        $serviceFieldName = $service . "Service";
        if (!isset($this->$serviceFieldName)) {
            require_once APPPATH . "services/$service.php";
            $serviceClassName = strtoupper(substr($service, 0, 1)) . substr($service, 1) . "Service";
            $this->$serviceFieldName = new $serviceClassName();
        }
    }

    protected function getRepository($repositoryName = null) {
        return $this->entityManager->getRepository($repositoryName ? : $this->defaultRepositoryName);
    }

    protected function loadFormHelper() {
        $this->load->library('form_validation');
        $this->load->helpers(array('form', 'form_validation'));
        $this->form_validation->set_error_delimiters('<span class="error">', '</span>');
    }

    protected function details($id, $redirectUrl, $repositoryName = null) {
        $this->redirectIfIdNotSet($id, 'details', $redirectUrl);

        $item = $this->getRepository($repositoryName ? : $this->defaultRepositoryName)->find($id);
        $this->redirectIfObjectNotExist($item, $id, 'details', $redirectUrl);

        return $item;
    }

    protected function createForm() {
        $this->loadFormHelper();
    }

    protected function edit($id, $redirectUrl) {
        $this->loadFormHelper();

        $item = $this->getRepository()->find($id);
        $this->redirectIfObjectNotExist($item, $id, 'edit', $redirectUrl);

        return $item;
    }

    protected function update($redirectUrl) {
        $id = $this->input->post('id');
        return $this->getItemOrRedirectIfNotExist($id, $redirectUrl, 'update');
    }

    protected function deleteItem($id, $redirectUrl, $repositoryName = null) {
        $this->getItemOrRedirectIfNotExist($id, $redirectUrl, 'delete');

        $this->entityManager->remove(
            $this->entityManager->getReference($repositoryName ? : $this->defaultRepositoryName, $id));
        $this->entityManager->flush();
    }

    protected function getItemOrRedirectIfNotExist($id, $redirectUrl, $fromMethod) {
        $this->redirectIfIdNotSet($id, $fromMethod, $redirectUrl);

        $item = $this->getRepository()->find($id);
        $this->redirectIfObjectNotExist($item, $id, $fromMethod, $redirectUrl);

        return $item;
    }

    protected function redirectIfIdNotSet($id, $fromMethod, $redirectUrl) {
        if (!isset($id)) {
            $this->log->warn("$fromMethod() called without any given id");
            redirect($redirectUrl);
            exit;
        }
    }

    protected function redirectIfObjectNotExist($item, $id, $fromMethod, $redirectUrl) {
        if (!isset($item)) {
            $this->log->warn("$fromMethod() called with non-existent id: $id");
            redirect($redirectUrl);
            exit;
        }
    }

    protected function preparePagination($pageUrl, $totalRows, $limitPerPage) {
        $this->load->library('pagination');
        $this->load->library('i18n');
        $this->load->helper('pagination');

        $paginationConfigurations = getPaginationConfigurations($pageUrl, $totalRows, $limitPerPage, $this->i18n);
        $this->pagination->initialize($paginationConfigurations);
    }

    protected function getPaginationLimit() {
        return $this->config->item('pagination_limit');
    }

    /**
     * @return models\Provider
     */
    protected function getProviderReference() {
        return $this->entityManager->getReference('models\Provider', $this->getLogin()->getProvider()->getId());
    }

    /**
     * @return models\Person
     */
    protected function getLogin() {
        return $this->session->userdata('login');
    }
}
