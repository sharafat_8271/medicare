<?php
/**
 * Author:  sharafat
 * Created: 8/31/12 11:35 AM
 */

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

require_once 'Controller.php';

class Investigation extends Controller {

    public function __construct() {
        parent::__construct("models\Investigation", __CLASS__);
    }

    public function listItems($id) {
        $this->redirectIfIdNotSet($id, __METHOD__, "patient/list");

        $patient = $this->getRepository("models\Patient")->find($id);
        $this->redirectIfObjectNotExist($patient, $id, __METHOD__, "patient/list");

        $this->loadService('investigation');

        $this->smarty->assign("patient", $patient);
        $this->smarty->assign("targetPerson", $patient);
        $this->smarty->assign("investigations", $this->investigationService->getInvestigations($patient));
        $this->smarty->assign("editMode", $this->input->get('editMode'));
        $this->smarty->view("investigation/list", $this);
    }

    public function createForm($id) {
        parent::createForm();

        $patient = $this->getRepository("models\Patient")->find($id);
        $this->assignPatientAndTargetPersonAndForwardToAddInvestigationView($patient);
    }

    private function assignPatientAndTargetPersonAndForwardToAddInvestigationView($patient) {
        $this->smarty->assign("patient", $patient);
        $this->smarty->assign("targetPerson", $patient);
        $this->smarty->view("investigation/add", $this);
    }

    protected function deleteItem($id, $patientId = 0) {
        parent::deleteItem($id, $redirectUrl = "/investigation/list/$patientId");

        if ($this->input->get('attachment')) {
            $this->load->helper('utils');
            deleteAttachment($this->input->get('attachment'));
        }

        $this->session->set_flashdata('success', true);
        redirect("/investigation/list/$patientId");
    }

    public function add() {
        $this->validateInputAndSaveInvestigationAndRedirect();
    }

    private function validateInputAndSaveInvestigationAndRedirect() {
        $this->prepareValidation();

        if (!$this->form_validation->run()) {
            $patient = $this->getRepository("models\Patient")->find($this->input->post('patientId'));
            $this->smarty->assign("validationErrors", true);
            $this->assignPatientAndTargetPersonAndForwardToAddInvestigationView($patient);

            return false;
        }

        $errors = $this->populateObjectAndSave();
        if (count($errors) > 0) {
            $this->session->set_flashdata('file_upload_errors', $errors);
        } else {
            $this->session->set_flashdata('success', true);
        }

        redirect("investigation/new/" . $this->input->post('patientId'));
    }

    private function prepareValidation() {
        $this->loadFormHelper();
        $this->load->library('i18n');

        foreach ($this->input->post(null, true) as $key => $value) {
            if (strpos($key, "Date") !== false) {
                $field = substr($key, 0, strlen($key) - strlen("Date"));
                $this->form_validation->set_rules($field . 'Value', '', "trim|htmlspecialchars");
                $this->form_validation->set_rules($key, '', "callback_checkEitherValueOrAttachmentIsPresent[$field]");
            }
        }
    }

    public function checkEitherValueOrAttachmentIsPresent($dateValue, $field) {
        if ($field == "cbc") {
            if ($this->dateIsNotEmpty($dateValue) && $this->allCbcFieldsAreEmpty()) {
                $this->form_validation->set_message('checkEitherValueOrAttachmentIsPresent',
                                                    $this->i18n->_('value.required'));
                return false;
            } elseif (!$this->allCbcFieldsAreEmpty() && $this->dateIsEmpty($dateValue)) {
                $this->form_validation->set_message('checkEitherValueOrAttachmentIsPresent',
                                                    $this->i18n->_('date.required'));
                return false;
            }
        } elseif ($field == "sElectrolyte") {
            if ($this->dateIsNotEmpty($dateValue) && $this->allSElectrolyteFieldsAreEmpty()) {
                $this->form_validation->set_message('checkEitherValueOrAttachmentIsPresent',
                                                    $this->i18n->_('value.required'));
                return false;
            } elseif (!$this->allSElectrolyteFieldsAreEmpty() && $this->dateIsEmpty($dateValue)) {
                $this->form_validation->set_message('checkEitherValueOrAttachmentIsPresent',
                                                    $this->i18n->_('date.required'));
                return false;
            }
        } else {
            if ($this->dateSetButValueAndAttachmentIsEmpty($dateValue, $field)) {
                $this->form_validation->set_message(
                    'checkEitherValueOrAttachmentIsPresent',
                    $this->i18n->_($this->attachmentFieldExists($field)
                                       ? 'remarks.or.attachment.required' : 'remarks.required'));
                return false;
            } elseif ($this->valueOrAttachmentSetButDateIsEmpty($field, $dateValue)) {
                $this->form_validation->set_message('checkEitherValueOrAttachmentIsPresent',
                                                    $this->i18n->_('date.required'));
                return false;
            }
        }

        return true;
    }

    private function allCbcFieldsAreEmpty() {
        return $this->input->post("cbcHb") == ""
                && $this->input->post("cbcEsr") == ""
                && $this->input->post("cbcTc") == ""
                && $this->input->post("cbcN") == ""
                && $this->input->post("cbcL") == ""
                && $this->input->post("cbcM") == ""
                && $this->input->post("cbcE") == ""
                && $this->input->post("cbcB") == ""
                && $this->input->post("cbcBlast") == ""
                && $this->input->post("cbcMyelocytes") == ""
                && $this->input->post("cbcPbf") == "";
    }

    private function allSElectrolyteFieldsAreEmpty() {
        return $this->input->post("sElectrolyteNa") == ""
            && $this->input->post("sElectrolyteK") == ""
            && $this->input->post("sElectrolyteCl") == ""
            && $this->input->post("sElectrolyteHCO3") == "";
    }

    private function dateSetButValueAndAttachmentIsEmpty($dateValue, $field) {
        return $this->dateIsNotEmpty($dateValue)
            && ($this->valueIsEmpty($field) && $this->attachmentFieldDoesNotExistOrIsEmpty($field));
    }

    private function valueOrAttachmentSetButDateIsEmpty($field, $dateValue) {
        return (!$this->valueIsEmpty($field) && $this->dateIsEmpty($dateValue))
            || ($this->attachmentFieldExists($field) && !$this->attachmentIsEmpty($field) && $this->dateIsEmpty($dateValue));
    }

    private function dateIsNotEmpty($dateValue) {
        return !$this->dateIsEmpty($dateValue);
    }

    private function dateIsEmpty($dateValue) {
        return $dateValue == "";
    }

    private function valueIsEmpty($field) {
        return $this->input->post($field . "Value") == "";
    }

    private function attachmentFieldDoesNotExistOrIsEmpty($field) {
        return !$this->attachmentFieldExists($field) || $this->attachmentIsEmpty($field);
    }

    private function attachmentFieldExists($field) {
        return array_key_exists($field . "Attachment", $_FILES);
    }

    private function attachmentIsEmpty($field) {
        return $_FILES[$field . "Attachment"]['name'] == "";
    }

    private function populateObjectAndSave() {
        $this->load->library('upload');
        $errors = array();

        $patient = $this->entityManager->getReference("models\Patient", $this->input->post('patientId'));
        $postedBy = $this->entityManager->getReference("models\Doctor", $this->session->userdata('login')->getId());
        $postedOn = new DateTime();

        foreach ($this->input->post(null, true) as $key => $value) {
            if (strpos($key, "Date") !== false && !empty($value)) {
                $field = substr($key, 0, strlen($key) - strlen("Date"));
                $date = $this->input->post($field . "Date");

                if ($field == "cbc") {
                    if ($this->input->post("cbcHb") != "") {
                        $this->addInvestigation(models\Investigation::CBC_HB, $date, $this->input->post("cbcHb"), $patient, $postedBy, $postedOn);
                    }
                    if ($this->input->post("cbcEsr") != "") {
                        $this->addInvestigation(models\Investigation::CBC_ESR, $date, $this->input->post("cbcEsr"), $patient, $postedBy, $postedOn);
                    }
                    if ($this->input->post("cbcTc") != "") {
                        $this->addInvestigation(models\Investigation::CBC_TC, $date, $this->input->post("cbcTc"), $patient, $postedBy, $postedOn);
                    }
                    if ($this->input->post("cbcN") != "") {
                        $this->addInvestigation(models\Investigation::CBC_N, $date, $this->input->post("cbcN"), $patient, $postedBy, $postedOn);
                    }
                    if ($this->input->post("cbcL") != "") {
                        $this->addInvestigation(models\Investigation::CBC_L, $date, $this->input->post("cbcL"), $patient, $postedBy, $postedOn);
                    }
                    if ($this->input->post("cbcM") != "") {
                        $this->addInvestigation(models\Investigation::CBC_M, $date, $this->input->post("cbcM"), $patient, $postedBy, $postedOn);
                    }
                    if ($this->input->post("cbcE") != "") {
                        $this->addInvestigation(models\Investigation::CBC_E, $date, $this->input->post("cbcE"), $patient, $postedBy, $postedOn);
                    }
                    if ($this->input->post("cbcB") != "") {
                        $this->addInvestigation(models\Investigation::CBC_B, $date, $this->input->post("cbcB"), $patient, $postedBy, $postedOn);
                    }
                    if ($this->input->post("cbcBlast") != "") {
                        $this->addInvestigation(models\Investigation::CBC_BLAST, $date, $this->input->post("cbcBlast"), $patient, $postedBy, $postedOn);
                    }
                    if ($this->input->post("cbcMyelocytes") != "") {
                        $this->addInvestigation(models\Investigation::CBC_MYELOCYTES, $date, $this->input->post("cbcMyelocytes"), $patient, $postedBy, $postedOn);
                    }
                    if ($this->input->post("cbcPbf") != "") {
                        $this->addInvestigation(models\Investigation::CBC_PBF, $date, $this->input->post("cbcPbf"), $patient, $postedBy, $postedOn);
                    }
                } elseif ($field == "sElectrolyte") {
                    if ($this->input->post("sElectrolyteNa") != "") {
                        $this->addInvestigation(models\Investigation::S_ELECTROLITE_NA, $date, $this->input->post("sElectrolyteNa"), $patient, $postedBy, $postedOn);
                    }
                    if ($this->input->post("sElectrolyteK") != "") {
                        $this->addInvestigation(models\Investigation::S_ELECTROLITE_K, $date, $this->input->post("sElectrolyteK"), $patient, $postedBy, $postedOn);
                    }
                    if ($this->input->post("sElectrolyteCl") != "") {
                        $this->addInvestigation(models\Investigation::S_ELECTROLITE_CL, $date, $this->input->post("sElectrolyteCl"), $patient, $postedBy, $postedOn);
                    }
                    if ($this->input->post("sElectrolyteHCO3") != "") {
                        $this->addInvestigation(models\Investigation::S_ELECTROLITE_HCO3, $date, $this->input->post("sElectrolyteHCO3"), $patient, $postedBy, $postedOn);
                    }
                } else {
                    $attachmentFieldName = $field . "Attachment";
                    $uploadData = null;

                    if ($this->attachmentExists($attachmentFieldName) && !empty($_FILES[$attachmentFieldName]['name'])) {
                        if (!$this->upload->do_upload($attachmentFieldName)) {
                            $errors[] = $this->upload->display_errors();
                            continue;
                        }

                        $uploadData = $this->upload->data();
                    }

                    $this->addInvestigation(models\Investigation::getTypeByName($field), $date,
                                            $this->input->post($field . "Value"), $patient, $postedBy, $postedOn,
                                            $attachmentFieldName, $uploadData);
                }
            }
        }

        $this->entityManager->flush();

        return $errors;
    }

    /**
     * @param      $type                int
     * @param      $date                string
     * @param      $value               string
     * @param      $patient
     * @param      $postedBy
     * @param      $postedOn
     * @param null $attachmentFieldName string
     * @param null $uploadData          array
     *
     * @return void
     */
    private function addInvestigation($type, $date, $value, $patient, $postedBy, $postedOn,
                                      $attachmentFieldName = null, $uploadData = null) {

        $investigation = new models\Investigation();
        $investigation->setType($type);
        $investigation->setValue($value);
        $investigation->setInvestigationDate(new DateTime($date));
        $investigation->setPatient($patient);
        $investigation->setPostedBy($postedBy);
        $investigation->setPostedOn($postedOn);

        if (!empty($attachmentFieldName) && $this->attachmentExists($attachmentFieldName) && !empty($uploadData)) {
            $investigation->setAttachmentName($uploadData['file_name']);
        }

        $this->entityManager->persist($investigation);
    }

    private function attachmentExists($attachmentFieldName) {
        return array_key_exists($attachmentFieldName, $_FILES);
    }
}
