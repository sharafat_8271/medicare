<?php
/**
 * Author:  sharafat
 * Created: 2/6/13 4:22 PM
 */

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

require_once 'SimpleModuleController.php';

class Advice extends SimpleModuleController {

    public function __construct() {
        parent::__construct("advice", "models\Advice");
    }

    protected function objectList(models\Patient $patient) {
        return $patient->getAdviceHistory();
    }
}
