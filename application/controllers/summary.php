<?php
/**
 * Author:  sharafat
 * Created: 2/6/13 4:22 PM
 */

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

require_once 'SimpleModuleController.php';

class Summary extends SimpleModuleController {

    public function __construct() {
        parent::__construct("summary", "models\Summary");
    }

    protected function objectList(models\Patient $patient) {
        return $patient->getSummaryHistory();
    }
}
