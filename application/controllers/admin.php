<?php
/**
 * Author:  sharafat
 * Created: 1/17/13 10:54 PM
 */

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

require_once 'person.php';

class Admin extends Person {

    function __construct() {
        parent::__construct('admin', "models\Admin", __CLASS__);
    }

    protected function populateObjectAndSave(models\Admin $admin) {
        $admin->setAdmin(true);

        parent::populateObjectAndSave($admin);
    }
}
