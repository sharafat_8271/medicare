<?php
/**
 * Author:  sharafat
 * Created: 8/31/12 11:35 AM
 */

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

require_once 'Controller.php';
use Doctrine\ORM\Tools\Pagination\Paginator;

abstract class Person extends Controller {

    protected $personType;

    function __construct($personType, $defaultRepositoryName, $className) {
        parent::__construct($defaultRepositoryName, $className);
        $this->personType = $personType;
    }

    public function listItems($offset = 0) {
        $personTypeConstantValue = $this->getPersonTypeConstantValue();
        $limit = $this->getPaginationLimit();

        $search = new Search();
        /** @var $queryObject Doctrine\ORM\Query */
        $queryObject = $search->getQueryObject($this->getProviderReference(), $personTypeConstantValue, $this->input,
                                               $this->entityManager, $offset, $limit);
        $personList = new Paginator($queryObject, $fetchJoinCollection = false);

        $pageUrl = ($personTypeConstantValue == Search::PERSON_TYPE_ALL ? "administer" : $this->personType) . "/list";
        parent::preparePagination($pageUrl, count($personList), $limit);

        $this->loadFormHelper();

        $this->smarty->assign("personType", $this->personType);
        $this->smarty->assign("personList", $personList);
        $this->smarty->assign("pagination", $this->pagination->create_links());
        $this->smarty->assign("startSerial", $queryObject->getFirstResult() + 1);
        $this->smarty->assign("endSerial", count($personList));
        $this->smarty->assign("sortBy", $this->input->get("sortBy") ?: "name");
        $this->smarty->assign("sortOrder", $this->input->get("sortOrder") ?: "asc");
        $this->smarty->view("person/list", $this);
    }

    public function details($id) {
        $person = parent::details($id, "/{$this->personType}/list");

        $this->load->helper('form');
        $this->smarty->assign("person", $person);
        $this->smarty->assign("targetPerson", $person);
        $this->smarty->assign("personType", $this->personType);
        $this->smarty->view(($this->personType == 'patient' ? 'patient' : 'person') . "/details", $this);
    }

    public function register() {
        parent::createForm();
        $this->assignVariablesAndAddEditView($this->createPersonInstance());
    }

    public function add() {
        $this->validateInputAndSavePersonAndRedirect($this->createPersonInstance());
    }

    public function edit($id) {
        /** @var $person models\Person */
        $person = parent::edit($id, "/{$this->personType}/list");
        $this->assignVariablesAndAddEditView($person);
    }

    public function update() {
        /** @var $person models\Person */
        $person = parent::update("/{$this->personType}/list");
        $this->validateInputAndSavePersonAndRedirect($person);
    }

    public function deleteItem() {
        throw new RuntimeException("Unsupported Operation - delete");
    }

    private function validateInputAndSavePersonAndRedirect(models\Person $person) {
        $this->prepareValidation($person);

        if (!$this->form_validation->run()) {
            $this->assignVariablesAndAddEditView($person);
            return false;
        }

        $updateAction = $person->getId() > 0;

        try {
            $this->entityManager->getConnection()->beginTransaction();
            $this->populateObjectAndSave($person);
            $this->entityManager->getConnection()->commit();
        } catch (Exception $e) {
            $this->log->error("Error saving person with id: {$person->getId()}", $e);
            $this->entityManager->getConnection()->rollback();
            $this->entityManager->close();
            throw $e;
        }

        if ($updateAction) {
            $this->session->set_flashdata("profile.updated", true);
        }
        redirect("/{$this->personType}/details/" . $person->getId());

        return true;
    }

    protected function prepareValidation(models\Person $person) {
        $this->loadFormHelper();
        $this->load->library('i18n');

        $this->form_validation->set_rules('name', $this->i18n->_("full.name"), 'trim|required|htmlspecialchars');
        $this->form_validation->set_rules('sex', $this->i18n->_("sex"), 'required');
        $this->form_validation->set_rules('dob', $this->i18n->_("dob"), 'trim|required');
        $this->form_validation->set_rules('address', $this->i18n->_("address"), 'trim|htmlspecialchars');
        $this->form_validation->set_rules('mobile', $this->i18n->_("mobile"), 'trim|htmlspecialchars');

        if ($person->getId() == 0 && $this->personType != 'patient') {
            $this->form_validation->set_rules('password', $this->i18n->_("password"), 'required');
            $this->form_validation->set_rules('retypePassword', $this->i18n->_("retype.password"),
                                              'required|matches[password]');
        }

        if ($person->getId() > 0) {
            $this->form_validation->set_rules('email', $this->i18n->_("email"),
                                              (!is_a($person, "models\Patient") ? 'required|' : '') . "trim|valid_email"
                                                  . "|callback_checkEmailNotAlreadyTaken[{$person->getId()}]");
        } else {
            $this->load->database();
            $this->form_validation->set_rules('email', $this->i18n->_("email"),
                                              (!is_a($person, "models\Patient") ? 'required|' : '')
                                                  . 'trim|valid_email|is_unique[Person.email]');
            $this->form_validation->set_message('is_unique', $this->i18n->_("email.already.registered"));
        }
    }

    public function checkEmailNotAlreadyTaken($email, $personId) {
        $query = $this->entityManager->createQuery('SELECT person.id '
                                                       . 'FROM models\Person person '
                                                       . 'WHERE person.email = :email AND person.id != :personId');
        $result = $query->setParameter('email', $email)
                        ->setParameter('personId', $personId)
                        ->getArrayResult();

        if (count($result) > 0) {
            $this->form_validation->set_message('checkEmailNotAlreadyTaken', $this->i18n->_("email.already.registered"));

            return false;
        }

        return true;
    }

    protected function populateObjectAndSave(models\Person $person) {
        $person->setName($this->input->post('name'));
        $person->setSex($this->input->post('sex'));
        $person->setDob(new DateTime($this->input->post('dob')));
        $person->setReligion($this->input->post('religion'));
        $person->setAddress($this->input->post('address'));
        $person->setMobile($this->input->post('mobile'));
        $person->setEmail($this->input->post('email'));
        $person->setRegistrationDateTime(new DateTime());
        $person->setAccountStatus(\models\Person::ACTIVE);
        /** @noinspection PhpParamsInspection */
        $person->setRegisteredBy($this->entityManager->getReference('models\Person', $this->getLogin()->getId()));
        $person->setProvider($this->getProviderReference());
        if ($person->getId() == 0) {
            $this->load->helper('password');
            $person->setPassword(encryptPassword($this->input->post('password'), $this->input->post('email')));
        }

        $this->entityManager->persist($person);
        $this->entityManager->flush();
    }

    private function createPersonInstance() {
        $personSubClass = "models\\" . strtoupper(substr($this->personType, 0, 1)) . substr($this->personType, 1);
        return new $personSubClass();
    }

    private function getPersonTypeConstantValue() {
        switch ($this->personType) {
            case 'doctor':
                return Search::PERSON_TYPE_DOCTOR;
            case 'patient':
                return Search::PERSON_TYPE_PATIENT;
            case 'admin':
                return Search::PERSON_TYPE_ADMIN;
            case 'person':
                return Search::PERSON_TYPE_ALL;
            default:
                return null;
        }
    }

    private function assignVariablesAndAddEditView(models\Person $entity) {
        $this->smarty->assign("person", $entity);
        if ($entity->getId() > 0) {
            $this->smarty->assign("targetPerson", $entity);
        }
        $this->smarty->assign("personType", $this->personType);
        $this->smarty->assign("templateFile", $this->personType == 'admin'
            ? "person/password_create.tpl" : "{$this->personType}/add_edit.tpl");
        $this->smarty->view("person/add_edit", $this);
    }
}
