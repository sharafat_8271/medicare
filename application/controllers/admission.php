<?php
/**
 * Author:  sharafat
 * Created: 8/31/12 11:35 AM
 */

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

require_once 'Controller.php';
use models\AdmissionDischarge;

class Admission extends Controller {

    function __construct() {
        parent::__construct("models\AdmissionDischarge", __CLASS__);
    }

    public function admit($id) {
        parent::createForm();
        $this->verifyThenAssignPatientAndView($id, AdmissionDischarge::ADMISSION);
    }

    public function discharge($id) {
        parent::createForm();
        $this->verifyThenAssignPatientAndView($id, AdmissionDischarge::DISCHARGE);
    }

    private function verifyThenAssignPatientAndView($id, $eventType) {
        $patient = $this->verifyIdAndGetPatient($id, __METHOD__);
        $this->redirectIfAdmittingAdmittedPatientOrDischargingDischargedPatient($patient, $eventType);
        $this->assignPatientAndView($patient);
    }

    /**
     * @param $id
     * @param $fromMethod
     *
     * @return models\Patient
     */
    private function verifyIdAndGetPatient($id, $fromMethod) {
        $this->redirectIfIdNotSet($id, $fromMethod, "patient/list");

        $patient = $this->getRepository("models\Patient")->find($id);
        $this->redirectIfObjectNotExist($patient, $id, $fromMethod, "patient/list");

        return $patient;
    }

    private function redirectIfAdmittingAdmittedPatientOrDischargingDischargedPatient($patient, $eventType) {
        if ($this->admittingAdmittedPatient($patient, $eventType)) {
            $this->log->warn("Attempted admission of already admitted patient. Patient ID: {$patient->getId()}");
            redirect("/patient/details/{$patient->getId()}");
            exit;
        }

        if ($this->dischargingDischargedPatient($patient, $eventType)) {
            $this->log->warn("Attempted discharging of already discharged patient. Patient ID: {$patient->getId()}");
            redirect("/patient/details/{$patient->getId()}");
            exit;
        }
    }

    private function admittingAdmittedPatient($patient, $eventType) {
        return ($patient->isIndoor() && $eventType == \models\AdmissionDischarge::ADMISSION);
    }

    private function dischargingDischargedPatient($patient, $eventType) {
        return (!$patient->isIndoor() && $eventType == \models\AdmissionDischarge::DISCHARGE);
    }

    private function assignPatientAndView($patient) {
        $this->smarty->assign("patient", $patient);
        $this->smarty->assign("targetPerson", $patient);
        $this->smarty->view("admission/add", $this);
    }

    public function add() {
        $patient = $this->getRepository("models\Patient")->find($this->input->post('patientId'));

        if (isset($patient)) {
            $this->prepareValidation();

            if (!$this->form_validation->run()) {
                $this->assignPatientAndView($patient);
                return false;
            }

            $discussion = $this->input->post('discussion');
            $date = new DateTime($this->input->post('date'));

            $this->entityManager->transactional(function($em) use ($patient, $discussion, $date) {
                $admissionDischarge = new AdmissionDischarge();
                $admissionDischarge->setPatient($patient);
                $admissionDischarge->setDiscussion($discussion);
                $admissionDischarge->setEventType(
                    $patient->isIndoor() ? models\AdmissionDischarge::DISCHARGE : models\AdmissionDischarge::ADMISSION);
                $admissionDischarge->setEventDate($date);

                $em->persist($admissionDischarge);

                $patient->setIndoor(!$patient->isIndoor());
                $em->persist($patient);
            });

            $this->session->set_flashdata($patient->isIndoor() ? 'patient.admitted' : 'patient.discharged', true);
            redirect('patient/details/' . $patient->getId());
        }
    }

    private function prepareValidation() {
        $this->loadFormHelper();

        $patientId = $this->input->post('patientId');
        $this->form_validation->set_rules('date', '', "callback_checkDateIsLaterThanLastEvent[$patientId]");
        $this->form_validation->set_rules('discussion', '', "trim|htmlspecialchars");
    }

    public function checkDateIsLaterThanLastEvent($dateString, $patientId) {
        $query = $this->entityManager->createQuery("SELECT ad.eventDate "
                                                 . "FROM models\AdmissionDischarge ad "
                                                 . "WHERE ad.patient = :patient "
                                                 . "ORDER BY ad.eventDate DESC");
        $query->setParameter('patient', $this->entityManager->getReference("models\Patient", $patientId))
              ->setMaxResults(1);
        $lastEventDateString = $query->getOneOrNullResult();

        if ($lastEventDateString == null) {
            return true;
        } elseif (new DateTime($dateString) < $lastEventDateString['eventDate']) {
            $this->load->library('i18n');
            $this->form_validation->set_message('checkDateIsLaterThanLastEvent',
                                                sprintf($this->i18n->_('select.date.later.than.x'), $lastEventDateString));
            return false;
        }

        return true;
    }

    public function listItems($id) {
        $patient = $this->verifyIdAndGetPatient($id, __METHOD__);

        $this->loadService('admission');
        list($admissionHistory, $dischargeHistory) =
            $this->admissionService->getSeparateAdmissionDischargeHistory($patient);

        $this->smarty->assign("admissionHistory", $admissionHistory);
        $this->smarty->assign("dischargeHistory", $dischargeHistory);
        $this->smarty->assign("patient", $patient);
        $this->smarty->assign("targetPerson", $patient);
        $this->smarty->assign("editMode", $this->input->get('editMode'));
        $this->smarty->view("admission/list", $this);
    }

    public function deleteItem($id, $patientId = 0) {
        $patient = $this->getRepository("models\Patient")->find($patientId);

        if (isset($patient)) {
            $this->entityManager->getConnection()->beginTransaction();
            try {
                parent::deleteItem($id, $redirectUrl = "/admission/list/{$patient->getId()}");

                $patient->setIndoor(!$patient->isIndoor());
                $this->entityManager->persist($patient);

                $this->entityManager->flush();
                $this->entityManager->getConnection()->commit();

                $this->session->set_flashdata('success', true);
                redirect("/admission/list/$patientId");
            } catch (Exception $e) {
                $this->log->error("Exception on deleting admission/discharge for id: $id, patientId: $patientId", $e);
                $this->entityManager->getConnection()->rollback();
                $this->entityManager->close();
                throw $e;
            }
        }
    }
}
