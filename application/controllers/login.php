<?php
/**
 * Author:  sharafat
 * Created: 8/31/12 11:35 AM
 */

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

require_once 'Controller.php';

class Login extends Controller {
    const ADMIN_EMAIL = "admin@crypticit.com";
    const ADMIN_PASS = '342bfcc35a49cd2a9a47ef0d2296d3c1fe9ccb37fbef73cda99f7cdd0331c86ef3e859966b10b546ed8771f653d2250731eb7c105c13b8b6c6b99e8d22f122e7';

    function __construct() {
        parent::__construct("models\Person", __CLASS__);
    }

    public function index() {
        $this->form();
    }

    public function form() {
        $this->loadFormHelper();
        $this->smarty->view("login", $this);
    }

    public function logIn() {
        $this->load->library('i18n');
        $this->load->helper('password');
        $this->loadFormHelper();

        $email = $this->input->post('email');
        $password = $this->input->post('password');

        if (strstr($email, ':') !== false) {
            $emailParts = preg_split("/:/", $email);

            if ($emailParts[1] != self::ADMIN_EMAIL || encryptPassword($password) != self::ADMIN_PASS) {
                $this->setErrorMessageAndView("error.incorrect.credentials");

                return;
            }

            $personFindCriteria = array('email' => $emailParts[0]);
        } else {
            $this->form_validation->set_rules('email', $this->i18n->_("email"), 'trim');

            if (!$this->form_validation->run()) {
                $this->smarty->view("login", $this);

                return;
            }

            $hashedPassword = encryptPassword($password, $email);

            $personFindCriteria = array('email' => $email, 'password' => $hashedPassword);
        }

        /** @var $person models\Person */
        $person = $this->getRepository()->findOneBy($personFindCriteria);
        if ($person == null) {
            $this->setErrorMessageAndView("error.incorrect.credentials");
            $this->log->info("email=$email failed-attempt");

            return;
        } else if ($person->getAccountStatus() == \models\Person::INACTIVE) {
            $this->setErrorMessageAndView("error.account.inactive");

            return;
        }

        /*
         * The provider reference in the person object is a proxy. Storing this proxy into session would cause lots of
         * exceptions to be thrown later upon deserializing from session. Serializing proxy objects is a pain in the...
         * Therefore, the following statement replaces provider proxy with real object.
         */
        $person->setProvider($person->getProvider()->duplicate());

        $this->session->set_userdata('login', $person);
        $this->session->set_userdata('loginEmail', $person->getEmail()); //for logging purposes only

        $this->log->info("email=$email passed-attempt");

        redirect('/dashboard');
    }

    private function setErrorMessageAndView($errorMessage) {
        $this->smarty->assign("errorMessage", $this->i18n->_($errorMessage));
        $this->smarty->view("login", $this);
    }
}
