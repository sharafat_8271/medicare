<?php
/**
 * Author:  sharafat
 * Created: 1/19/13 10:53 AM
 */

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

require_once 'person.php';

class Administer extends Person {

    function __construct() {
        parent::__construct('person', "models\Person", __CLASS__);
    }

    function toggleActivation($id) {
        /** @var $person models\Person */
        $person = $this->getRepository()->find($id);

        if ($person->getAccountStatus() == models\Person::ACTIVE) {
            $person->setAccountStatus(models\Person::INACTIVE);
        } else {
            $person->setAccountStatus(models\Person::ACTIVE);
        }

        $this->entityManager->persist($person);
        $this->entityManager->flush();

        $this->session->set_flashdata(
            'person.'.($person->getAccountStatus() == models\Person::ACTIVE ? 'activated' : 'deactivated'), true);
        $this->session->set_flashdata('affected.person.id', $person->getId(true));

        redirect("/administer/list");
    }
}
