<?php
/**
 * Author:  sharafat
 * Created: 8/31/12 11:35 AM
 */

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

require_once 'Controller.php';

class Dashboard extends Controller {

    function __construct() {
        parent::__construct(null, __CLASS__);
    }

    public function index() {
        $categories = $this->getCategories();
        $followups = $this->getFollowups();

        $this->smarty->assign("categories", $categories);
        $this->smarty->assign("followups", $followups);
        $this->smarty->view("dashboard", $this);
    }

    private function getCategories() {
        $result = $this->entityManager
                       ->createQuery('select patient.category, count(patient.id) '
                                         . 'from models\Patient patient '
                                         . 'where patient.category is not null and patient.provider = :provider '
                                         . 'group by patient.category')
                       ->setParameter("provider", $this->getProviderReference())
                       ->getResult();

        /* Sample $result contents:
           Array (
                [0] => Array (
                        [category] => 1
                        [1] => 2 )
                [1] => Array (
                        [category] => 2
                        [1] => 1 )
                [2] => Array (
                        [category] => 5
                        [1] => 1)
           )

            But we want to have an array as [categoryId] => patientCount
         */
        $categories = array();
        foreach ($result as $categoryCountArray) {
            $categories[$categoryCountArray['category']] = $categoryCountArray[1];
        }

        $categories = $this->getAllCategoryCount($categories);
        unset($categories[0]);  //remove the null category
        ksort($categories);

        return $categories;
    }

    private function getAllCategoryCount($categories) {
        foreach (models\Patient::enumerateCategories() as $categoryId => $categoryName) {
            if (!array_key_exists($categoryId, $categories)) {
                $categories[$categoryId] = 0;
            }
        }

        return $categories;
    }

    private function getFollowups() {
        $today = new DateTime();
        return $this->entityManager
                    ->createQuery('select followup '
                                      . 'from models\Followup followup join followup.patient patient '
                                      . 'where followup.followUpDate = :today '
                                      . 'and followup.closingDate is null '
                                      . 'and patient.provider = :provider')
                    ->setParameter("today", $today->format("Y-m-d"))
                    ->setParameter("provider", $this->getProviderReference())
                    ->getResult();
    }
}
