<?php
/**
 * Author:  sharafat
 * Created: 8/31/12 11:35 AM
 */

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

require_once 'Controller.php';

class Findings extends Controller {

    public function __construct() {
        parent::__construct("models\Findings", __CLASS__);
    }

    public function listItems($id) {
        $this->redirectIfIdNotSet($id, __METHOD__, "patient/list");

        $patient = $this->getRepository("models\Patient")->find($id);
        $this->redirectIfObjectNotExist($patient, $id, __METHOD__, "patient/list");

        $this->loadService('findings');
        $findings = $this->findingsService->getGroupedFindings($patient);

        $this->smarty->assign("patient", $patient);
        $this->smarty->assign("targetPerson", $patient);
        $this->smarty->assign("findings", $findings);
        $this->smarty->assign("editMode", $this->input->get('editMode'));
        $this->smarty->view("findings/list", $this);
    }

    protected function deleteItem($id, $patientId = 0) {
        parent::deleteItem($id, $redirectUrl = "/findings/list/$patientId");

        $this->session->set_flashdata('success', true);
        redirect("/findings/list/$patientId");
    }

    public function createForm($id) {
        parent::createForm();

        $patient = $this->getRepository("models\Patient")->find($id);
        $this->smarty->assign("patient", $patient);
        $this->smarty->assign("targetPerson", $patient);
        $this->smarty->view("findings/add", $this);
    }

    public function add() {
        $this->validateInputAndSaveFindingsAndRedirect();
    }

    private function validateInputAndSaveFindingsAndRedirect() {
        $this->prepareValidation();
        $this->form_validation->run();
        $this->populateObjectAndSave();
        $this->session->set_flashdata('findings.added', true);
        redirect("patient/details/" . $this->input->post('patientId'));
    }

    private function prepareValidation() {
        $this->loadFormHelper();

        foreach ($this->input->post(NULL, true) as $field => $value) {
            if (models\Findings::getTypeByName($field) != null) {
                $this->form_validation->set_rules($field, '', "trim|htmlspecialchars");
            }
        }
    }

    private function populateObjectAndSave() {
        $patient = $this->entityManager->getReference("models\Patient", $this->input->post('patientId'));
        $postedBy = $this->entityManager->getReference("models\Doctor", $this->session->userdata('login')->getId());
        $postedOn = new DateTime();

        foreach ($this->input->post(NULL, true) as $field => $value) {
            $findingsType = models\Findings::getTypeByName($field);
            if ($findingsType != null && !empty($value)) {
                $this->addFindings($findingsType, $this->getFindingsDate($field), $value, $patient, $postedBy, $postedOn);
            }
        }

        $this->entityManager->flush();
    }

    private function getFindingsDate($field) {
        $findingsDateInput = $this->input->post($field . "Date");
        return $findingsDateInput != "" ? new DateTime($findingsDateInput) : new DateTime();
    }

    private function addFindings($findingsType, DateTime $findingsDate, $value, models\Patient $patient,
                                 models\Person $postedBy, DateTime $postedOn) {
        $findings = new models\Findings();
        $findings->setType($findingsType);
        $findings->setFindingsDate($findingsDate);
        $findings->setDetails($value);
        $findings->setPatient($patient);
        $findings->setPostedBy($postedBy);
        $findings->setPostedOn($postedOn);

        $this->entityManager->persist($findings);
    }
}
