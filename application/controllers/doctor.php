<?php
/**
 * Author:  sharafat
 * Created: 8/31/12 11:35 AM
 */

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

require_once 'person.php';

class Doctor extends Person {

    function __construct() {
        parent::__construct('doctor', "models\Doctor", __CLASS__);
    }

    protected function prepareValidation(models\Doctor $doctor) {
        parent::prepareValidation($doctor);

        $this->form_validation->set_rules('degree', '', 'trim|htmlspecialchars');
    }

    protected function populateObjectAndSave(models\Doctor $doctor) {
        $doctor->setDegree($this->input->post('degree'));
        $doctor->setAdmin($this->input->post('admin') != null);

        parent::populateObjectAndSave($doctor);
    }
}
