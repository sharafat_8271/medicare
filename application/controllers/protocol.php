<?php
/**
 * Author:  sharafat
 * Created: 1/11/13 12:58 PM
 */

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

require_once 'Controller.php';

class Protocol extends Controller {

    public function __construct() {
        parent::__construct("models\Protocol", __CLASS__);
    }

    public function details($id) {
        $this->loadFormHelper();

        $patient = parent::details($id, "/patient/details/$id", "models\Patient");

        $this->loadService('protocol');
        $protocols = $this->protocolService->getProtocols($patient);

        $lastProtocol = count($protocols) > 0 ? $protocols[count($protocols) - 1]->_clone() : new models\Protocol();
        $this->prepareLastProtocol($lastProtocol);

        $this->smarty->assign("patient", $patient);
        $this->smarty->assign("targetPerson", $patient);
        $this->smarty->assign("protocols", $protocols);
        $this->smarty->assign("protocol", $lastProtocol);
        $this->smarty->view("protocols/details", $this);
    }

    private function prepareLastProtocol(models\Protocol $protocol) {
        if ($protocol->getId() > 0) {
            $protocol->setDay($protocol->getDay() + $protocol->getProtocolDate()->diff(new DateTime(), true)->format('%a'));
            $protocol->setRemarks("");
            $protocol->setId(0);
        } else {
            $protocol->setDay(1);
            $protocol->setCycle(1);
        }

        $protocol->setProtocolDate(new DateTime());
    }

    public function deleteItem($id, $patientId) {
        parent::deleteItem($id, "protocols/details");

        if ($this->input->get('attachment')) {
            $this->load->helper('utils');
            deleteAttachment($this->input->get('attachment'));
        }

        $this->session->set_flashdata('deleted.protocol', true);
        redirect("protocol/details/$patientId");
    }

    public function edit($id) {
        $this->loadFormHelper();

        $protocol = $this->getRepository('models\Protocol')->find($id);
        $patient = $protocol->getPatient();

        $this->smarty->assign("patient", $patient);
        $this->smarty->assign("targetPerson", $patient);
        $this->smarty->assign("protocol", $protocol);
        $this->smarty->view("protocols/edit", $this);
    }

    public function update() {
        $this->validateInputAndSaveProtocolAndRedirect($add = false);
    }

    public function add() {
        $this->validateInputAndSaveProtocolAndRedirect($add = true);
    }

    private function validateInputAndSaveProtocolAndRedirect($add) {
        $this->prepareValidation();

        if (!$this->form_validation->run()) {
            $patient = $this->getRepository("models\Patient")->find($this->input->post('patientId'));

            $this->loadService('protocol');
            $protocols = $this->protocolService->getProtocols($patient);

            $protocol = $add ? new models\Protocol()
                : $this->entityManager->getReference('models\Protocol', $this->input->post('protocolId'));
            if ($protocol->getId() == 0) {
                $this->prepareLastProtocol($protocol);
            }

            $this->smarty->assign("patient", $patient);
            $this->smarty->assign("targetPerson", $patient);
            $this->smarty->assign("protocols", $protocols);
            $this->smarty->assign("protocol", $protocol);
            $this->smarty->view("protocols/" . ($add ? 'details' : 'edit'), $this);

            return FALSE;
        }

        $fileUploadError = $this->populateObjectAndSave($add);

        $this->session->set_flashdata(($add ? 'added' : 'updated') . '.protocol', true);
        if (!empty($fileUploadError)) {
            $this->session->set_flashdata('file.upload.error', $fileUploadError);
        }
        redirect("protocol/details/" . $this->input->post('patientId'));
    }

    private function prepareValidation() {
        $this->loadFormHelper();
        $this->load->library('i18n');

        $this->form_validation->set_rules('type', $this->i18n->_('type'), "required|trim|htmlspecialchars");
        $this->form_validation->set_rules('cycle', $this->i18n->_('cycle'), "required|trim|integer");
        $this->form_validation->set_rules('day', $this->i18n->_('day'), "required|trim|integer");
        $this->form_validation->set_rules('date', $this->i18n->_('date'), "required|trim");
        $this->form_validation->set_rules('remarks', $this->i18n->_('remarks'), "trim|htmlspecialchars");
    }

    private function populateObjectAndSave($add) {
        $this->load->library('upload');
        $fileUploadError = "";
        if (!empty($_FILES['attachment']['name'])) {
            if ($this->upload->do_upload('attachment')) {
                $uploadData = $this->upload->data();
            } else {
                $fileUploadError = $this->upload->display_errors();
            }
        }

        $protocol = $add ? new models\Protocol()
            : $this->entityManager->getReference('models\Protocol', $this->input->post('protocolId'));

        $protocol->setType($this->input->post('type'));
        $protocol->setCycle($this->input->post('cycle'));
        $protocol->setDay($this->input->post('day'));
        $protocol->setProtocolDate(new DateTime($this->input->post('date')));
        $protocol->setRemarks($this->input->post('remarks'));
        $protocol->setPatient($this->entityManager->getReference('models\Patient', $this->input->post('patientId')));

        $oldAttachmentName = $protocol->getAttachmentName();
        if (!empty($uploadData)) {
            $protocol->setAttachmentName($uploadData['file_name']);
        } elseif ($this->input->post('removeAttachment')) {
            $protocol->setAttachmentName("");
        }

        $this->entityManager->persist($protocol);
        $this->entityManager->flush();

        if (!empty($oldAttachmentName)) {
            $this->load->helper('utils');
            deleteAttachment($oldAttachmentName);
        }

        return $fileUploadError;
    }


}
