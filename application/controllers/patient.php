<?php
/**
 * Author:  sharafat
 * Created: 8/31/12 11:35 AM
 */

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

require_once 'person.php';

class Patient extends Person {

    function __construct() {
        parent::__construct('patient', "models\Patient", __CLASS__);
    }

    public function printForm($id) {
        $person = $this->getRepository()->find($id);

        $this->loadFormHelper();

        $this->loadService('admission');
        list($admissionHistory, $dischargeHistory) =
            $this->admissionService->getSeparateAdmissionDischargeHistory($person);
        $this->smarty->assign("admissionHistory", $admissionHistory);
        $this->smarty->assign("dischargeHistory", $dischargeHistory);
        $this->smarty->assign("from", $this->getEarliestDateInPatientRecords($person));

        $this->assignVariablesAndPrintFormView($person);
    }

    private function getEarliestDateInPatientRecords(models\Patient $patient) {
        $dates[] = $this->getEarliestFindingsDate($patient);
        $dates[] = $this->getEarliestInvestigationDate($patient);
        $dates[] = $this->getEarliestProtocolDate($patient);
        $dates[] = $patient->getRegistrationDateTime();

        $this->load->helper('utils');
        return minDateTime($dates)->format(models\Person::DOB_INPUT_FORMAT);
    }

    private function getEarliestFindingsDate(models\Patient $patient) {
        $date = $this->entityManager->createQuery("SELECT min(findings.findingsDate) "
                                                  . "FROM models\Findings findings "
                                                  . "WHERE findings.patient = :patient")
                                    ->setParameter('patient', $patient)
                                    ->getSingleScalarResult();
        return new DateTime($date);
    }

    private function getEarliestInvestigationDate(models\Patient $patient) {
        $date = $this->entityManager->createQuery("SELECT min(investigation.investigationDate) "
                                                  . "FROM models\Investigation investigation "
                                                  . "WHERE investigation.patient = :patient")
                                    ->setParameter('patient', $patient)
                                    ->getSingleScalarResult();
        return new DateTime($date);
    }

    private function getEarliestProtocolDate(models\Patient $patient) {
        $date = $this->entityManager->createQuery("SELECT min(protocol.protocolDate) "
                                                  . "FROM models\Protocol protocol "
                                                  . "WHERE protocol.patient = :patient")
                                    ->setParameter('patient', $patient)
                                    ->getSingleScalarResult();
        return new DateTime($date);
    }

    private function assignVariablesAndPrintFormView($person) {
        $this->smarty->assign("person", $person);
        $this->smarty->assign("targetPerson", $person);
        $this->smarty->view("patient/printForm", $this);
    }

    public function printRecords() {
        /** @var $person models\Person */
        $person = $this->getRepository()->find($this->input->post('patientId'));

        $this->loadFormHelper();
        $this->load->library('i18n');

        $this->form_validation->set_rules('from', $this->i18n->_("print.records.from"), 'trim|required');
        $this->form_validation->set_rules('to', $this->i18n->_("print.records.to"), 'trim');

        if (!$this->form_validation->run()) {
            $this->assignVariablesAndPrintFormView($person);
            return FALSE;
        }

        $from = new DateTime($this->input->post('from'));
        $to = $this->input->post('to') != "" ? new DateTime($this->input->post('to')) : new DateTime();

        $this->loadService('findings');
        $findings = $this->findingsService->getGroupedFindings($person, $from, $to);

        $this->loadService('investigation');
        $investigations = $this->investigationService->getInvestigations($person, $from, $to);

        $this->loadService('admission');
        list($admissionHistory, $dischargeHistory) =
            $this->admissionService->getSeparateAdmissionDischargeHistory($person, $from, $to);

        $this->loadService('protocol');
        $protocols = $this->protocolService->getProtocols($person, $from, $to);

        $this->smarty->assign("person", $person);
        $this->smarty->assign("findings", $findings);
        $this->smarty->assign("investigations", $investigations);
        $this->smarty->assign("protocols", $protocols);
        $this->smarty->assign("admissionHistory", $admissionHistory);
        $this->smarty->assign("dischargeHistory", $dischargeHistory);
        $this->smarty->assign("from", $from->format(models\Investigation::INVESTIGATION_DATE_DISPLAY_FORMAT));
        $this->smarty->assign("to", $to->format(models\Investigation::INVESTIGATION_DATE_DISPLAY_FORMAT));

        $includeSections = $this->input->post('sections');
        if ($includeSections == null) {
            $includeSections = array();
        } elseif (!is_array($includeSections)) {
            $includeSections[] = $includeSections;
        }
        $this->smarty->assign("includeDrug", in_array('drug', $includeSections));
        $this->smarty->assign("includeSummary", in_array('summary', $includeSections));
        $this->smarty->assign("includeDiscussion", in_array('discussion', $includeSections));
        $this->smarty->assign("includeAdvice", in_array('advice', $includeSections));

        $output = $this->smarty->fetch("patient/print.tpl");
        require_once __DIR__ . '/../third_party/mpdf5.4/mpdf.php';
        $pdf = new mPDF('utf-8');
        $pdf->useSubstitutions = true;
        $pdf->defaultfooterline = 0;
        $pdf->setFooter('|Page {PAGENO} of {nb}|');
        $pdf->SetAutoFont();
        $pdf->WriteHTML($output);
        $pdf->SetSubject($this->i18n->_("patient.evaluation.summary"));
        $pdf->SetAuthor($this->session->userdata('login')->getId(true) . " - " . $this->session->userdata('login')->getName());
        $pdf->SetCreator("Cryptic IT - " . $this->i18n->_("app.name"));
        $pdf->Output($person->getId(true) . " - " . $person->getName() . ".pdf", 'I');
    }

    protected function prepareValidation(models\Patient $patient) {
        parent::prepareValidation($patient);

        $this->form_validation->set_rules('height', $this->i18n->_("height"), 'trim|numeric');
        $this->form_validation->set_rules('weight', $this->i18n->_("weight"), 'trim|numeric');
        $this->form_validation->set_rules('diagnosis', $this->i18n->_("diagnosis"), 'trim');
        $this->form_validation->set_rules('currentStatus', $this->i18n->_("current.status"), 'trim');
    }

    protected function populateObjectAndSave(models\Patient $patient) {
        if ($this->input->post('heightUnit') == 'cm') {
            $patient->setHeightInCentimeters($this->input->post('height'));
        } else {
            $patient->setHeightInInches($this->input->post('height'));
        }

        if ($this->input->post('weightUnit') == 'kg') {
            $patient->setWeightInKilograms($this->input->post('weight'));
        } else {
            $patient->setWeightInPounds($this->input->post('weight'));
        }

        $this->deleteExistingPrognosticFactorsIfCategoryIsChanged($patient); //this line must come before calling $patient->setCategory()
        $patient->setCategory($this->input->post('category'));
        $patient->setDiagnosis($this->input->post('diagnosis'));
        $patient->setCurrentStatus($this->input->post('currentStatus'));

        parent::populateObjectAndSave($patient);
    }

    private function deleteExistingPrognosticFactorsIfCategoryIsChanged(models\Patient $patient) {
        if ($patient->getId() > 0 && $patient->getCategory() != $this->input->post('category')) {
            /*
             * Deleting PrognosticFactor collection from patient would've required simply calling
             *          $patient->getPrognosticFactors()->clear();
             * But Doctrine issues DELETE query for each and every collection element.
             * Therefore, to optimize querying, a DQL has been used instead.
             */
            $this->entityManager->createQueryBuilder()
                ->delete('models\PrognosticFactor', 'prognosticFactor')
                ->where('prognosticFactor.patient = :patient')
                ->setParameter('patient', $patient)
                ->getQuery()
                ->execute();
        }
    }
}
