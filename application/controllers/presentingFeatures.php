<?php
/**
 * Author:  sharafat
 * Created: 8/31/12 11:35 AM
 */

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

require_once 'Controller.php';

class PresentingFeatures extends Controller {

    public function __construct() {
        parent::__construct("models\Patient", __CLASS__);
    }

    protected function details($id) {
        $patient = parent::details($id, "/patient/details/$id");
        $this->prepareModelAndView($patient, "details");
    }

    private function prepareModelAndView(models\Patient $patient, $view = 'edit') {
        $this->smarty->assign("patient", $patient);
        $this->smarty->assign("targetPerson", $patient);
        $this->smarty->assign("presentingFeatures", $patient->getPresentingFeatures() != null
            ? $patient->getPresentingFeatures() : new models\PresentingFeatures());
        $this->smarty->view("presentingFeatures/$view", $this);
    }

    public function edit($id) {
        parent::createForm();

        $patient = $this->getRepository()->find($id);
        $this->prepareModelAndView($patient);
    }

    public function update() {
        $patient = $this->getRepository()->find($this->input->post('patientId'));
        $this->validateInputAndSavePresentingFeaturesAndRedirect($patient);
    }

    private function validateInputAndSavePresentingFeaturesAndRedirect(models\Patient $patient) {
        $this->prepareValidation();

        if (!$this->form_validation->run()) {
            $this->prepareModelAndView($patient);
            return FALSE;
        }

        $this->populateObjectAndSave($patient);

        $this->session->set_flashdata("presenting.features.updated", true);
        redirect('/patient/details/' . $this->input->post('patientId'));
    }

    private function prepareValidation() {
        $this->loadFormHelper();

        foreach ($this->input->post(NULL, true) as $key => $value) {
            $this->form_validation->set_rules($key, '', 'trim|htmlspecialchars');
        }
    }

    private function populateObjectAndSave(models\Patient $patient) {
        $presentingFeatures = $patient->getPresentingFeatures() != null
            ? $patient->getPresentingFeatures() : new models\PresentingFeatures();

        $presentingFeatures->setAbdominalDistension($this->input->post('abdominalDistension'));
        $presentingFeatures->setBlurringOfVision($this->input->post('blurringOfVision'));
        $presentingFeatures->setBodyache($this->input->post('bodyache'));
        $presentingFeatures->setBowelAndBladderIncontinence($this->input->post('bowelAndBladderIncontinence'));
        $presentingFeatures->setCough($this->input->post('cough'));
        $presentingFeatures->setDyspnea($this->input->post('dyspnea'));
        $presentingFeatures->setFever($this->input->post('fever'));
        $presentingFeatures->setGumbleeding($this->input->post('gumbleeding'));
        $presentingFeatures->setOthers($this->input->post('others'));
        $presentingFeatures->setRash($this->input->post('rash'));
        $presentingFeatures->setWeakness($this->input->post('weakness'));

        $patient->setPresentingFeatures($presentingFeatures);

        $this->entityManager->persist($patient);
        $this->entityManager->flush();
    }
}
