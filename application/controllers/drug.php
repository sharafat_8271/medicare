<?php
/**
 * Author:  sharafat
 * Created: 2/6/13 4:22 PM
 */

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

require_once 'SimpleModuleController.php';

class Drug extends SimpleModuleController {

    public function __construct() {
        parent::__construct("drug", "models\Drug");
    }

    protected function objectList(models\Patient $patient) {
        return $patient->getDrugHistory();
    }
}
