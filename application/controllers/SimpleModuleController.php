<?php
/**
 * Author:  sharafat
 * Created: 2/6/13 7:27 PM
 */

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

require_once 'Controller.php';

abstract class SimpleModuleController extends Controller {
    private $module;
    private $moduleModel;

    public function __construct($module, $model) {
        parent::__construct($model, __CLASS__);
        $this->module = $module;
        $this->moduleModel = $model;
    }

    public function listItems($id) {
        $this->loadFormHelper();

        $patient = parent::details($id, "/patient/details/$id", "models\Patient");
        $this->assignVariablesAndView($patient, new $this->moduleModel, $add = true);
    }

    private function assignVariablesAndView(models\Patient $patient, $object, $add) {
        $this->smarty->assign("patient", $patient);
        $this->smarty->assign("targetPerson", $patient);
        $this->smarty->assign("object", $object);
        $this->smarty->assign("module", $this->module);
        $this->smarty->assign("objectList", $this->objectList($patient));
        $this->smarty->view('simpleModule/' . ($add ? 'list' : 'edit'), $this);
    }

    protected abstract function objectList(models\Patient $patient);

    public function add() {
        $this->validateInputAndSaveObjectAndRedirect($add = true);
    }

    public function edit($id) {
        $this->loadFormHelper();

        $object = $this->getRepository()->find($id);
        $this->assignVariablesAndView($object->getPatient(), $object, $add = false);
    }

    public function update() {
        $this->validateInputAndSaveObjectAndRedirect($add = false);
    }

    public function deleteItem($id, $patientId) {
        parent::deleteItem($id, $this->module . "/list");

        $this->session->set_flashdata('deleted.' . $this->module, true);
        redirect("{$this->module}/list/$patientId");
    }

    private function validateInputAndSaveObjectAndRedirect($add) {
        $this->prepareValidation();

        if (!$this->form_validation->run()) {
            $patient = $this->getRepository("models\Patient")->find($this->input->post('patientId'));
            $object = $this->getObject($add);

            $this->assignVariablesAndView($patient, $object, $add);

            return FALSE;
        }

        $this->populateObjectAndSave($add);

        $this->session->set_flashdata(($add ? 'added' : 'updated') . '.' . $this->module, true);
        redirect("{$this->module}/list/" . $this->input->post('patientId'));
    }

    private function getObject($add) {
        return $add ? new $this->moduleModel
            : $this->entityManager->getReference($this->moduleModel, $this->input->post($this->module . 'Id'));
    }

    private function prepareValidation() {
        $this->loadFormHelper();
        $this->load->library('i18n');

        $this->form_validation->set_rules($this->module, $this->i18n->_($this->module), "trim|required|htmlspecialchars");
    }

    private function populateObjectAndSave($add) {
        $object = $this->getObject($add);
        $object->setDetails($this->input->post($this->module));
        $object->setPostedOn(new DateTime());
        $object->setPatient($this->entityManager->getReference('models\Patient', $this->input->post('patientId')));

        $this->entityManager->persist($object);
        $this->entityManager->flush();
    }
}
