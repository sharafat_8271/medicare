<?php
/**
 * Author:  sharafat
 * Created: 1/6/13 8:34 PM
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

require_once 'Controller.php';

class Followup extends Controller {

    function __construct() {
        parent::__construct("models\Followup", __CLASS__);
    }

    public function createForm($id) {
        parent::createForm();

        $patient = $this->verifyIdAndGetPatient($id, __METHOD__);

        $this->closeLastFollowup($patient);

        $this->smarty->assign("patient", $patient);
        $this->smarty->assign("targetPerson", $patient);
        $this->smarty->view("followup/add", $this);
    }

    /**
     * @param $id
     * @param $fromMethod
     *
     * @return models\Patient
     */
    private function verifyIdAndGetPatient($id, $fromMethod) {
        $this->redirectIfIdNotSet($id, $fromMethod, "patient/list");

        $patient = $this->getRepository("models\Patient")->find($id);
        $this->redirectIfObjectNotExist($patient, $id, $fromMethod, "patient/list");

        return $patient;
    }

    private function closeLastFollowup(models\Patient $patient) {
        if (!$patient->isLastFollowupClosed()) {
            $followupHistory = $patient->getFollowupHistory();
            $lastFollowup = $followupHistory[count($patient->getFollowupHistory()) - 1];

            if ($this->followupHasBecomePast($lastFollowup)) {
                $this->closeFollowupAsAbsent($lastFollowup);
            } else {
                redirect('followup/close/'. $patient->getId());
                exit;
            }
        }
    }

    private function followupHasBecomePast($lastFollowup) {
        return $lastFollowup->getFollowUpDate()->add(new DateInterval("P1D")) < new DateTime();
    }

    private function closeFollowupAsAbsent($followup) {
        $followup->setClosingDate(new DateTime());
        $followup->setClosingRemarks("Absent");

        $this->entityManager->persist($followup);
        $this->entityManager->flush();
    }

    public function add() {
        $this->validateInputAndSaveFollowupAndRedirect();
    }

    private function validateInputAndSaveFollowupAndRedirect() {
        $this->prepareValidation();

        if (!$this->form_validation->run()) {
            $patient = $this->getRepository("models\Patient")->find($this->input->post('patientId'));

            $this->smarty->assign("patient", $patient);
            $this->smarty->assign("targetPerson", $patient);
            $this->smarty->view("followup/add", $this);

            return FALSE;
        }

        $this->populateObjectAndSave();
        $this->session->set_flashdata("followup.added", true);
        redirect('/patient/details/' . $this->input->post('patientId'));
    }

    private function prepareValidation() {
        $this->loadFormHelper();
        $this->load->library('i18n');

        $this->form_validation->set_rules('followupDate', $this->i18n->_('followup.date'), "required|trim|callback_checkIsPastDate");
        $this->form_validation->set_rules('followupPlan', $this->i18n->_('followup.plan'), "required|trim|htmlspecialchars");
        $this->form_validation->set_rules('patientId', '', "required");
    }

    public function checkIsPastDate($date) {
        if (new DateTime($date) <= new DateTime()) {
            $this->form_validation->set_message('checkIsPastDate', $this->i18n->_('must.be.a.future.date'));
            return false;
        }

        return true;
    }

    private function populateObjectAndSave() {
        $followup = new models\Followup();
        $followup->setPatient($this->entityManager->getReference('models\Patient', $this->input->post('patientId')));
        $followup->setFollowUpDate(new DateTime($this->input->post('followupDate')));
        $followup->setNextPlan($this->input->post('followupPlan'));

        $this->entityManager->persist($followup);
        $this->entityManager->flush();
    }

    public function close($id) {
        parent::createForm();

        $patient = $this->verifyIdAndGetPatient($id, __METHOD__);
        $followupHistory = $patient->getFollowupHistory();

        $this->smarty->assign("patient", $patient);
        $this->smarty->assign("targetPerson", $patient);
        $this->smarty->assign("lastFollowup", $followupHistory[count($followupHistory) - 1]);
        $this->smarty->view("followup/close", $this);
    }

    public function doClose() {
        $this->loadFormHelper();
        $this->load->library('i18n');

        $this->form_validation->set_rules('closingRemarks', $this->i18n->_('closing.remarks'), "required|trim|htmlspecialchars");
        $this->form_validation->set_rules('patientId', '', "required");
        $this->form_validation->set_rules('lastFollowupId', '', "required");

        if (!$this->form_validation->run()) {
            $patient = $this->getRepository("models\Patient")->find($this->input->post('patientId'));
            $followupHistory = $patient->getFollowupHistory();

            $this->smarty->assign("patient", $patient);
            $this->smarty->assign("targetPerson", $patient);
            $this->smarty->assign("lastFollowup", $followupHistory[count($followupHistory) - 1]);
            $this->smarty->view("followup/close", $this);

            return;
        }

        /** @var $followup models\Followup */
        $followup = $this->getRepository()->find($this->input->post('lastFollowupId'));
        $followup->setClosingDate(new DateTime());
        $followup->setClosingRemarks($this->input->post('closingRemarks'));
        $this->entityManager->persist($followup);
        $this->entityManager->flush();

        $this->session->set_flashdata("followup.closed", true);
        redirect('/patient/details/' . $this->input->post('patientId'));
    }

    public function listItems($id) {
        $patient = $this->verifyIdAndGetPatient($id, __METHOD__);

        $this->smarty->assign("patient", $patient);
        $this->smarty->assign("targetPerson", $patient);
        $this->smarty->view("followup/list", $this);
    }
}
