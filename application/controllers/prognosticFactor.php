<?php
/**
 * Author:  sharafat
 * Created: 8/31/12 11:35 AM
 */

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

require_once 'Controller.php';

class PrognosticFactor extends Controller {

    public function __construct() {
        parent::__construct("models\Patient", __CLASS__);
    }

    protected function details($id) {
        $patient = parent::details($id, "/patient/details/$id");
        $this->prepareModelAndView($patient, "details");
    }

    private function prepareModelAndView(models\Patient $patient, $view = 'edit') {
        $this->smarty->assign("prognosticFactors",
                              models\PrognosticFactor::getFactorsByCategory($patient->getCategory()));
        $this->smarty->assign("patient", $patient);
        $this->smarty->assign("targetPerson", $patient);
        $this->smarty->view("prognosticFactor/$view", $this);
    }

    public function edit($id) {
        parent::createForm();

        $patient = $this->getRepository()->find($id);
        $this->prepareModelAndView($patient);
    }

    public function update() {
        $patient = $this->getRepository()->find($this->input->post('patientId'));
        $this->validateInputAndSavePrognosticFactorsAndRedirect($patient);
    }

    private function validateInputAndSavePrognosticFactorsAndRedirect(models\Patient $patient) {
        $this->prepareValidation();

        if (!$this->form_validation->run()) {
            $this->prepareModelAndView($patient);
            return FALSE;
        }

        $this->populateObjectAndSave($patient);

        $this->session->set_flashdata("prognostic.factors.updated", true);
        redirect('/patient/details/' . $this->input->post('patientId'));
    }

    private function prepareValidation() {
        $this->loadFormHelper();

        foreach ($this->input->post(NULL, true) as $key => $value) {
            if (strstr($key, "status")) {
                $this->form_validation->set_rules($key, '', 'trim|htmlspecialchars');
            }
        }
    }

    private function populateObjectAndSave(models\Patient $patient) {
        $patientPrognosticFactors = $patient->getPrognosticFactors();
        $patientPrognosticFactorsExistInDatabase = count($patientPrognosticFactors) > 0;

        $index = 0;
        foreach ($this->input->post(NULL, true) as $key => $value) {
            if (strstr($key, "status_")) {
                $keyParts = explode("_", $key);
                $factorId = $keyParts[1];

                $factor = $patientPrognosticFactorsExistInDatabase ? $patientPrognosticFactors[$index]
                    : new models\PrognosticFactor();
                $factor->setFactor($factorId);
                $factor->setStatus($value);
                $factor->setFavorable($this->isFavorable($factorId));
                $factor->setPatient($patient);
                $patientPrognosticFactors[] = $factor;

                $index++;
            }
        }

        $this->entityManager->persist($patient);
        $this->entityManager->flush();
    }

    private function isFavorable($factorId) {
        if ($this->input->post("favorable_$factorId") === false) { //user didn't select any option at all
            return null;
        }

        switch ($this->input->post("favorable_$factorId")) {
            case '0':
                return 0;
            case '1':
                return 1;
            default:
                return null;
        }
    }
}
