<?php
/**
 * Author:  sharafat
 * Created: 1/16/13 6:29 PM
 */

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

require_once 'Controller.php';

class Password extends Controller {

    function __construct() {
        parent::__construct("models\Person", __CLASS__);
    }

    public function resetPass($id) {
        $this->loadFormHelper();

        $person = $this->getRepository()->find($id);

        $this->smarty->assign("person", $person);
        $this->smarty->assign("targetPerson", $person);
        $this->smarty->view("administer/resetPass", $this);
    }

    public function update() {
        $this->load->helper('password');

        /** @var $person models\Person */
        $person = $this->getRepository()->find($this->input->post("id"));
        $this->prepareValidation($person);

        if (!$this->form_validation->run()) {
            $this->assignVariablesAndViewForDisplayingError($person);
            return;
        }

        $this->updatePassword($person);

        $this->session->set_flashdata("password.updated", true);
        redirect("/{$this->getControllerByPersonType($person)}/details/{$person->getId()}");
    }

    private function getControllerByPersonType($person) {
        if (is_a($person, "models\Doctor")) {
            return"doctor";
        } elseif (is_a($person, "models\Admin")) {
            return "admin";
        } else {
            return "patient";
        }
    }

    private function prepareValidation(models\Person $person) {
        $this->loadFormHelper();
        $this->load->library('i18n');

        if ($this->input->post('currentPassword')) {
            $this->form_validation->set_rules('currentPassword', $this->i18n->_("current.password"),
                                              "required|callback_verifyCurrentPasswordIsCorrect"
                                                  . "[{$person->getEmail()},{$person->getPassword()}]");
        }
        $this->form_validation->set_rules('newPassword', $this->i18n->_("new.password"),
                                          "required|callback_verifyNewPasswordIsDifferentThanCurrentPassword");
        $this->form_validation->set_rules('retypeNewPassword', $this->i18n->_("retype.new.password"),
                                          'required|matches[newPassword]');
    }

    private function assignVariablesAndViewForDisplayingError($person) {
        $this->smarty->assign("person", $person);
        $this->smarty->assign("targetPerson", $person);

        if ($this->input->post("from") == "password/resetPass") {
            $this->smarty->view("administer/resetPass", $this);
        } else {
            if (is_a($person, "models\Doctor")) {
                $this->smarty->assign("doctor", $person);
                $this->smarty->assign("personType", "doctor");
            } elseif (is_a($person, "models\Person")) {
                $this->smarty->assign("admin", $person);
                $this->smarty->assign("personType", "admin");
            }

            $this->smarty->view($this->input->post("from"), $this);
        }
    }

    public function verifyCurrentPasswordIsCorrect($currentPassword, $emailAndStoredPassword) {
        list($email, $storedPassword) = preg_split("/,/", $emailAndStoredPassword);

        $encryptedCurrentPassword = encryptPassword($currentPassword, $email);
        if ($storedPassword != $encryptedCurrentPassword) {
            $this->form_validation->set_message('verifyCurrentPasswordIsCorrect',
                                                $this->i18n->_('current.password.incorrect'));
            return false;
        }

        return true;
    }

    public function verifyNewPasswordIsDifferentThanCurrentPassword($newPassword) {
        if ($newPassword == $this->input->post('currentPassword')) {
            $this->form_validation->set_message('verifyNewPasswordIsDifferentThanCurrentPassword',
                                                $this->i18n->_('new.password.cant.be.same.as.current.password'));
            return false;
        }

        return true;
    }

    private function updatePassword($person) {
        $encryptedNewPassword = encryptPassword($this->input->post('newPassword'), $person->getEmail());
        $person->setPassword($encryptedNewPassword);

        $this->entityManager->persist($person);
        $this->entityManager->flush();
    }
}
