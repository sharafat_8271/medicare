<?php
/**
 * Author:  sharafat
 * Created: 8/31/12 11:35 AM
 */

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

require_once 'Controller.php';

class Logout extends Controller {

    function __construct() {
        parent::__construct(null, __CLASS__);
    }

    public function index() {
        $this->session->sess_destroy();
        redirect("/login");
    }
}
