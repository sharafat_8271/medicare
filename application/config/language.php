<?php
/**
 * Author:  sharafat
 * Created: 8/29/12 7:15 AM
 */

global $config;

/*
|--------------------------------------------------------------------------
| Supported Languages
|--------------------------------------------------------------------------
|
| Contains all languages your site will store data in. Other languages can
| still be displayed via language files, that's totally different.
|
| Check for HTML equivalents for characters such as � with the URL below:
|    http://htmlhelp.com/reference/html40/entities/latin1.html
|
*/
$config['supported_languages'] = array(
    'en'=> array('name' => 'English', 'lang' => 'en'),
    'en_US'=> array('name' => 'English', 'lang' => 'en'),
    'en_GB'=> array('name' => 'English', 'lang' => 'en'),
    'bn'=> array('name' => 'Bengali', 'lang' => 'bn'),
    'bn_BD'=> array('name' => 'Bengali', 'lang' => 'bn')
);

/*
|--------------------------------------------------------------------------
| Default Language
|--------------------------------------------------------------------------
|
| If no language is specified, which one to use? Must be in the array above
|
|   en
|
*/
$config['default_language'] = 'en';

/*
|--------------------------------------------------------------------------
| Messages File Path
|--------------------------------------------------------------------------
|
*/
$config['messages_file_path'] = APPPATH . "language";
