<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| Hooks
| -------------------------------------------------------------------------
| This file lets you define "hooks" to extend CI without hacking the core
| files.  Please see the user guide for info:
|
|	http://codeigniter.com/user_guide/general/hooks.html
|
*/

$hook['pre_system'][] = array(
    'class'    => '',
    'function' => 'log_uri',
    'filename' => 'log_profile.php',
    'filepath' => 'hooks'
);

$hook['pre_system'][] = array(
    'class'    => '',
    'function' => 'configureLog4php',
    'filename' => 'log4php.php',
    'filepath' => 'hooks'
);

$hook['pre_system'][] = array(
    'class'    => '',
    'function' => 'setGlobalExceptionHandler',
    'filename' => 'exception_handler.php',
    'filepath' => 'hooks'
);

$hook['pre_controller'] = array(
    'class'    => '',
    'function' => 'pickLanguage',
    'filename' => 'i18n.php',
    'filepath' => 'hooks'
);

$hook['post_controller_constructor'] = array(
    'class'    => '',
    'function' => 'authenticate',
    'filename' => 'authenticate.php',
    'filepath' => 'hooks'
);

$hook['post_controller'] = array(
    'class'    => '',
    'function' => 'log_profile',
    'filename' => 'log_profile.php',
    'filepath' => 'hooks'
);

/* End of file hooks.php */
/* Location: ./application/config/hooks.php */
