<?php
$config['upload_path'] = UPLOADS_PATH;
$config['allowed_types'] = 'gif|jpg|jpeg|png|pdf';
$config['encrypt_name'] = true;
