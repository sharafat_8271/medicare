SET AUTOCOMMIT=0;
START TRANSACTION;


CREATE TABLE IF NOT EXISTS `AdmissionDischarge` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` int(11) NOT NULL,
  `discussion` text NOT NULL,
  `eventType` tinyint(1) unsigned NOT NULL,
  `eventDate` date NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_ADMISSION_DISCHARGE_PATIENT_ID` (`patient_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

CREATE TABLE IF NOT EXISTS `Advice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` int(11) NOT NULL,
  `details` text NOT NULL,
  `postedOn` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_ADVICE_PATIENT_ID` (`patient_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

CREATE TABLE IF NOT EXISTS `Discussion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` int(11) NOT NULL,
  `details` text NOT NULL,
  `postedOn` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_DISCUSSION_PATIENT_ID` (`patient_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

CREATE TABLE IF NOT EXISTS `Drug` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` int(11) NOT NULL,
  `details` text NOT NULL,
  `postedOn` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_DRUG_PATIENT_ID` (`patient_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

CREATE TABLE IF NOT EXISTS `Findings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` int(11) NOT NULL,
  `type` tinyint(2) unsigned NOT NULL,
  `findingsDate` date NOT NULL,
  `details` text NOT NULL,
  `postedOn` datetime NOT NULL,
  `postedBy_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_FINDINGS_PATIENT_ID` (`patient_id`),
  KEY `IDX_FINDINGS_POSTED_BY_ID` (`postedBy_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=201 ;

CREATE TABLE IF NOT EXISTS `Followup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` int(11) NOT NULL,
  `nextPlan` text NOT NULL,
  `followUpDate` date NOT NULL,
  `closingRemarks` text,
  `closingDate` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_FOLLOWUP_PATIENT_ID` (`patient_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=203 ;

CREATE TABLE IF NOT EXISTS `Investigation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` int(11) NOT NULL,
  `type` tinyint(2) NOT NULL,
  `investigationDate` date NOT NULL,
  `value` text NOT NULL,
  `attachmentName` varchar(255) DEFAULT NULL,
  `postedOn` datetime NOT NULL,
  `postedBy_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_INVESTIGATION_PATIENT_ID` (`patient_id`),
  KEY `IDX_INVESTIGATION_POSTED_BY_ID` (`postedBy_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=201 ;

CREATE TABLE IF NOT EXISTS `Person` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `sex` tinyint(1) unsigned NOT NULL,
  `dob` date NOT NULL,
  `religion` tinyint(1) unsigned DEFAULT NULL,
  `address` text,
  `mobile` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` char(128) DEFAULT NULL,
  `accountStatus` tinyint(1) unsigned NOT NULL,
  `registrationDateTime` datetime NOT NULL,
  `admin` tinyint(1) unsigned NOT NULL,
  `registeredBy_id` int(11) DEFAULT NULL,
  `type` varchar(255) NOT NULL,
  `degree` text,
  `heightInCentimeters` double DEFAULT NULL,
  `weightInKilograms` double DEFAULT NULL,
  `currentStatus` text,
  `diagnosis` text,
  `indoor` tinyint(1) unsigned DEFAULT NULL,
  `category` tinyint(2) unsigned DEFAULT NULL,
  `presentingFeatures_id` int(11) DEFAULT NULL,
  `provider_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_PERSON_REG_BY_ID` (`registeredBy_id`),
  KEY `IDX_PERSON_PRESENTING_FEATURES_ID` (`presentingFeatures_id`),
  KEY `IDX_PERSON_PROVIDER_ID` (`provider_id`),
  KEY `IDX_PERSON_NAME` (`name`),
  KEY `IDX_PERSON_EMAIL` (`email`),
  KEY `IDX_PERSON_EMAIL_PASSWORD` (`email`,`password`),
  KEY `IDX_PERSON_TYPE` (`type`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=311 ;

CREATE TABLE IF NOT EXISTS `PresentingFeatures` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fever` varchar(255) NOT NULL,
  `cough` varchar(255) NOT NULL,
  `weakness` varchar(255) NOT NULL,
  `bodyache` varchar(255) NOT NULL,
  `dyspnea` varchar(255) NOT NULL,
  `gumbleeding` varchar(255) NOT NULL,
  `rash` varchar(255) NOT NULL,
  `abdominalDistension` varchar(255) NOT NULL,
  `bowelAndBladderIncontinence` varchar(255) NOT NULL,
  `blurringOfVision` varchar(255) NOT NULL,
  `others` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=202 ;

CREATE TABLE IF NOT EXISTS `PrognosticFactor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` int(11) NOT NULL,
  `factor` tinyint(3) unsigned NOT NULL,
  `status` varchar(255) DEFAULT NULL,
  `favorable` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_PROGNOSTIC_FACTOR_PATIENT_ID` (`patient_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=29 ;

CREATE TABLE IF NOT EXISTS `Protocol` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `cycle` tinyint(3) unsigned NOT NULL,
  `day` tinyint(4) unsigned NOT NULL,
  `protocolDate` date NOT NULL,
  `remarks` text NOT NULL,
  `attachmentName` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_PROTOCOL_PATIENT_ID` (`patient_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

CREATE TABLE IF NOT EXISTS `Provider` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `departmentName` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `contactNo` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `logoFileName` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

CREATE TABLE IF NOT EXISTS `Summary` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` int(11) NOT NULL,
  `details` text NOT NULL,
  `postedOn` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_SUMMARY_PATIENT_ID` (`patient_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;


ALTER TABLE `AdmissionDischarge`
  ADD CONSTRAINT `FK_ADMISSION_DISCHARGE_PATIENT_ID` FOREIGN KEY (`patient_id`) REFERENCES `Person` (`id`);

ALTER TABLE `Advice`
  ADD CONSTRAINT `FK_ADVICE_PATIENT_ID` FOREIGN KEY (`patient_id`) REFERENCES `Person` (`id`);

ALTER TABLE `Discussion`
  ADD CONSTRAINT `FK_DISCUSSION_PATIENT_ID` FOREIGN KEY (`patient_id`) REFERENCES `Person` (`id`);

ALTER TABLE `Drug`
  ADD CONSTRAINT `FK_DRUG_PATIENT_ID` FOREIGN KEY (`patient_id`) REFERENCES `Person` (`id`);

ALTER TABLE `Findings`
  ADD CONSTRAINT `FK_FINDINGS_PATIENT_ID` FOREIGN KEY (`patient_id`) REFERENCES `Person` (`id`),
  ADD CONSTRAINT `FK_FINDINGS_POSTED_BY_ID` FOREIGN KEY (`postedBy_id`) REFERENCES `Person` (`id`);

ALTER TABLE `Followup`
  ADD CONSTRAINT `FK_FOLLOWUP_PATIENT_ID` FOREIGN KEY (`patient_id`) REFERENCES `Person` (`id`);

ALTER TABLE `Investigation`
  ADD CONSTRAINT `FK_INVESTIGATION_PATIENT_ID` FOREIGN KEY (`patient_id`) REFERENCES `Person` (`id`),
  ADD CONSTRAINT `FK_INVESTIGATION_POSTED_BY_ID` FOREIGN KEY (`postedBy_id`) REFERENCES `Person` (`id`);

ALTER TABLE `Person`
  ADD CONSTRAINT `FK_PERSON_REGISTERED_BY_ID` FOREIGN KEY (`registeredBy_id`) REFERENCES `Person` (`id`),
  ADD CONSTRAINT `FK_PERSON_PRESENTING_FEATURES_ID` FOREIGN KEY (`presentingFeatures_id`) REFERENCES `PresentingFeatures` (`id`),
  ADD CONSTRAINT `FK_PERSON_PROIDER_ID` FOREIGN KEY (`provider_id`) REFERENCES `Provider` (`id`);

ALTER TABLE `PrognosticFactor`
  ADD CONSTRAINT `FK_PROGNOSTIC_FACTOR_PATIENT_ID` FOREIGN KEY (`patient_id`) REFERENCES `Person` (`id`);

ALTER TABLE `Protocol`
  ADD CONSTRAINT `FK_PROTOCOL_PATIENT_ID` FOREIGN KEY (`patient_id`) REFERENCES `Person` (`id`);

ALTER TABLE `Summary`
  ADD CONSTRAINT `FK_SUMMARY_PATIENT_ID` FOREIGN KEY (`patient_id`) REFERENCES `Person` (`id`);


INSERT INTO `Provider` (`id`, `name`, `departmentName`, `address`, `contactNo`, `email`, `logoFileName`) VALUES
(1, 'Sir Salimullah Medical College', 'Dept. of Haematology', 'Mitford, Dhaka-1100', '+88-02-7310061 - 4', 'info@ssmc.edu', '1_ssmc.jpg'),
(2, 'Cryptic IT', '', '4/2, Road 1, Kalyanpur, Dhaka-1207', '+88-01755529174', 'admin@crypticit.com', '2_cryptic.gif');

INSERT INTO `Person` (`id`, `name`, `sex`, `dob`, `religion`, `address`, `mobile`, `email`, `password`, `accountStatus`, `registrationDateTime`, `admin`, `registeredBy_id`, `type`, `provider_id`) VALUES
(1, 'Cryptic IT Admin', 5, '2011-08-01', 1, 'House 4/2, Road 1, Kalyanpur, Dhaka-1207', '+88-017555-29174', 'cryptic-adm@ssmc.edu', '5879891417ac4751f89aae41d270ccdd9bb6c3d4ed8250a719b1bcdd3113a421f4ef81174bcd784af5b15dc571d463ca9c0f68154ee16485d719a243b92e7958', 8, '2013-01-01 00:00:00', 1, 1, 'admin', 1),
(2, 'Cryptic IT Admin', 5, '2011-08-01', 1, 'House 4/2, Road 1, Kalyanpur, Dhaka-1207', '+88-017555-29174', 'cryptic-adm@crypticit.com', 'bc48d5ab77df009e699485ed5f956f8b0b397382993ea5410fd80cf322e99b2f9bad66354d75e47b6fdd87a06186844cdf61e58a572d7e66ff860c2d1439e25d', 8, '2013-01-01 00:00:00', 1, 2, 'admin', 2);


COMMIT;
