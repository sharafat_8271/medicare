/*
    Copyright(c) Cryptic IT
    Author     : Sharafat
    Created on : Sep 19, 2011, 12:33:38 AM
*/

$.widget("cryptic.box", {
    options: {
        width: "100%",
        shadow: false
    },
    _create: function() {
        this.element.css("width", this.options.width);
        this.element.addClass("ui-widget-content ui-corner-all");
        if (this.options.shadow) {
            this.element.addClass("box-shadow");
            this.element.css("margin", "10px");
        }
        this.element.children("div:nth-child(1)").addClass("ui-state-default ui-corner-top box-header");
        this.element.children("div:nth-child(2)").addClass("box-body");
    }
});

$.widget("cryptic.shadowBox", {
    options: {
        width: "100%"
    },
    _create: function() {
        this.element.box({width: this.options.width, shadow:true});
    }
});

$.widget("cryptic.button", {
    _create: function() {
        var buttonElem = this.element;

        buttonElem.addClass("form-input-text rounded-corners");
        buttonElem.addClass("form-input-button");
        buttonElem.hover(function() {
            $(this).addClass("form-input-button-hover");
        }, function() {
            $(this).removeClass("form-input-button-hover");
        });
    }
});

$.widget("cryptic.styleTable", {
    _create: function() {
        var table = this.element;

        table.addClass("style-table");

        table.find("tr").live('mouseover mouseout', function (event) {
            var tdList = $(this).children("td");
            if (event.type == 'mouseover') {
                tdList.addClass("hover");
            } else {
                tdList.removeClass("hover");
            }
        });

        if (table.find("tbody").length) {
            table.find("tbody tr:odd").each(function () {
                $(this).children("td").addClass("odd");
            });
        }
    }
});

function setEqualWidth(jqElems) {
    var maxWidth = 0;
    $.each(jqElems, function(index, elem) {
        var elemWidth = $(elem).width();
        if (elemWidth > maxWidth) {
            maxWidth = elemWidth;
        }
    });
    var errorValue = 1 / 100; //1% error - tentative error margin observed
    jqElems.width(maxWidth + maxWidth * errorValue);
}

function filterSameWidthInclusiveElements(jqElems) {
    var noSameWidthClass = "no-same-width";
    var sameWidthIncludeElements = new Array();
    jqElems.each(function(index, value) {
        if (!$(value).hasClass(noSameWidthClass)) {
            sameWidthIncludeElements.push(value);
        }
    });
    return $(sameWidthIncludeElements);
}
