# README #

Medicare is an online application to manage hospital patients. Features include:

* Doctors, patients and hospital staff management
* Patients' haematological information
* Patient findings, investigations and protocols information
* Patient summaries, advices and drug details
* Hospital admission/discharge history
* Followup history
* Printable patient discharge notes with full diagnosis, investigation, advices and other information

### Screenshots ###

![Medicare Screenshot 1](https://bytebucket.org/sharafat_8271/medicare/raw/17eb5e60c0af06500e8bb775657b08b992f4d77c/screenshots/01.png)

--------

![Medicare Screenshot 2](https://bytebucket.org/sharafat_8271/medicare/raw/17eb5e60c0af06500e8bb775657b08b992f4d77c/screenshots/02.png)

--------

![Medicare Screenshot 3](https://bytebucket.org/sharafat_8271/medicare/raw/17eb5e60c0af06500e8bb775657b08b992f4d77c/screenshots/03.png)

--------

![Medicare Screenshot 4](https://bytebucket.org/sharafat_8271/medicare/raw/17eb5e60c0af06500e8bb775657b08b992f4d77c/screenshots/04.png)

--------

![Medicare Screenshot 5](https://bytebucket.org/sharafat_8271/medicare/raw/17eb5e60c0af06500e8bb775657b08b992f4d77c/screenshots/05.png)

--------

![Medicare Screenshot 6](https://bytebucket.org/sharafat_8271/medicare/raw/17eb5e60c0af06500e8bb775657b08b992f4d77c/screenshots/06.png)

--------

![Medicare Screenshot 7](https://bytebucket.org/sharafat_8271/medicare/raw/17eb5e60c0af06500e8bb775657b08b992f4d77c/screenshots/07.png)

--------

![Medicare Screenshot 8](https://bytebucket.org/sharafat_8271/medicare/raw/17eb5e60c0af06500e8bb775657b08b992f4d77c/screenshots/08.png)

--------

![Medicare Screenshot 9](https://bytebucket.org/sharafat_8271/medicare/raw/17eb5e60c0af06500e8bb775657b08b992f4d77c/screenshots/09.png)

--------

![Medicare Screenshot 10](https://bytebucket.org/sharafat_8271/medicare/raw/17eb5e60c0af06500e8bb775657b08b992f4d77c/screenshots/10.png)
